/**
 * @file ImageUtils.h
 * @brief ImageUtils
 *
 * @author Abhijit Kundu
 */

#ifndef SHAPESPACE_IMAGEUTILS_H_
#define SHAPESPACE_IMAGEUTILS_H_

#include <Eigen/Core>

namespace ShapeSpace {

void saveImage(const Eigen::Matrix<unsigned char, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>& image, const std::string& filename);
void saveImage(const Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>& image, const std::string& filename);

template<class Derived>
void saveImageAs8UC1(const Eigen::MatrixBase<Derived>& image, const std::string& filename, const double scale = 255.0) {
  using Image8UC1 = Eigen::Matrix<unsigned char, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>;
  Image8UC1 image_8UC1 = (image.array() * scale).template cast<unsigned char>();
  image_8UC1.colwise().reverseInPlace();
  saveImage(image_8UC1, filename);
}

template<class Derived>
void saveImageAs32FC1(const Eigen::MatrixBase<Derived>& image, const std::string& filename, const double scale = 255.0) {
  using Image32FC1 = Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>;
  Image32FC1 image_32FC1 = (image.array() * scale).template cast<float>();
  image_32FC1.colwise().reverseInPlace();
  saveImage(image_32FC1, filename);
}

}  // namespace ShapeSpace

#endif // end SHAPESPACE_IMAGEUTILS_H_
