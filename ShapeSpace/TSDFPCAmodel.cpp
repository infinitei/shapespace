/**
 * @file TSDFPCAmodel.cpp
 * @brief TSDFPCAmodel
 *
 * @author Abhijit Kundu
 */

#include "ShapeSpace/TSDFPCAmodel.h"
#include "ShapeSpace/H5EigenTensor.h"
#include "ShapeSpace/H5StdContainers.h"

namespace ShapeSpace {

TSDFPCAmodel::TSDFPCAmodel(const std::string& tsdf_pca_model_file) {
  H5::H5File file(tsdf_pca_model_file, H5F_ACC_RDONLY);

  H5Eigen::load(file, "tsdf_mean", mean);
  H5Eigen::load(file, "tsdf_basis", basis);

  assert(mean.dimension(0) == basis.dimension(0));
  assert(mean.dimension(1) == basis.dimension(1));
  assert(mean.dimension(2) == basis.dimension(2));

  H5Eigen::load(file, "encoded_training_data", encoded_training_data);
  H5StdContainers::load(file, "model_names", model_names);

  H5Eigen::load(file, "thicknesses", thicknesses);

  assert(basis.dimension(4) == encoded_training_data.rows());
  assert(model_names.size() == encoded_training_data.cols());
}

}  // end namespace ShapeSpace
