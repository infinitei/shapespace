/**
 * @file SMPLmodel.cpp
 * @brief SMPLmodel
 *
 * @author Abhijit Kundu
 */

#include "ShapeSpace/SMPLmodel.h"

namespace ShapeSpace {

template class SMPLmodel<float>;
template class SMPLmodel<double>;

}  // end namespace ShapeSpace
