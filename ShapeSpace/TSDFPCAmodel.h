/**
 * @file TSDFPCAmodel.h
 * @brief TSDFPCAmodel
 *
 * @author Abhijit Kundu
 */

#ifndef SHAPESPACE_TSDFPCAMODEL_H_
#define SHAPESPACE_TSDFPCAMODEL_H_

#include <Eigen/CXX11/Tensor>
#include <vector>
#include <string>

namespace ShapeSpace {

class TSDFPCAmodel {
 public:
  TSDFPCAmodel(const std::string& tsdf_pca_model_file);

  using Scalar = float;
  using Tensor1 = Eigen::Tensor<Scalar, 1>;
  using Tensor3 = Eigen::Tensor<Scalar, 3>;
  using Tensor4 = Eigen::Tensor<Scalar, 4>;


  template <class DerivedShape>
  Tensor3 computeTSDF(const Eigen::DenseBase<DerivedShape>& shape_coeffs) const {
    EIGEN_STATIC_ASSERT_VECTOR_ONLY(DerivedShape);
    static_assert(std::is_same<typename DerivedShape::Scalar, Scalar>::value, "shape_coeffs has diff type");

    const Eigen::Index num_shape_params = basis.dimension(3);
    assert(shape_coeffs.size() == num_shape_params);

    Tensor1 betas(num_shape_params);
    Eigen::Map<Eigen::VectorXf> betas_map(betas.data(), num_shape_params, 1);
    betas_map = shape_coeffs;

    typedef Tensor1::DimensionPair DimPair;
    Eigen::array<DimPair, 1> product_dims = {{DimPair(3, 0)}};
    return mean + basis.contract(betas, product_dims);
  }

  Tensor3 computeTSDF(const Tensor1& shape_coeffs) const {
    assert(shape_coeffs.size() == basis.dimension(3));

    typedef Tensor1::DimensionPair DimPair;
    Eigen::array<DimPair, 1> product_dims = {{DimPair(3, 0)}};
    return mean + basis.contract(shape_coeffs, product_dims);
  }


  Tensor3 mean;
  Tensor4 basis;

  Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> encoded_training_data;
  std::vector<std::string> model_names;

  Eigen::Vector3d thicknesses;
};

}  // end namespace ShapeSpace
#endif // end SHAPESPACE_TSDFPCAMODEL_H_
