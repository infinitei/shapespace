/**
 * @file KeyCoordTransformer.h
 * @brief KeyCoordTransformer
 *
 * @author Abhijit Kundu
 */

#ifndef SHAPESAPCE_KEYCOORDTRANSFORMER_H_
#define SHAPESAPCE_KEYCOORDTRANSFORMER_H_

namespace ShapeSpace {

template<int _Dimension, typename _PositionScalar = double>
class KeyToCoord {
 public:
  using PositionScalar = _PositionScalar;
  static const int Dimension = _Dimension;
  using PositionType = Eigen::Matrix<PositionScalar, Dimension, 1>;

  KeyToCoord(const PositionType& bbx_min, const PositionScalar resolution)
      : bbx_min_(bbx_min),
        resolution_(resolution) {
  }

  template<class KeyScalar>
  PositionType operator()(KeyScalar x, KeyScalar y, KeyScalar z) const {
    static_assert(Dimension == 3, "This method is only for KeyToCoord<3, ..>");
    return bbx_min_ + PositionType(x+ 0.5, y + 0.5, z + 0.5) * resolution_;
  }

  template<class KeyScalar>
  PositionType operator()(KeyScalar x, KeyScalar y) const {
    static_assert(Dimension == 2, "This method is only for KeyToCoord<2, ..>");
    return bbx_min_ + PositionType(x+ 0.5, y + 0.5) * resolution_;
  }

  template<class KeyScalar>
  PositionType operator()(const std::array<KeyScalar, 3>& key) const {
    static_assert(Dimension == 3, "This method is only for KeyToCoord<3, ..>");
    return bbx_min_ + PositionType(key[0] + 0.5, key[1] + 0.5, key[2] + 0.5) * resolution_;
  }

  template<class KeyScalar>
  PositionType operator()(const std::array<KeyScalar, 2>& key) const {
    static_assert(Dimension == 2, "This method is only for KeyToCoord<2, ..>");
    return bbx_min_ + PositionType(key[0] + 0.5, key[1] + 0.5) * resolution_;
  }

 private:

  PositionType bbx_min_;
  PositionScalar resolution_;
};

}  // namespace ShapeSpace


#endif // end SHAPESAPCE_KEYCOORDTRANSFORMER_H_
