/**
 * @file ShapeSliders.cpp
 * @brief ShapeSliders
 *
 * @author Abhijit Kundu
 */

#include "ShapeSliders.h"
#include <QtWidgets>

ShapeSliders::ShapeSliders(const std::size_t dimension, const QString& title, QWidget* parent)
    : QGroupBox(title, parent) {

  for (std::size_t i = 0; i < dimension; ++i) {
    sliders_.emplace_back(new QSlider(Qt::Horizontal));
  }

  QBoxLayout *slidersLayout = new QBoxLayout(QBoxLayout::TopToBottom);

  for (std::size_t i = 0; i < dimension; ++i) {
    sliders_[i]->setFocusPolicy(Qt::StrongFocus);
    sliders_[i]->setTickPosition(QSlider::TicksBothSides);
    sliders_[i]->setRange(0, 100);
    sliders_[i]->setValue(50);
    sliders_[i]->setTickInterval(10);
    sliders_[i]->setSingleStep(1);

    connect(sliders_[i].get(), SIGNAL(valueChanged(int)), this, SIGNAL(valueChanged()));

    slidersLayout->addWidget(sliders_[i].get());
  }

  setLayout(slidersLayout);
}

Eigen::VectorXi ShapeSliders::value() const {
  Eigen::VectorXi value(sliders_.size());

  for (std::size_t i = 0; i < sliders_.size(); ++i) {
    value[i] = sliders_[i]->value();
  }

  return value;
}
