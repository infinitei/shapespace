/**
 * @file ImageUtils.cpp
 * @brief ImageUtils
 *
 * @author Abhijit Kundu
 */

#define EIGEN_DEFAULT_DENSE_INDEX_TYPE int

#include "ShapeSpace/ImageUtils.h"
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

namespace ShapeSpace {

void saveImage(const Eigen::Matrix<unsigned char, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>& image, const std::string& filename) {
  cv::Mat cv_image(image.rows(), image.cols(), CV_8UC1, (void*)image.data());
  cv::imwrite(filename, cv_image);
}

void saveImage(const Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>& image, const std::string& filename) {
  cv::Mat cv_image(image.rows(), image.cols(), CV_32FC1, (void*)image.data());
  cv::imwrite(filename, cv_image);
}


}  // namespace Shapespace
