/**
 * @file NumericDiff.h
 * @brief NumericDiff
 *
 * @author Abhijit Kundu
 */

#ifndef SHAPESPACE_NUMERICDIFF_H_
#define SHAPESPACE_NUMERICDIFF_H_

#include <Eigen/Core>

namespace ShapeSpace {

template <class CostFunctor, class DerivedParams, class Scalar>
typename DerivedParams::PlainObject
numericDiffForward(const CostFunctor& functor,
                   const Eigen::MatrixBase<DerivedParams>& parameters,
                   const Scalar cost_at_eval_point) {
  EIGEN_STATIC_ASSERT_VECTOR_ONLY(DerivedParams);

  using Vector = typename DerivedParams::PlainObject;
  Vector gradient;
  gradient.resize(parameters.size());

  for (Eigen::Index i = 0; i < gradient.size(); ++i) {
    Vector parameters_plus_h = parameters;

    const Scalar min_step_size = 1e-4;
    const Scalar relative_step_size = 1e-1;

    const Scalar step_size = std::abs(parameters[i]) * relative_step_size;
    const Scalar h = std::max(min_step_size, step_size);

    parameters_plus_h[i] += h;
    const Scalar f_plus = functor(parameters_plus_h);

    gradient[i] = (f_plus - cost_at_eval_point) / h;
  }

  return gradient;
}

template <class CostFunctor, class DerivedParams>
typename DerivedParams::PlainObject
numericDiffCentral(const CostFunctor& functor,
                   const Eigen::MatrixBase<DerivedParams>& parameters) {
  EIGEN_STATIC_ASSERT_VECTOR_ONLY(DerivedParams);

  using Scalar = typename DerivedParams::Scalar;
  using Vector = typename DerivedParams::PlainObject;

//  const Scalar min_step_size = std::sqrt(std::numeric_limits<Scalar>::epsilon());
  const Scalar min_step_size = 1e-4;
  const Scalar relative_step_size = 1e-1;

  Vector gradient;
  gradient.resize(parameters.size());

  for (Eigen::Index i = 0; i < gradient.size(); ++i) {
    Vector parameters_plus_h = parameters;

    const Scalar step_size = std::abs(parameters[i]) * relative_step_size;
    const Scalar h = std::max(min_step_size, step_size);

    const Scalar two_h = 2 * h;

    // Mutate 1 element at a time
    parameters_plus_h[i] += h;
    const Scalar f_plus = functor(parameters_plus_h);

    parameters_plus_h[i] -= two_h;
    const Scalar f_minus = functor(parameters_plus_h);

    gradient[i] = (f_plus - f_minus) / two_h;
  }

  return gradient;
}




template <class CostFunctor, class DerivedParams, class Scalar>
Scalar computeGradientAtIndex(const CostFunctor& functor,
                              const Eigen::MatrixBase<DerivedParams>& x,
                              const Eigen::Index param_index,
                              const Scalar h) {
  using Vector = typename DerivedParams::PlainObject;
  Vector x_plus_h = x;

  x_plus_h[param_index] += h;
  const Scalar f_plus = functor(x_plus_h);

  const Scalar two_h = 2 * h;
  x_plus_h[param_index] -= two_h;
  const Scalar f_minus = functor(x_plus_h);

  return (f_plus - f_minus) / two_h;
}

template <class CostFunctor, class DerivedParams, class Scalar>
Scalar computeRiddersGradSingleParam(const CostFunctor& functor,
                                   const Eigen::MatrixBase<DerivedParams>& parameters,
                                   const Eigen::Index param_index,
                                   const Scalar delta) {

  Scalar grad;

  // Maximal number of adaptive extrapolations (sampling) in Ridders' method.
  const Eigen::Index max_num_ridders_extrapolations =  10;

  // Convergence criterion on extrapolation error for Ridders adaptive
  // differentiation. The available error estimation methods are defined in
  // NumericDiffErrorType and set in the "ridders_error_method" field.
  const Scalar ridders_epsilon =  1e-12;

  // The factor in which to shrink the step size with each extrapolation in
  // Ridders' method.
  const Scalar ridders_step_shrink_factor = 2.0;


  // In order for the algorithm to converge, the step size should be
  // initialized to a value that is large enough to produce a significant
  // change in the function.
  // As the derivative is estimated, the step size decreases.
  // By default, the step sizes are chosen so that the middle column
  // of the Romberg tableau uses the input delta.
  Scalar current_step_size = delta * pow(ridders_step_shrink_factor, max_num_ridders_extrapolations / 2);


  using VectorX = Eigen::Matrix<Scalar, Eigen::Dynamic, 1>;

  // Double-buffering temporary differential candidate vectors
  // from previous step size.
  VectorX current_candidates(max_num_ridders_extrapolations);
  VectorX previous_candidates(max_num_ridders_extrapolations);

  // Represents the computational error of the derivative. This variable is
  // initially set to a large value, and is set to the difference between
  // current and previous finite difference extrapolations.
  // norm_error is supposed to decrease as the finite difference tableau
  // generation progresses, serving both as an estimate for differentiation
  // error and as a measure of differentiation numerical stability.
  Scalar norm_error = std::numeric_limits<double>::max();


  // Loop over decreasing step sizes until:
  //  1. Error is smaller than a given value (ridders_epsilon),
  //  2. Maximal order of extrapolation reached, or
  //  3. Extrapolation becomes numerically unstable.
  for (Eigen::Index i = 0; i < max_num_ridders_extrapolations; ++i) {

    // Compute the numerical derivative at this step size
    current_candidates[0] = computeGradientAtIndex(functor, parameters, param_index, current_step_size);

    // Store initial results.
    if (i == 0) {
      grad = current_candidates[0];
    }

    // Shrink differentiation step size.
    current_step_size /= ridders_step_shrink_factor;

    // Extrapolation factor for Richardson acceleration method (see below).
    Scalar richardson_factor = ridders_step_shrink_factor * ridders_step_shrink_factor;

    for (int k = 1; k <= i; ++k) {
      // Extrapolate the various orders of finite differences using
      // the Richardson acceleration method.

      current_candidates[k] =
          (richardson_factor * current_candidates[k - 1] - previous_candidates[k - 1]) / (richardson_factor - 1.0);


      richardson_factor *= ridders_step_shrink_factor * ridders_step_shrink_factor;

      // Compute the difference between the previous value and the current.
      Scalar candidate_error = std::max(
                  std::abs(current_candidates[k] - current_candidates[k - 1]),
                  std::abs(current_candidates[k] - previous_candidates[k - 1]));

      // If the error has decreased, update results.
      if (candidate_error <= norm_error) {
        norm_error = candidate_error;
        grad = current_candidates[k];

        // If the error is small enough, stop.
        if (norm_error < ridders_epsilon) {
          break;
        }
      }
    }

    // After breaking out of the inner loop, declare convergence.
    if (norm_error < ridders_epsilon) {
      break;
    }


    // Check to see if the current gradient estimate is numerically unstable.
    // If so, bail out and return the last stable result.
    if (i > 0) {
      Scalar tableau_error = std::abs(current_candidates[i] - previous_candidates[i - 1]);
      // Compare current error to the chosen candidate's error.
      if (tableau_error >= 2 * norm_error) {
        break;
      }
    }

    current_candidates.swap(previous_candidates);
//    std::swap(current_candidates, previous_candidates);
  }

  return grad;
}




template <class CostFunctor, class DerivedParams>
typename DerivedParams::PlainObject
numericDiffRidders(const CostFunctor& functor,
                   const Eigen::MatrixBase<DerivedParams>& parameters) {
  EIGEN_STATIC_ASSERT_VECTOR_ONLY(DerivedParams);

  using Scalar = typename DerivedParams::Scalar;
  using Vector = typename DerivedParams::PlainObject;

  Scalar min_step_size = std::sqrt(std::numeric_limits<Scalar>::epsilon());

  // Initial step size for Ridders adaptive numeric differentiation (multiplied
  // by parameter block's order of magnitude).
  // If parameters are close to zero, Ridders' method sets the step size
  // directly to this value. This parameter is separate from
  // "relative_step_size" in order to set a different default value.
  //
  // Note: For Ridders' method to converge, the step size should be initialized
  // to a value that is large enough to produce a significant change in the
  // function. As the derivative is estimated, the step size decreases.
  const Scalar ridders_relative_initial_step_size = 1e-2;

  // For Ridders' method, the initial step size is required to be large,
  // thus ridders_relative_initial_step_size is used.
  min_step_size = std::max(min_step_size, ridders_relative_initial_step_size);

  Vector gradient;
  gradient.resize(parameters.size());

  for (Eigen::Index i = 0; i < gradient.size(); ++i) {

    const Scalar step_size = std::abs(parameters[i]) * ridders_relative_initial_step_size;
    const Scalar delta = std::max(min_step_size, step_size);

    gradient[i] = computeRiddersGradSingleParam(functor, parameters, i, delta);
  }

  return gradient;
}

}  // namespace ShapeSpace

#endif // end SHAPESPACE_NUMERICDIFF_H_
