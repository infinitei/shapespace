/**
 * @file MarchingCubes.h
 * @brief MarchingCubes
 *
 * @author Abhijit Kundu
 */

#ifndef SHAPESPACE_MARCHINGCUBES_H_
#define SHAPESPACE_MARCHINGCUBES_H_

#include "ShapeSpace/MarchingCubeTables.h"
#include <Eigen/CXX11/Tensor>
#include <vector>
#include <array>
#include <map>

namespace ShapeSpace {

template<class Index>
std::tuple<Index, Index, Index> tie(const std::array<Index, 3>& key) {
  return std::tie(key[0], key[1], key[2]);
}

template<class KeyType>
struct KeyEdge {
  KeyEdge(const KeyType& key0, const KeyType& key1) {
    if (tie(key0) < tie(key1)) {
      key0_ = key0;
      key1_ = key1;
    } else {
      key0_ = key1;
      key1_ = key0;
    }
  }

  bool operator<(const KeyEdge& rhs) const {
    if (key0_ != rhs.key0_)
      return (tie(key0_) < tie(rhs.key0_));
    else
      return (tie(key1_) < tie(rhs.key1_));
  }

 private:
  KeyType key0_, key1_;

};



template<class KeyType>
std::array<KeyType, 8> getNeighborKeys(const KeyType& key) {
  return std::array<KeyType, 8> { {
    key,
    KeyType({{key[0] + 1, key[1],     key[2]}}),
    KeyType({{key[0] + 1, key[1] + 1, key[2]}}),
    KeyType({{key[0],     key[1] + 1, key[2]}}),
    KeyType({{key[0],     key[1],     key[2] + 1}}),
    KeyType({{key[0] + 1, key[1],     key[2] + 1}}),
    KeyType({{key[0] + 1, key[1] + 1, key[2] + 1}}),
    KeyType({{key[0],     key[1] + 1, key[2] + 1}})
  } };
}

template<class MapType>
class MarchingCubes {
  using KeyScalar = typename MapType::Index;
  using KeyType = std::array<KeyScalar, MapType::NumIndices>;
  using KeyEdgeType = KeyEdge<KeyType>;
  using KeyPairToIndexMap = std::map<KeyEdgeType, std::size_t>;

 public:
  template<class KeyToCoord, class VertexType, class IndexType>
  MarchingCubes(const MapType& map, const KeyToCoord& key_to_coord, std::vector<VertexType>& vertices, std::vector<IndexType>& faces) {

    for (Eigen::Index z = 1; z < (map.dimension(2) - 1); ++z)
      for (Eigen::Index y = 1; y < (map.dimension(1) - 1); ++y)
        for (Eigen::Index x = 1; x < (map.dimension(0) - 1); ++x) {

          const KeyType key = { x, y, z };

          std::array<KeyType, 8> grid_keys = getNeighborKeys(key);
          std::array<float, 8> values;
          {
            for (int i = 0; i < 8; ++i) {
              values[i] = map(grid_keys[i]);
            }

            // bit representation of cube type
            unsigned char cube_type(0);

            // set cube type
            for (int i = 0; i < 8; ++i)
              if (values[i] > 0.0)
                cube_type |= (1 << i);

            // cube doesnot have any iso surface
            if (cube_type == 0 || cube_type == 255)
              continue;

            std::array<std::size_t, 12> vertex_index;

            // Find the vertices where the surface intersects the cube
            if (edgeTable[cube_type] & 1)
              vertex_index[0] = addVertex(key_to_coord, grid_keys[0], grid_keys[1], values[0], values[1], vertices);
            if (edgeTable[cube_type] & 2)
              vertex_index[1] = addVertex(key_to_coord, grid_keys[1], grid_keys[2], values[1], values[2], vertices);
            if (edgeTable[cube_type] & 4)
              vertex_index[2] = addVertex(key_to_coord, grid_keys[3], grid_keys[2], values[3], values[2], vertices);
            if (edgeTable[cube_type] & 8)
              vertex_index[3] = addVertex(key_to_coord, grid_keys[0], grid_keys[3], values[0], values[3], vertices);
            if (edgeTable[cube_type] & 16)
              vertex_index[4] = addVertex(key_to_coord, grid_keys[4], grid_keys[5], values[4], values[5], vertices);
            if (edgeTable[cube_type] & 32)
              vertex_index[5] = addVertex(key_to_coord, grid_keys[5], grid_keys[6], values[5], values[6], vertices);
            if (edgeTable[cube_type] & 64)
              vertex_index[6] = addVertex(key_to_coord, grid_keys[7], grid_keys[6], values[7], values[6], vertices);
            if (edgeTable[cube_type] & 128)
              vertex_index[7] = addVertex(key_to_coord, grid_keys[4], grid_keys[7], values[4], values[7], vertices);
            if (edgeTable[cube_type] & 256)
              vertex_index[8] = addVertex(key_to_coord, grid_keys[0], grid_keys[4], values[0], values[4], vertices);
            if (edgeTable[cube_type] & 512)
              vertex_index[9] = addVertex(key_to_coord, grid_keys[1], grid_keys[5], values[1], values[5], vertices);
            if (edgeTable[cube_type] & 1024)
              vertex_index[10] = addVertex(key_to_coord, grid_keys[2], grid_keys[6], values[2], values[6], vertices);
            if (edgeTable[cube_type] & 2048)
              vertex_index[11] = addVertex(key_to_coord, grid_keys[3], grid_keys[7], values[3], values[7], vertices);

            // create the triangles
            for (int i = 0; triTable[cube_type][0][i] != -1; i += 3) {
              faces.emplace_back(vertex_index[triTable[cube_type][0][i]]);
              faces.emplace_back(vertex_index[triTable[cube_type][0][i + 1]]);
              faces.emplace_back(vertex_index[triTable[cube_type][0][i + 2]]);
            }

          }  // end of current cube (grid)

        }  // end of map iteration
  }

  template<class KeyToCoord, class VertexType, class ValueScalar>
  std::size_t addVertex(const KeyToCoord& key_to_coord,
                        const KeyType& key0,
                        const KeyType& key1,
                        const ValueScalar value0,
                        const ValueScalar value1,
                        std::vector<VertexType>& vertices) {

    KeyEdgeType edge(key0, key1);

    auto it = edge_to_index_.find(edge);
    if (it != edge_to_index_.end())
      return it->second;

    using PositionType = typename VertexType::PositionType;
    PositionType p0 = key_to_coord(key0);
    PositionType p1 = key_to_coord(key1);

    ValueScalar s0 = std::abs(value0);
    ValueScalar s1 = std::abs(value1);
    ValueScalar t = s0 / (s0 + s1);

    VertexType vertex;
    vertex.position = (1.0f - t) * p0 + t * p1;

    vertices.push_back(vertex);
    edge_to_index_[edge] = vertices.size() - 1;

    return vertices.size() - 1;
  }

 private:
  KeyPairToIndexMap edge_to_index_;
};


template<class MapType, class KeyToCoord, class VertexType, class IndexType>
void runMarchingCubes(const MapType& map, const KeyToCoord& key_to_coord, std::vector<VertexType>& vertices, std::vector<IndexType>& faces) {
  MarchingCubes<MapType> marching_cubes(map, key_to_coord, vertices, faces);
}

template<class MapType, class KeyToCoord, class VertexType, class Derived>
void runMarchingCubes(const MapType& map, const KeyToCoord& key_to_coord, std::vector<VertexType>& vertices, Eigen::MatrixBase<Derived> const & faces_) {
  static_assert(Derived::ColsAtCompileTime == 3, "faces should be 3 column matrix/array only");

  using IndexType = typename Derived::Scalar;
  std::vector<IndexType> face_indices;
  MarchingCubes<MapType> marching_cubes(map, key_to_coord, vertices, face_indices);

  const Eigen::Index num_of_faces = face_indices.size() / 3;
  using FacesContainer = Eigen::MatrixBase<Derived>;
  FacesContainer& faces = const_cast<FacesContainer& >(faces_);
  faces.derived().resize(num_of_faces, 3);

  for(Eigen::Index i = 0; i < num_of_faces; ++i) {
    faces(i, 0) = face_indices[3*i];
    faces(i, 1) = face_indices[3*i + 1];
    faces(i, 2) = face_indices[3*i + 2];
  }
}

}  // namespace ShapeSpace

#endif // end SHAPESPACE_MARCHINGCUBES_H_
