/**
 * @file TSDFIO.cpp
 * @brief TSDFIO
 *
 * @author Abhijit Kundu
 */

#include "ShapeSpace/TSDFIO.h"
#include <fstream>
#include <iostream>
#include <Eigen/Core>

namespace ShapeSpace {

bool operator ==(const NRRD& lhs, const NRRD& rhs) {
  return std::tie(lhs.dimension, lhs.sizes, lhs.thicknesses) == std::tie(rhs.dimension, rhs.sizes, rhs.thicknesses);
}

bool operator !=(const NRRD& lhs, const NRRD& rhs) {
  return std::tie(lhs.dimension, lhs.sizes, lhs.thicknesses) != std::tie(rhs.dimension, rhs.sizes, rhs.thicknesses);
}

void saveTSDFasNRRD(const Eigen::Tensor<float, 3>& tsdf, const std::string& filename, const double resolution) {
  std::ofstream ofs(filename.c_str(), std::ios::out);

  if (!ofs.is_open())
    throw std::runtime_error("saveTSDFasNRRD: Cannot save at " + filename);


  ofs << "NRRD0001" << '\n'
      << "content: " << "volume" << '\n'
      << "type: " << "float" << '\n'
      << "dimension: " << "3" << '\n'
      << "sizes: " << tsdf.dimension(0) << " " << tsdf.dimension(1) << " " << tsdf.dimension(2) << '\n'
      << "spacings: " << "1 1 1" << '\n'
      << "thicknesses: " << resolution << " " << resolution << " " << resolution << '\n'
      << "encoding: " << "raw" << "\n\n";

  ofs.write((char *) tsdf.data(), sizeof(float) * tsdf.size());
  ofs.close();
}

void saveTSDFasNRRD(const Eigen::Tensor<unsigned char, 3>& tsdf, const std::string& filename, const double resolution) {
  std::ofstream ofs(filename.c_str(), std::ios::out);

  if (!ofs.is_open())
    throw std::runtime_error("saveTSDFasNRRD: Cannot save at " + filename);


  ofs << "NRRD0001" << '\n'
      << "content: " << "volume" << '\n'
      << "type: " << "unsigned char" << '\n'
      << "dimension: " << "3" << '\n'
      << "sizes: " << tsdf.dimension(0) << " " << tsdf.dimension(1) << " " << tsdf.dimension(2) << '\n'
      << "spacings: " << "1 1 1" << '\n'
      << "thicknesses: " << resolution << " " << resolution << " " << resolution << '\n'
      << "encoding: " << "raw" << "\n\n";

  ofs.write((char *) tsdf.data(), sizeof(unsigned char) * tsdf.size());
  ofs.close();
}

NRRD parseNRRDheader(std::istream &istream) {
  {
    std::string magic_token;
    std::getline(istream, magic_token);
    if (magic_token != "NRRD0001")
      throw std::runtime_error("parseNRRDheader: Expects only NRRD0001. Read token(" + magic_token + ").");
  }

  NRRD nrrd;

  for (std::string line; std::getline(istream, line);) {
    if (line.empty())
      break;

    std::istringstream iss(line);

    std::string keyword;
    iss >> keyword;

    if (keyword == "type:") {
      nrrd.type = line.substr(6,5);
    }
    else if (keyword == "dimension:") {
      iss >> nrrd.dimension;
    }
    else if (keyword == "sizes:") {
      nrrd.sizes.resize(nrrd.dimension, 1);
      for (int i = 0; i < nrrd.dimension; ++i)
        iss >> nrrd.sizes[i];
    }
    else if (keyword == "spacings:") {
      nrrd.spacings.resize(nrrd.dimension, 1);
      for (int i = 0; i < nrrd.dimension; ++i)
        iss >> nrrd.spacings[i];
    }
    else if (keyword == "thicknesses:") {
      nrrd.thicknesses.resize(nrrd.dimension, 1);
      for (int i = 0; i < nrrd.dimension; ++i)
        iss >> nrrd.thicknesses[i];
    }
    else if (keyword == "encoding:") {
      iss >> nrrd.encoding;
    }
  }

  return nrrd;
}

NRRD parseNRRDheader(const std::string& filename) {
  std::ifstream ifs(filename.c_str(), std::ios::in);

  if (!ifs.is_open()) {
    throw std::runtime_error("parseNRRDheader: Cannot read at " + filename);
  }

  NRRD nrrd = parseNRRDheader(ifs);
  ifs.close();

  return nrrd;
}

NRRD loadTSDFfromNRRD(const std::string& filename, Eigen::Tensor<float, 3>& tsdf) {
  std::ifstream ifs(filename.c_str(), std::ios::in);

  if (!ifs.is_open()) {
    throw std::runtime_error("loadTSDFfromNRRD: Cannot read at " + filename);
  }

  NRRD nrrd = parseNRRDheader(ifs);

  if (nrrd.type != "float")
    throw std::runtime_error("loadTSDFfromNRRD: this function only takes float type TSDF");

  if (nrrd.dimension != 3)
    throw std::runtime_error("loadTSDFfromNRRD: Only supports 3 dimensions");

  if (!nrrd.spacings.isConstant(1))
    throw std::runtime_error("loadTSDFfromNRRD: Only supports spacings of 1 1 1");

  if (nrrd.encoding != "raw")
    throw std::runtime_error("loadTSDFfromNRRD: Only supports raw encoding");


  tsdf.resize(nrrd.sizes.x(), nrrd.sizes.y(), nrrd.sizes.z());

  ifs.read((char *) tsdf.data(), sizeof(float) * tsdf.size());
  ifs.close();

  return nrrd;
}

NRRD loadTSDFfromNRRD(const std::string& filename, Eigen::Tensor<unsigned char, 3>& tsdf) {
  std::ifstream ifs(filename.c_str(), std::ios::in);

  if (!ifs.is_open()) {
    throw std::runtime_error("loadTSDFfromNRRD: Cannot read at " + filename);
  }

  NRRD nrrd = parseNRRDheader(ifs);

  if (nrrd.type != "unsigned char")
    throw std::runtime_error("loadTSDFfromNRRD: this function only takes unsigned char type TSDF");

  if (nrrd.dimension != 3)
    throw std::runtime_error("loadTSDFfromNRRD: Only supports 3 dimensions");

  if (!nrrd.spacings.isConstant(1))
    throw std::runtime_error("loadTSDFfromNRRD: Only supports spacings of 1 1 1");

  if (nrrd.encoding != "raw")
    throw std::runtime_error("loadTSDFfromNRRD: Only supports raw encoding");


  tsdf.resize(nrrd.sizes.x(), nrrd.sizes.y(), nrrd.sizes.z());

  ifs.read((char *) tsdf.data(), sizeof(unsigned char) * tsdf.size());
  ifs.close();

  return nrrd;
}

NRRD loadTSDFfromNRRD(const std::string& filename, float * tsdf_data, const NRRD& expected_nrrd) {
  std::ifstream ifs(filename.c_str(), std::ios::in);

  if (!ifs.is_open()) {
    throw std::runtime_error("loadTSDFfromNRRD: Cannot read at " + filename);
  }

  NRRD nrrd = parseNRRDheader(ifs);

  if (nrrd.type != "float")
    throw std::runtime_error("loadTSDFfromNRRD: this function only takes float type TSDF");

  if (nrrd.dimension != 3)
    throw std::runtime_error("loadTSDFfromNRRD: Only supports 3 dimensions");

  if (!nrrd.spacings.isConstant(1))
    throw std::runtime_error("loadTSDFfromNRRD: Only supports spacings of 1 1 1");

  if (nrrd.encoding != "raw")
    throw std::runtime_error("loadTSDFfromNRRD: Only supports raw encoding");

  if(expected_nrrd != nrrd) {
     throw std::runtime_error("loadTSDFfromNRRD: Does not meet expected nrrd for " + filename);
  }

  ifs.read((char *) tsdf_data, sizeof(float) * nrrd.sizes.prod());
  ifs.close();

  return nrrd;
}

NRRD loadTSDFfromNRRD(const std::string& filename, float* tsdf_data, const Eigen::Index expected_size) {
  std::ifstream ifs(filename.c_str(), std::ios::in);

  if (!ifs.is_open()) {
    throw std::runtime_error("loadTSDFfromNRRD: Cannot read at " + filename);
  }

  NRRD nrrd = parseNRRDheader(ifs);

  if (nrrd.type != "float")
    throw std::runtime_error("loadTSDFfromNRRD: this function only takes float type TSDF");

  if (nrrd.dimension != 3)
    throw std::runtime_error("loadTSDFfromNRRD: Only supports 3 dimensions");

  if (!nrrd.spacings.isConstant(1))
    throw std::runtime_error("loadTSDFfromNRRD: Only supports spacings of 1 1 1");

  if (nrrd.encoding != "raw")
    throw std::runtime_error("loadTSDFfromNRRD: Only supports raw encoding");

  if(expected_size) {
    if (nrrd.sizes.prod() != expected_size)
      throw std::runtime_error("loadTSDFfromNRRD: Does not meet expected size for " + filename);
  }

  ifs.read((char *) tsdf_data, sizeof(float) * nrrd.sizes.prod());
  ifs.close();

  return nrrd;
}

NRRD loadTSDFfromNRRD(const std::string& filename, unsigned char* tsdf_data, const Eigen::Index expected_size) {
  std::ifstream ifs(filename.c_str(), std::ios::in);

  if (!ifs.is_open()) {
    throw std::runtime_error("loadTSDFfromNRRD: Cannot read at " + filename);
  }

  NRRD nrrd = parseNRRDheader(ifs);

  if (nrrd.type != "unsigned char")
    throw std::runtime_error("loadTSDFfromNRRD: this function only takes float type TSDF");

  if (nrrd.dimension != 3)
    throw std::runtime_error("loadTSDFfromNRRD: Only supports 3 dimensions");

  if (!nrrd.spacings.isConstant(1))
    throw std::runtime_error("loadTSDFfromNRRD: Only supports spacings of 1 1 1");

  if (nrrd.encoding != "raw")
    throw std::runtime_error("loadTSDFfromNRRD: Only supports raw encoding");

  if(expected_size) {
    if (nrrd.sizes.prod() != expected_size)
      throw std::runtime_error("loadTSDFfromNRRD: Does not meet expected size for" + filename);
  }

  ifs.read((char *) tsdf_data, sizeof(unsigned char) * nrrd.sizes.prod());
  ifs.close();

  return nrrd;
}


}  // namespace ShapeSpace
