set(_HDRS
  EigenTypedefs.h
  H5EigenBase.h
  H5EigenDense.h
  H5EigenSparse.h
  H5EigenTensor.h
  H5StdContainers.h
  ImageUtils.h
  KeyCoordTransformer.h
  MarchingCubes.h
  MarchingCubeTables.h
  MultivariateNormal.h
  NumericDiff.h
  ShapeSliders.h
  TSDFIO.h
  TSDFPCAmodel.h
)

set(_SRCS
  H5StdContainers.cpp
  ImageUtils.cpp
  ShapeSliders.cpp
  TSDFIO.cpp
  TSDFPCAmodel.cpp
)

if(WITH_SMPL)
  list(APPEND _HDRS 
    SMPLMeshRenderer.h
    SMPLmodel.h
    )
  list(APPEND _SRCS
    SMPLMeshRenderer.cpp
    SMPLmodel.cpp
    )
endif()

########################### Add Target ################################

# Instruct CMake to run moc automatically when needed.
SET(CMAKE_AUTOMOC ON)

ADD_LIBRARY(ShapeSpace ${_SRCS} ${_HDRS})
TARGET_LINK_LIBRARIES(ShapeSpace
  ${Boost_LIBRARIES}
  ${HDF5_LIBRARIES}
  CuteGL::Surface
  Qt5::Widgets
  ${OpenCV_LIBS}
  )

TARGET_INCLUDE_DIRECTORIES(ShapeSpace
  PUBLIC
    ${EIGEN_INCLUDE_DIRS}
    ${Boost_INCLUDE_DIRS}
    ${HDF5_INCLUDE_DIRS}
 )

TARGET_INCLUDE_DIRECTORIES(ShapeSpace
  INTERFACE
    $<BUILD_INTERFACE:${CMAKE_SOURCE_DIR}>
    $<INSTALL_INTERFACE:${INSTALL_INCLUDE_DIR}>
  PRIVATE
    ${CMAKE_SOURCE_DIR}
 )