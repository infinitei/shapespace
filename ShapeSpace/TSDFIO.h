/**
 * @file TSDFIO.h
 * @brief TSDFIO
 *
 * @author Abhijit Kundu
 */

#ifndef SHAPESPACE_TSDFIO_H_
#define SHAPESPACE_TSDFIO_H_

#include <Eigen/CXX11/Tensor>
#include <string>

namespace ShapeSpace {

struct NRRD {
  std::string type;
  std::string encoding;
  int dimension;
  Eigen::Matrix<Eigen::Index, Eigen::Dynamic, 1> sizes;
  Eigen::Matrix<Eigen::Index, Eigen::Dynamic, 1> spacings;
  Eigen::Matrix<double, Eigen::Dynamic, 1> thicknesses;
};

bool operator==(const NRRD& lhs, const NRRD& rhs);
bool operator !=(const NRRD& lhs, const NRRD& rhs);

void saveTSDFasNRRD(const Eigen::Tensor<float, 3>& tsdf, const std::string& filename, const double resolution = 1.0);
void saveTSDFasNRRD(const Eigen::Tensor<unsigned char, 3>& tsdf, const std::string& filename, const double resolution = 1.0);

NRRD parseNRRDheader(const std::string& filename);

NRRD loadTSDFfromNRRD(const std::string& filename, Eigen::Tensor<float, 3>& tsdf);
NRRD loadTSDFfromNRRD(const std::string& filename, Eigen::Tensor<unsigned char, 3>& tsdf);

NRRD loadTSDFfromNRRD(const std::string& filename, float * tsdf_data, const NRRD& expected_nrrd);
NRRD loadTSDFfromNRRD(const std::string& filename, float * tsdf_data, const Eigen::Index expected_size = 0);
NRRD loadTSDFfromNRRD(const std::string& filename, unsigned char * tsdf_data, const Eigen::Index expected_size = 0);

}  // namespace ShapeSpace


#endif // end SHAPESPACE_TSDFIO_H_
