/**
 * @file ShapeSliders.h
 * @brief ShapeSliders
 *
 * @author Abhijit Kundu
 */

#ifndef SHAPESPACE_SHAPESLIDERS_H_
#define SHAPESPACE_SHAPESLIDERS_H_

#include <QGroupBox>
#include <QSlider>
#include <memory>
#include <Eigen/Core>

class ShapeSliders : public QGroupBox {
Q_OBJECT

 public:
  ShapeSliders(const std::size_t dimension, const QString &title, QWidget *parent = 0);

  Eigen::VectorXi value() const;

 signals:
  void valueChanged();

 private:
  std::vector<std::unique_ptr<QSlider>> sliders_;
};

#endif // end SHAPESPACE_SHAPESLIDERS_H_
