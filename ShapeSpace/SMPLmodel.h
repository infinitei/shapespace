/**
 * @file SMPLmodel.h
 * @brief SMPLmodel
 *
 * @author Abhijit Kundu
 */

#ifndef SHAPESPACE_SMPLMODEL_H_
#define SHAPESPACE_SMPLMODEL_H_

#include "ShapeSpace/EigenTypedefs.h"
#include <CuteGL/Core/SMPLData.h>

namespace ShapeSpace {

template <class PositionScalar_ = float, class IndexScalar_ = unsigned int>
class SMPLmodel : public CuteGL::SMPLData<PositionScalar_, IndexScalar_> {
 public:
  SMPLmodel(const std::string& smpl_model_h5_file);

  using SMPLDataType = CuteGL::SMPLData<PositionScalar_, IndexScalar_>;

  using PositionScalar = typename SMPLDataType::PositionScalar;
  using IndexScalar = typename SMPLDataType::IndexScalar;
  using MatrixX1u = typename SMPLDataType::MatrixX1u;
  using MatrixX3u = typename SMPLDataType::MatrixX3u;
  using MatrixX3 = typename SMPLDataType::MatrixX3;
  using MatrixXX = typename SMPLDataType::MatrixXX;
  using Tensor3 = typename SMPLDataType::Tensor3;
  using Tensor2 = typename SMPLDataType::Tensor2;
  using Tensor1 = typename SMPLDataType::Tensor1;

  template<class DerivedPose, class DerivedShape>
  MatrixX3 computeVertices(const Eigen::DenseBase<DerivedPose>& pose_coeffs,
                           const Eigen::DenseBase<DerivedShape>& shape_coeffs) const;

  template<class DerivedPose, class DerivedShape>
  void computeVertices(const Eigen::DenseBase<DerivedPose>& pose_coeffs,
                       const Eigen::DenseBase<DerivedShape>& shape_coeffs,
                       const MatrixX3& posed_vertices, const MatrixX3& joint_positions) const;

  template<class DerivedPose, class DerivedShape>
  MatrixX3 computePosedJoints(const Eigen::DenseBase<DerivedPose>& pose_coeffs,
                              const Eigen::DenseBase<DerivedShape>& shape_coeffs) const;

  using SMPLDataType::parents;
  using SMPLDataType::faces;
  using SMPLDataType::template_vertices;

  using SMPLDataType::blend_weights;
  using SMPLDataType::joint_regressor;
  using SMPLDataType::pose_displacements;
  using SMPLDataType::shape_displacements;

};

}  // end namespace ShapeSpace

#include "ShapeSpace/SMPLmodel-impl.hpp"

#endif // end SHAPESPACE_SMPLMODEL_H_
