/**
 * @file visualizeTSDFviaMarchingCubes.cpp
 * @brief visualizeTSDFviaMarchingCubes
 *
 * @author Abhijit Kundu
 */

#include "ShapeSpace/MarchingCubes.h"
#include "ShapeSpace/KeyCoordTransformer.h"
#include "ShapeSpace/TSDFIO.h"

#include <CuteGL/Surface/WindowRenderViewer.h>
#include <CuteGL/Renderer/MultiObjectRenderer.h>
#include <CuteGL/Geometry/ComputeAlignedBox.h>
#include <CuteGL/Geometry/ComputeNormals.h>
#include <CuteGL/IO/ImportPLY.h>

#include <QGuiApplication>
#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include <memory>
#include <iostream>

int main(int argc, char **argv) {
  std::cout.imbue(std::locale(""));
  namespace po = boost::program_options;
  namespace fs = boost::filesystem;

  po::options_description generic_options("Generic Options");
    generic_options.add_options()("help,h", "Help screen");

  po::options_description config_options("Config");
  config_options.add_options()
      ("tsdf_file",  po::value<std::string>()->required(), "Path to the Input TSDF file")
      ;

  po::options_description cmdline_options;
  cmdline_options.add(generic_options).add(config_options);

  po::positional_options_description positional_options;
  positional_options.add("tsdf_file", 1);

  po::variables_map vm;

  try {
    po::store(
        po::command_line_parser(argc, argv).options(cmdline_options).positional(positional_options).run(),
        vm);
    po::notify(vm);
  } catch (const po::error &ex) {
    std::cerr << ex.what() << '\n';
    std::cout << cmdline_options << '\n';
    return EXIT_FAILURE;
  }

  if (vm.count("help")) {
    std::cout << cmdline_options << '\n';
    return EXIT_SUCCESS;
  }

  fs::path tsdf_file(vm.at("tsdf_file").as<std::string>());

  using Tensor3f = Eigen::Tensor<float, 3>;
  Tensor3f tsdf;
  ShapeSpace::NRRD nrrd = ShapeSpace::loadTSDFfromNRRD(tsdf_file.string(), tsdf);

  if (!nrrd.thicknesses.isConstant(nrrd.thicknesses.x())) {
    std::cout << "Does not support TSDF with non-uniform thickness\n";
    std::cout << "nrrd.thicknesses = " << nrrd.thicknesses.transpose() << "\n";
    return EXIT_FAILURE;
  }

  const Tensor3f::Dimensions tsdf_dim = tsdf.dimensions();
  const std::string tsdf_dim_str = std::to_string(tsdf_dim[0]) + "x" + std::to_string(tsdf_dim[1]) + "x" + std::to_string(tsdf_dim[2]);
  std::cout << "tsdf_dim = " << tsdf_dim_str << "\n";

  const float resolution = nrrd.thicknesses.x();
  const Eigen::Vector3f bbx_min = - resolution * Eigen::Vector3f(tsdf_dim[0], tsdf_dim[1], tsdf_dim[2]) / 2;

  const ShapeSpace::KeyToCoord<3, float> key_to_coord(bbx_min, resolution);

  std::cout << "tsdf.minimum() = " << tsdf.minimum() << "\n";
  std::cout << "tsdf.maximum() = " << tsdf.maximum() << "\n";

  CuteGL::MeshData md;

  std::cout << "Marching Cubes .. " << std::flush;
  ShapeSpace::runMarchingCubes(tsdf, key_to_coord, md.vertices, md.faces);
  std::cout << " Done" << std::endl;

  // Compute Normals
 {
    std::cout << "Computing Normals (Per Face) .. " << std::flush;
    CuteGL::computeNormals(md);
    std::cout << " Done" << std::endl;
  }

  std::cout << "Coloring Mesh (with default color).. " << std::flush;
  for (CuteGL::MeshData::VertexData& vd : md.vertices)
    vd.color = CuteGL::MeshData::ColorType(180, 180, 180, 255);
  std::cout << " Done" << std::endl;

  std::cout << "md.vertices.size() = " << md.vertices.size() << "\n";
  std::cout << "md.faces.rows() = " << md.faces.rows() << "\n";

  {
    const std::string mesh_ply_filename = tsdf_file.stem().string() + ".ply";
    std::cout << "Saving Mesh " << std::flush;
    CuteGL::saveMeshAsBinPly(md, mesh_ply_filename);
    std::cout << " Done. Saved at "<< mesh_ply_filename << std::endl;
  }



  QGuiApplication app(argc, argv);

  // Create the renderer
  std::unique_ptr<CuteGL::MultiObjectRenderer> renderer(new CuteGL::MultiObjectRenderer());

  // Instantiate the viewers
  CuteGL::WindowRenderViewer viewer(renderer.get());
  viewer.setBackgroundColor(55, 55, 55);
  viewer.resize(1024, 768);
  viewer.showAndWaitTillExposed();

  renderer->modelDrawers().addItem(Eigen::Affine3f::Identity(), md);

  Eigen::AlignedBox3f bbx = computeAlignedBox(md);

  const Eigen::IOFormat vecfmt(Eigen::StreamPrecision, Eigen::DontAlignCols, ", ", ", ", "", "", "[", "]");
  std::cout << "Mesh BBX Extends: " << bbx.min().format(vecfmt) << " ---- " << bbx.max().format(vecfmt) << "\n";
  std::cout << "Mesh BBX Diagonal Length: " << bbx.diagonal().norm() << "\n";

  viewer.setSceneBoundingBox(bbx);
  viewer.showEntireScene();

  return app.exec();
}
