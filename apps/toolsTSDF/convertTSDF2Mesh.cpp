/**
 * @file convertTSDF2Mesh.cpp
 * @brief convertTSDF2Mesh
 *
 * @author Abhijit Kundu
 */

#include "ShapeSpace/MarchingCubes.h"
#include "ShapeSpace/KeyCoordTransformer.h"
#include "ShapeSpace/TSDFIO.h"

#include <CuteGL/IO/ImportPLY.h>
#include <CuteGL/Geometry/ComputeNormals.h>

#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include <memory>
#include <iostream>

int main(int argc, char **argv) {
  namespace po = boost::program_options;
  namespace fs = boost::filesystem;

  po::options_description generic_options("Generic Options");
      generic_options.add_options()("help,h", "Help screen");

  po::options_description config_opts("Config");
  config_opts.add_options()
      ("tsdf_file",  po::value<std::string>()->required(), "Path to the Input TSDF file")
      ;

  po::options_description cmdline_opts;
  cmdline_opts.add(generic_options).add(config_opts);

  po::positional_options_description pos_opts;
  pos_opts.add("tsdf_file", 1);

  po::variables_map vm;

  try {
    po::store(
        po::command_line_parser(argc, argv).options(cmdline_opts).positional(pos_opts).run(),
        vm);
    po::notify(vm);
  } catch (const po::error &ex) {
    std::cerr << ex.what() << '\n';
    std::cout << cmdline_opts << '\n';
    return EXIT_FAILURE;
  }

  if (vm.count("help")) {
    std::cout << cmdline_opts << '\n';
    return EXIT_SUCCESS;
  }

  fs::path tsdf_file(vm.at("tsdf_file").as<std::string>());

  using Tensor3f = Eigen::Tensor<float, 3>;
  Tensor3f tsdf;
  ShapeSpace::NRRD nrrd = ShapeSpace::loadTSDFfromNRRD(tsdf_file.string(), tsdf);

  if (!nrrd.thicknesses.isConstant(nrrd.thicknesses.x())) {
    std::cout << "Does not support TSDF with non-uniform thickness\n";
    std::cout << "nrrd.thicknesses = " << nrrd.thicknesses.transpose() << "\n";
    return EXIT_FAILURE;
  }

  const Tensor3f::Dimensions tsdf_dim = tsdf.dimensions();

  const float resolution = nrrd.thicknesses.x();
  const Eigen::Vector3f bbx_min = -resolution * Eigen::Vector3f(tsdf_dim[0], tsdf_dim[1], tsdf_dim[2]) / 2;

  const ShapeSpace::KeyToCoord<3, float> key_to_coord(bbx_min, resolution);

  CuteGL::MeshData md;
  ShapeSpace::runMarchingCubes(tsdf, key_to_coord, md.vertices, md.faces);

  // Compute Normals
  CuteGL::computeNormals(md);

  // Coloring Mesh (with default color)
  for (CuteGL::MeshData::VertexData& vd : md.vertices)
      vd.color = CuteGL::MeshData::ColorType(180, 180, 180, 255);

  {
    const std::string mesh_ply_filename = tsdf_file.stem().string() + ".ply";
    CuteGL::saveMeshAsBinPly(md, mesh_ply_filename);
  }

  return EXIT_SUCCESS;
}
