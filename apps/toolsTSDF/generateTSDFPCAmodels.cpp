/**
 * @file generateTSDFPCAmodels.cpp
 * @brief generateTSDFPCAmodels
 *
 * @author Abhijit Kundu
 */

#include "ShapeSpace/TSDFPCAmodel.h"
#include "ShapeSpace/KeyCoordTransformer.h"
#include "ShapeSpace/MarchingCubes.h"
#include <CuteGL/IO/ImportPLY.h>
#include <CuteGL/Core/MeshUtils.h>
#include <CuteGL/Utils/ColorUtils.h>
#include <CuteGL/Geometry/ComputeNormals.h>

#include <iostream>
#include <random>

int main(int argc, char **argv) {
  using namespace CuteGL;
  using namespace ShapeSpace;

  if (argc != 2) {
    std::cout << "ERROR: Incorrect number of arguments" << "\n";
    std::cout << "Example Usage: " << argv[0] << " /path/to/shape/file.h5";
    std::cout << "\n";
    return EXIT_FAILURE;
  }

  std::cout << "Loading TSDFPCAmodel from " << argv[1]  << std::flush;
  TSDFPCAmodel tsdf_pca(argv[1]);
  std::cout << " Done" << std::endl;

  std::random_device rd;  //Will be used to obtain a seed for the random number engine
  std::mt19937 gen(rd());  //Standard mersenne_twister_engine seeded with rd()
  std::uniform_real_distribution<float> hue_dist(0.0f, 1.0f);
  std::uniform_real_distribution<float> sat_dist(0.95f, 1.0f);
  std::uniform_real_distribution<float> val_dist(0.95f, 1.0f);

  using Tensor3 = TSDFPCAmodel::Tensor3;
  const Tensor3::Dimensions tsdf_dim = tsdf_pca.mean.dimensions();

  if (!tsdf_pca.thicknesses.isConstant(tsdf_pca.thicknesses.x())) {
    std::cout << "Does not support TSDF with non-uniform thickness\n";
    std::cout << "nrrd.thicknesses = " << tsdf_pca.thicknesses.transpose() << "\n";
    return EXIT_FAILURE;
  }

  const float resolution = tsdf_pca.thicknesses.x();
  const Eigen::Vector3f bbx_min = - resolution * Eigen::Vector3f(tsdf_dim[0], tsdf_dim[1], tsdf_dim[2]) / 2;

  const ShapeSpace::KeyToCoord<3, float> key_to_coord(bbx_min, resolution);

#pragma omp parallel for
  for (std::size_t i = 0; i < tsdf_pca.model_names.size(); ++i) {
    MeshData mesh;

    {
      Eigen::VectorXf shape_params = tsdf_pca.encoded_training_data.col(i);
      ShapeSpace::runMarchingCubes(tsdf_pca.computeTSDF(shape_params), key_to_coord, mesh.vertices, mesh.faces);
    }

    CuteGL::computeNormals(mesh);

    const float golden_ratio_conjugate = 0.618033988749895f;
    const float hue = 360.0f * std::fmod(hue_dist(gen) + golden_ratio_conjugate, 1.0f);
    const MeshData::ColorType color =  CuteGL::makeRGBAfromHSV(hue, sat_dist(gen), val_dist(gen));

    CuteGL::colorizeMesh(mesh, color);
    saveMeshAsBinPly(mesh, tsdf_pca.model_names[i] + ".ply");
  }
}



