/**
 * @file renderOSwithViewAndCrop.cpp
 * @brief renderOSwithViewAndCrop
 *
 * @author Abhijit Kundu
 */

#include <CuteGL/Core/PoseUtils.h>
#include <CuteGL/Geometry/ComputeAlignedBox.h>
#include <CuteGL/Renderer/MultiObjectRenderer.h>
#include <CuteGL/Surface/OffScreenRenderViewer.h>
#include <CuteGL/Core/PoseUtils.h>
#include <CuteGL/IO/ImportPLY.h>

#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include <QGuiApplication>
#include <QFileInfo>
#include <memory>
#include <iostream>

Eigen::Vector3f spherical2cartesian(float azimuth_deg, float elevation_deg, float radial) {
  const float pi_by_180 = M_PI / 180.0f;
  const float azimuth_rad = azimuth_deg * pi_by_180;
  const float elevation_rad = elevation_deg * pi_by_180;
  Eigen::Vector3f cart;
  cart.x() = (radial * std::cos(elevation_rad) * std::cos(azimuth_rad));
  cart.y() = (radial * std::cos(elevation_rad) * std::sin(azimuth_rad));
  cart.z() = (radial * std::sin(elevation_rad));
  return cart;
}

int main(int argc, char **argv) {
  using namespace CuteGL;
  namespace po = boost::program_options;
  namespace fs = boost::filesystem;

  float azimuth, elevation, tilt, distance;


  po::options_description generic_options("Generic Options");
    generic_options.add_options()("help,h", "Help screen");

  po::options_description config_options("Config");
    config_options.add_options()
        ("model,m",  po::value<std::string>()->required(), "Path to ply model")
        ("azimuth,a",  po::value<float>(&azimuth)->default_value(0.0f), "Azimuth in Degrees)")
        ("elevation,e",  po::value<float>(&elevation)->default_value(0.0f), "Elevation in Degrees)")
        ("tilt,t",  po::value<float>(&tilt)->default_value(0.0f), "Tilt in Degrees)")
        ("distance,d",  po::value<float>(&distance)->default_value(3.0f), "Distance to object center)")
        ("output,o",  po::value<std::string>(), "Output rendered image filename")
        ;

  po::options_description cmdline_options;
  cmdline_options.add(generic_options).add(config_options);

  po::variables_map vm;

  try {
    po::store(po::command_line_parser(argc, argv).options(cmdline_options).run(), vm);
    po::notify(vm);
  } catch (const po::error &ex) {
    std::cerr << ex.what() << '\n';
    std::cout << cmdline_options << '\n';
    return EXIT_FAILURE;
  }

  if (vm.count("help")) {
    std::cout << cmdline_options << '\n';
    return EXIT_SUCCESS;
  }

  // Get the input args in vector of strings
  const fs::path model_file(vm["model"].as<std::string>());
  const fs::path render_output_file = vm.count("output") ? vm["output"].as<std::string>() : model_file.filename().replace_extension("png");

  std::cout << "Will load PLY file from " << model_file << "\n";
  std::cout << "Will save rendered image at " << render_output_file << "\n";

  int fake = 0;
  QGuiApplication app(fake, argv);


  // Create the Renderer
  std::unique_ptr<MultiObjectRenderer> renderer(new MultiObjectRenderer());
  renderer->setDisplayGrid(false);
  renderer->setDisplayAxis(false);

  // Set the viewer up
  OffScreenRenderViewer viewer(renderer.get());
  viewer.setBackgroundColor(0, 0, 0, 0);
  QSurfaceFormat format = viewer.format();
  format.setSamples(0);
  viewer.setFormat(format);


  const float window_width = 224;
  const float window_height = 224;

  viewer.resize(window_width, window_height);

  // Set camera intrinsics
  viewer.camera().intrinsics() = CuteGL::getGLPerspectiveProjection(1050.0f, 1050.0f, 0.0f, 480.0f, 270.0f, 960, 540, 0.1f, 100.0f);

  // Compute extrinsics from viewpoint
  const float pi_by_180 = M_PI / 180.0f;
  viewer.camera().extrinsics() = CuteGL::getExtrinsicsFromViewPoint(
      azimuth * pi_by_180,
      elevation * pi_by_180,
      tilt * pi_by_180,
      distance);

  // iTc= BoundingBox(translation=[ 0.33125     0.37777778], scale=[ 0.42395833  0.37962963])
  // OpenGL uses lower-left as image origin instead of top left

  Eigen::Affine2f iTc = Eigen::Translation2f(0.33125f, 1.0f - 0.37777778f - 0.37962963f) * Eigen::Scaling(0.42395833f, 0.37962963f);
  Eigen::Affine2f cTi = iTc.inverse();
  Eigen::Affine2f sTo = Eigen::Scaling(window_width, window_height) * cTi;

  const Eigen::Vector2f viewport_xy =  (sTo * Eigen::Vector2f::Zero());
  const Eigen::Vector2f viewport_wh =  (sTo * Eigen::Vector2f::Ones()) - viewport_xy;
  viewer.setViewPort(viewport_xy.x(), viewport_xy.y(), viewport_wh.x(), viewport_wh.y());


  viewer.create();
  viewer.makeCurrent();

  {
    MeshData mesh = loadMeshFromPLY(model_file.string());
    renderer->modelDrawers().addItem(Eigen::Affine3f::Identity(), mesh);
  }

  viewer.render();
  QImage image = viewer.readColorBuffer();
  image.save(render_output_file.c_str());

  return EXIT_SUCCESS;
}







