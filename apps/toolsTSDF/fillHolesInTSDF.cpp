/**
 * @file fillHolesInTSDF.cpp
 * @brief fillHolesInTSDF
 *
 * @author Abhijit Kundu
 */

#include "ShapeSpace/TSDFIO.h"

#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include <iostream>
#include <queue>

void floodfill(Eigen::Tensor<float, 3>& tsdf, Eigen::Index y) {
  using Loc = Eigen::Matrix<int, 2, 1>;
  std::queue<Loc> locs;
  locs.emplace(0, 0);

  Eigen::MatrixXi ff_canvas(tsdf.dimension(2), tsdf.dimension(0));
  ff_canvas.setZero();
  ff_canvas(0, 0) = 1;

  while (!locs.empty()) {
    Loc center = locs.front();
    locs.pop();

    for (int dz : {-1, 0, 1})
      for (int dx : {-1, 0, 1}) {
        if (dz==0  && dx ==0)
          continue;
        Loc coord = center + Loc(dz, dx);
        if (coord[0] < 0 || coord[0] >= tsdf.dimension(2))
          continue;
        if (coord[1] < 0 || coord[1] >= tsdf.dimension(0))
          continue;

        if (ff_canvas(coord[0], coord[1])==0 && tsdf(coord[1], y, coord[0]) > 0.0f) {
          ff_canvas(coord[0], coord[1]) = 1;
          locs.emplace(coord);
        }
      }
  }

  for (Eigen::Index z = 0; z < tsdf.dimension(2); ++z)
    for (Eigen::Index x = 0; x < tsdf.dimension(0); ++x) {
      if (ff_canvas(z, x)==0 && tsdf(x, y, z) > 0.0f) {
        tsdf(x, y, z) = -1.0f;
      }
    }

}

int main(int argc, char **argv) {
  namespace po = boost::program_options;
  namespace fs = boost::filesystem;

  po::options_description generic_options("Generic Options");
    generic_options.add_options()("help,h", "Help screen");

  po::options_description config_options("Config");
  config_options.add_options()
      ("tsdf_file",  po::value<std::string>()->required(), "Path to the Input TSDF file")
      ("fill_half_width,w",  po::value<double>()->default_value(1), "Half Width of the filler")
      ;

  po::options_description cmdline_options;
  cmdline_options.add(generic_options).add(config_options);

  po::positional_options_description positional_options;
  positional_options.add("tsdf_file", 1);

  po::variables_map vm;

  try {
    po::store(
        po::command_line_parser(argc, argv).options(cmdline_options).positional(positional_options).run(),
        vm);
    po::notify(vm);
  } catch (const po::error &ex) {
    std::cerr << ex.what() << '\n';
    std::cout << cmdline_options << '\n';
    return EXIT_FAILURE;
  }

  if (vm.count("help")) {
    std::cout << cmdline_options << '\n';
    return EXIT_SUCCESS;
  }

  const fs::path tsdf_file(vm.at("tsdf_file").as<std::string>());

  using Tensor3f = Eigen::Tensor<float, 3>;
  Tensor3f input_tsdf;
  ShapeSpace::NRRD nrrd = ShapeSpace::loadTSDFfromNRRD(tsdf_file.string(), input_tsdf);

  if (!nrrd.thicknesses.isConstant(nrrd.thicknesses.x())) {
    std::cout << "Does not support TSDF with non-uniform thickness\n";
    std::cout << "nrrd.thicknesses = " << nrrd.thicknesses.transpose() << "\n";
    return EXIT_FAILURE;
  }

  const float resolution = nrrd.thicknesses.x();
  const Tensor3f::Dimensions tsdf_dim = input_tsdf.dimensions();

  const std::string tsdf_dim_str = std::to_string(tsdf_dim[0]) + "x" + std::to_string(tsdf_dim[1]) + "x" + std::to_string(tsdf_dim[2]);
  std::cout << "Input TSDF Dim = " << tsdf_dim_str << "\n";

  const double fill_half_width = vm["fill_half_width"].as<double>() - std::numeric_limits<double>::epsilon();

  Eigen::Index lb = std::floor(input_tsdf.dimension(1) / 2.0 - fill_half_width);
  Eigen::Index ub = std::ceil(input_tsdf.dimension(1) / 2.0 + fill_half_width);
  for (Eigen::Index y = lb; y < ub; ++y) {
    floodfill(input_tsdf, y);
  }
  {
    const fs::path vol_filename = tsdf_file.filename();
    std::cout << "Saving TSDF (32F) to " << vol_filename << std::flush;
    ShapeSpace::saveTSDFasNRRD(input_tsdf, vol_filename.string(), resolution);
    std::cout << " Done" << std::endl;
  }

  return EXIT_SUCCESS;
}



