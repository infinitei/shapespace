/**
 * @file animateTSDFPCAmodel.cpp
 * @brief animateTSDFPCAmodel
 *
 * @author Abhijit Kundu
 */

#include "ShapeSpace/TSDFPCAmodel.h"
#include "ShapeSpace/KeyCoordTransformer.h"
#include "ShapeSpace/MarchingCubes.h"

#include <CuteGL/Surface/WindowRenderViewer.h>
#include <CuteGL/Renderer/ModelRenderer.h>
#include <CuteGL/Core/MeshUtils.h>
#include <CuteGL/Geometry/ComputeNormals.h>
#include <QApplication>

#include <iostream>

int main(int argc, char **argv) {
  using namespace CuteGL;
  using namespace ShapeSpace;

  if (argc != 2) {
    std::cout << "ERROR: Incorrect number of arguments" << "\n";
    std::cout << "Example Usage: " << argv[0] << " /path/to/shape/file.h5";
    std::cout << "\n";
    return EXIT_FAILURE;
  }

  std::cout << "Loading TSDFPCAmodel from " << argv[1] << std::flush;
  TSDFPCAmodel tsdf_pca(argv[1]);
  std::cout << " Done" << std::endl;

  using Tensor3 = TSDFPCAmodel::Tensor3;
  const Tensor3::Dimensions tsdf_dim = tsdf_pca.mean.dimensions();

  if (!tsdf_pca.thicknesses.isConstant(tsdf_pca.thicknesses.x())) {
    std::cout << "Does not support TSDF with non-uniform thickness\n";
    std::cout << "nrrd.thicknesses = " << tsdf_pca.thicknesses.transpose() << "\n";
    return EXIT_FAILURE;
  }

  const float resolution = tsdf_pca.thicknesses.x();
  const Eigen::Vector3f bbx_min = -resolution * Eigen::Vector3f(tsdf_dim[0], tsdf_dim[1], tsdf_dim[2]) / 2;

  const ShapeSpace::KeyToCoord<3, float> key_to_coord(bbx_min, resolution);

  QApplication app(argc, argv);

  // Create the renderer
  std::unique_ptr<ModelRenderer> renderer(new ModelRenderer());

  // Instantiate the viewers
  WindowRenderViewer viewer(renderer.get());
  viewer.setBackgroundColor(55, 55, 55);
  viewer.resize(1024, 768);
  viewer.showAndWaitTillExposed();

  // Set camera pose via lookAt
  viewer.setCameraToLookAt(Eigen::Vector3f(0.5f, -2.0f, 2.0f), Eigen::Vector3f::Zero(), Eigen::Vector3f::UnitZ());

  const int num_of_samples = tsdf_pca.encoded_training_data.cols();

  int curr_index = 0;
  int step = 1;
  const int steps = 5;

  while (!QApplication::closingDown()) {
    Eigen::VectorXf shape_curr = tsdf_pca.encoded_training_data.col(curr_index);
    Eigen::VectorXf shape_target = tsdf_pca.encoded_training_data.col((curr_index + step) % num_of_samples);
    Eigen::VectorXf shape_increments = (shape_target - shape_curr) / steps;

    for (int i = 0; i <= steps; ++i) {
      Eigen::VectorXf shape = shape_curr + i * shape_increments;
      MeshData mesh;
      ShapeSpace::runMarchingCubes(tsdf_pca.computeTSDF(shape), key_to_coord, mesh.vertices, mesh.faces);
      CuteGL::computeNormals(mesh);
      CuteGL::colorizeMesh(mesh, MeshData::ColorType(180, 180, 180, 255));

      renderer->setModel(mesh);
      app.sendPostedEvents();
      QApplication::processEvents(QEventLoop::AllEvents, 30);
    }

    curr_index++;
    curr_index = curr_index % num_of_samples;
  }
}

