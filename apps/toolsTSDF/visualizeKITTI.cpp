/**
 * @file visualizeKITTI.cpp
 * @brief visualizeKITTI
 *
 * @author Abhijit Kundu
 */

#include <CuteGL/Core/PoseUtils.h>
#include <CuteGL/Geometry/ComputeAlignedBox.h>
#include <CuteGL/Renderer/MultiObjectRenderer.h>
#include <CuteGL/Surface/OffScreenRenderViewer.h>
#include <CuteGL/Core/PoseUtils.h>
#include <CuteGL/IO/ImportPLY.h>

#include <CuteGL/IO/ImportPLY.h>
#include <CuteGL/Core/MeshUtils.h>
#include <CuteGL/Geometry/ComputeAlignedBox.h>

#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include <QGuiApplication>
#include <QFileInfo>
#include <memory>
#include <iostream>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

std::vector<std::pair<Eigen::Vector4d, Eigen::Vector3d>> loadObjectsFromKITTIResults(const std::string& filename, double threshold = 0.3) {
  std::ifstream file(filename.c_str());
  if(! file.is_open() ) {
    throw std::runtime_error("Cannot open File at " + filename);
  }

  std::vector<std::pair<Eigen::Vector4d, Eigen::Vector3d>> annotations;

  for( std::string line; getline( file, line ); ) {
    std::istringstream iss(line);

    std::string type;
    Eigen::Vector4d amodal_bbx;
    Eigen::Vector3d viewpoint;
    double score;


    int ignore_int;
    double ignore_dbl;
    std::string ignore_str;

    iss >> type;       // type
    iss >> ignore_dbl; // truncated
    iss >> ignore_int; // occluded
    iss >> ignore_dbl; // alpha

    iss >> amodal_bbx.x(); // l
    iss >> amodal_bbx.y(); // t
    iss >> amodal_bbx.z(); // r
    iss >> amodal_bbx.w(); // b

    iss >> ignore_dbl;  // height
    iss >> ignore_dbl;  // width
    iss >> ignore_dbl;  // length

    iss >> viewpoint.x();  // PositionX
    iss >> viewpoint.y();  // PositionY
    iss >> viewpoint.z();  // PositionZ

    iss >> ignore_dbl;    // rotation_y

    iss >> score;    // score

    if(type != "Car")
      continue;

    if (score < threshold)
      continue;

    // Add to objects
    annotations.emplace_back(amodal_bbx, viewpoint);
  }

  file.close();
  return annotations;
}

std::vector<CuteGL::MeshData> loadModels() {
  std::vector<CuteGL::MeshData> meshes(404);
  const std::string base = "/home/abhijit/CPPWorkspace/ShapeSpace/build/tsdf_meshes/";
#if defined _OPENMP && _OPENMP >= 200805
#pragma omp parallel for
#endif
  for (std::size_t i = 0; i< 404; ++i) {
    std::string file = base + "tsdf_mesh_" + std::to_string(i)+ ".ply";
    meshes[i] = CuteGL::loadMeshFromPLY(file);
  }
  return meshes;
}


int main(int argc, char **argv) {

  if (argc < 2) {
    std::cout << "Usage:" << argv[0] << " [KITTI_file]\n";
    return EXIT_FAILURE;
  }

  std::vector<CuteGL::MeshData> meshes = loadModels();
  std::vector<Eigen::AlignedBox3f> boxes (meshes.size());
  for (std::size_t i = 0; i< 404; ++i) {
    boxes[i] = CuteGL::computeAlignedBox(meshes[i]);
  }



  std::string image_dir = "/media/Scratchspace/KITTI-Object/training/image_2/";
//  std::string image_dir = "/media/Scratchspace/KITTI-Object/training/label_2";

  namespace fs = boost::filesystem;

  fs::path fp(argv[1]);
  if (!fs::exists(fp)) {
    std::cout << "Error:" << fp << " does not exist\n";
    return EXIT_FAILURE;
  }

  std::string image_id = fp.stem().string();
  std::cout << image_id << "\n";


  std::vector<std::pair<Eigen::Vector4d, Eigen::Vector3d>> annos = loadObjectsFromKITTIResults(fp.string());

  for (const auto& tuple :  annos) {
    std::cout << tuple.first << "\n";
  }



  cv::Mat cv_image = cv::imread(image_dir + image_id + ".png", cv::IMREAD_COLOR);

  return EXIT_SUCCESS;
}
