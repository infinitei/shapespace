/**
 * @file exportTemplateMesh.cpp
 * @brief exportTemplateMesh
 *
 * @author Abhijit Kundu
 */

#include "ShapeSpace/H5EigenDense.h"

#include <CuteGL/IO/ImportPLY.h>
#include <CuteGL/Geometry/ComputeNormals.h>
#include <iostream>

int main(int argc, char **argv) {

  H5::H5File file("smpl_female_lbs_10_207_0.h5", H5F_ACC_RDONLY);

  Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> template_vertices;
  H5Eigen::load(file, "template_vertices", template_vertices);

  Eigen::Matrix<unsigned int, Eigen::Dynamic, 3, Eigen::RowMajor> faces;
  H5Eigen::load(file, "faces", faces);

  Eigen::MatrixXd blend_weights;
  H5Eigen::load(file, "blend_weights", blend_weights);

  Eigen::Matrix<double, 3, 24> colors;
  colors.setRandom();
  colors.array() += 1.0;

  CuteGL::MeshData mesh;

  // Set vertices
  mesh.vertices.resize(template_vertices.rows());
  for (Eigen::Index i = 0; i < template_vertices.rows(); ++i) {
    CuteGL::MeshData::VertexData& vd = mesh.vertices[i];
    vd.position = template_vertices.row(i);

    Eigen::Vector3d rgb = 255.0 * colors * blend_weights.row(i).transpose();
    vd.color << rgb.cast<unsigned char>(), 255;
//    vd.color = CuteGL::MeshData::ColorType(180, 180, 180, 255);
  }

  // Set faces
  mesh.faces = faces;
//  std::copy(faces.data(), faces.data() + faces.size(), std::back_inserter( mesh.indices));

  // Set normals
  CuteGL::computeNormals(mesh);

  // Save  as PLY
  CuteGL::saveMeshAsBinPly(mesh, "mesh.ply");

  return EXIT_SUCCESS;
}
