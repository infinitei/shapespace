/**
 * @file resizeTSDF.cpp
 * @brief resizeTSDF
 *
 * @author Abhijit Kundu
 */

#include "ShapeSpace/TSDFIO.h"

#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include <iostream>

template <typename T>
T gaussian(T x, T stddev ) {
    T variance2 = stddev*stddev*2.0;
    return std::exp(-(x*x)/variance2)/std::sqrt(M_PI*variance2);
}

int main(int argc, char **argv) {
  std::cout.imbue(std::locale(""));
  namespace po = boost::program_options;
  namespace fs = boost::filesystem;

  po::options_description generic_options("Generic Options");
    generic_options.add_options()("help,h", "Help screen");

  po::options_description config_options("Config");
  config_options.add_options()
      ("tsdf_file",  po::value<std::string>()->required(), "Path to the Input TSDF file")
      ;

  po::options_description cmdline_options;
  cmdline_options.add(generic_options).add(config_options);

  po::positional_options_description positional_options;
  positional_options.add("tsdf_file", 1);

  po::variables_map vm;

  try {
    po::store(
        po::command_line_parser(argc, argv).options(cmdline_options).positional(positional_options).run(),
        vm);
    po::notify(vm);
  } catch (const po::error &ex) {
    std::cerr << ex.what() << '\n';
    std::cout << cmdline_options << '\n';
    return EXIT_FAILURE;
  }

  if (vm.count("help")) {
    std::cout << cmdline_options << '\n';
    return EXIT_SUCCESS;
  }

  const fs::path tsdf_file(vm.at("tsdf_file").as<std::string>());

  using Tensor3f = Eigen::Tensor<float, 3>;
  Tensor3f input_tsdf;
  ShapeSpace::NRRD nrrd = ShapeSpace::loadTSDFfromNRRD(tsdf_file.string(), input_tsdf);

  if (!nrrd.thicknesses.isConstant(nrrd.thicknesses.x())) {
    std::cout << "Does not support TSDF with non-uniform thickness\n";
    std::cout << "nrrd.thicknesses = " << nrrd.thicknesses.transpose() << "\n";
    return EXIT_FAILURE;
  }

  const float resolution = nrrd.thicknesses.x();

  Tensor3f blur_kernel(5, 5, 5);
  for (Eigen::Index i = 0; i < 5; ++i)
    for (Eigen::Index j = 0; j < 5; ++j)
      for (Eigen::Index k = 0; k < 5; ++k) {
      float x = Eigen::Vector3f(i - 2.0, j - 2.0, k- 2.0).norm();
      blur_kernel(i, j, k) = gaussian(x, 1.0f);
    }
  Eigen::Tensor<float, 0> blur_sum = blur_kernel.sum();
  blur_kernel = blur_kernel / blur_sum(0);


  Eigen::array<ptrdiff_t, 3> stride_of_2({2, 2, 2});
  Eigen::array<std::pair<ptrdiff_t, ptrdiff_t>, 3> paddings;
  paddings.fill(std::make_pair(2, 2));

  //  Tensor3f output(14); // output_dim_size = input_dim_size - kernel_dim_size + 1
  Eigen::array<ptrdiff_t, 3> dims({0, 1, 2});  // Specify dimension for convolution.
  Tensor3f resized_tsdf = input_tsdf.pad(paddings, 1.0f).convolve(blur_kernel, dims).stride(stride_of_2);

  {

    {
      const Tensor3f::Dimensions tsdf_dim = input_tsdf.dimensions();
      const std::string tsdf_dim_str = std::to_string(tsdf_dim[0]) + "x" + std::to_string(tsdf_dim[1]) + "x" + std::to_string(tsdf_dim[2]);
      std::cout << "Input TSDF Dim = " << tsdf_dim_str << "  ";
    }
    {
      const Tensor3f::Dimensions tsdf_dim = resized_tsdf.dimensions();
      const std::string tsdf_dim_str = std::to_string(tsdf_dim[0]) + "x" + std::to_string(tsdf_dim[1]) + "x" + std::to_string(tsdf_dim[2]);
      std::cout << "Resized TSDF Dim = " << tsdf_dim_str << "\n";
    }
  }

  const fs::path resized_file = tsdf_file.filename();
  ShapeSpace::saveTSDFasNRRD(resized_tsdf, resized_file.string(), 2.0*resolution);
  std::cout << "Saved resized TSDF as " << resized_file << "\n";

  return EXIT_SUCCESS;
}
