/**
 * @file visualizeJsonDataset.cpp
 * @brief visualizeJsonDataset
 *
 * @author Abhijit Kundu
 */

#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include <json.hpp>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <fstream>
#include <iostream>
#include <vector>
#include <array>

namespace ShapeSpace {

// a simple struct to model a person
struct Annotation {
  using Array4d = std::array<double, 4>;
  using Array10d = std::array<double, 10>;

  std::string image_file;
  Array4d viewpoint;
  Array4d bbx_amodal;
  Array4d bbx_crop;
  Array10d shape_param;
};


struct Dataset {
  std::string name;
  boost::filesystem::path rootdir;
  std::vector<Annotation> annotations;
};

void to_json(nlohmann::json& j, const Dataset& dataset) {
  j = nlohmann::json {
    { "name", dataset.name },
    { "rootdir", dataset.rootdir.string() },
    { "annotations", dataset.annotations }
  };
}

void from_json(const nlohmann::json& j, Dataset& dataset) {
  dataset.name = j["name"].get<std::string>();
  dataset.rootdir = j["rootdir"].get<std::string>();
  dataset.annotations = j["annotations"].get<std::vector<Annotation>>();
}


void to_json(nlohmann::json& j, const Annotation& p) {
  j = nlohmann::json {
    { "image_file", p.image_file },
    { "viewpoint", p.viewpoint },
    { "bbx_amodal", p.bbx_amodal },
    { "bbx_crop", p.bbx_crop },
    { "shape_param", p.shape_param }
  };
}

void from_json(const nlohmann::json& j, Annotation& p) {
  p.image_file = j["image_file"].get<std::string>();

  using VectorXd = std::vector<double>;
  std::copy_n(j["viewpoint"].get<VectorXd>().begin(), 4, p.viewpoint.begin());
  std::copy_n(j["bbx_amodal"].get<VectorXd>().begin(), 4, p.bbx_amodal.begin());
  std::copy_n(j["bbx_crop"].get<VectorXd>().begin(), 4, p.bbx_crop.begin());
  std::copy_n(j["shape_param"].get<VectorXd>().begin(), 10, p.shape_param.begin());
}

std::ostream& operator<<(std::ostream& os, const Annotation& anno) {
  os << "image_file: " << anno.image_file;
  os << "\nviewpoint: ";
  for(const auto& s: anno.viewpoint)
    os << s << ' ';
  os << "\nbbx_amodal: ";
  for(const auto& s: anno.bbx_amodal)
      os << s << ' ';
  os << "\nbbx_crop: ";
  for(const auto& s: anno.bbx_crop)
      os << s << ' ';
  os << "\nshape_param: ";
  for(const auto& s: anno.shape_param)
        os << s << ' ';
  return os;
}

} // end namespace ShapeSpace





int main(int argc, char **argv) {
  using json = nlohmann::json;
  namespace po = boost::program_options;
  namespace fs = boost::filesystem;

  po::options_description generic_options("Generic Options");
    generic_options.add_options()("help,h", "Help screen");

  po::options_description config_options("Config");
    config_options.add_options()
        ("dataset,d",  po::value<std::string>()->required(), "Path to dataset file (JSON)")
        ;

  po::options_description cmdline_options;
  cmdline_options.add(generic_options).add(config_options);

  po::variables_map vm;

  try {
    po::store(po::command_line_parser(argc, argv).options(cmdline_options).run(), vm);
    po::notify(vm);
  } catch (const po::error &ex) {
    std::cerr << ex.what() << '\n';
    std::cout << cmdline_options << '\n';
    return EXIT_FAILURE;
  }

  if (vm.count("help")) {
    std::cout << cmdline_options << '\n';
    return EXIT_SUCCESS;
  }

  const fs::path dataset_file(vm["dataset"].as<std::string>());
  if (!fs::exists(dataset_file)) {
    std::cout << "Error:" << dataset_file << " does not exist\n";
    return EXIT_FAILURE;
  }

  ShapeSpace::Dataset dataset;
  {
    json dataset_json;
    {
      std::cout << "Parsing " << dataset_file.string() << " .... " << std::flush;
      std::ifstream file(dataset_file.c_str());
      if (!file.is_open()) {
        throw std::runtime_error("Cannot open File from " + dataset_file.string());
      }
      file >> dataset_json;
      std::cout << "Done." << std::endl;
    }
    dataset = dataset_json;
  }
  std::cout << "Loaded dataset \"" << dataset.name << "\" with " << dataset.annotations.size() << " annotations" << std::endl;

  const int num_of_annotations = dataset.annotations.size();

  const std::string windowname = "ImageViewer";
  cv::namedWindow(windowname, CV_WINDOW_AUTOSIZE | CV_WINDOW_KEEPRATIO | CV_GUI_EXPANDED);

  std::cout << "Press \"P\" to PAUSE/UNPAUSE\n";
  std::cout << "Use Arrow keys or WASD keys for prev/next images\n";

  bool paused = true;
  int step = 1;
  for (int i = 0;;) {
    const ShapeSpace::Annotation& anno = dataset.annotations[i];
    const std::string image_path = (dataset.rootdir / anno.image_file).string();
    cv::Mat cv_image = cv::imread(image_path, cv::IMREAD_COLOR);
    std::cout << anno << std::endl;

    cv::imshow(windowname, cv_image);
    const int key = cv::waitKey(!paused) % 256;

    if (key == 27 || key == 'q')  // Esc or Q or q
      break;
    else if (key == 123 || key == 125 || key == 50 || key == 52 || key == 81 || key == 84 || key == 'a' || key == 's')  // Down or Left Arrow key (including numpad) or 'a' and 's'
      step = -1;
    else if (key == 124 || key == 126 || key == 54 || key == 56 || key == 82 || key == 83 || key == 'w' || key == 'd')  // Up or Right Arrow or 'w' or 'd'
      step = 1;
    else if (key == 'p' || key == 'P')
      paused = !paused;

    i += step;
    i = std::max(0, i);
    i = std::min(i, num_of_annotations - 1);
  }

  return EXIT_SUCCESS;
}


