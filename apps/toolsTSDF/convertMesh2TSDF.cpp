/**
 * @file convertMesh2TSDF.cpp
 * @brief convertMesh2TSDF
 *
 * @author Abhijit Kundu
 */

#include "ShapeSpace/EigenTypedefs.h"
#include "ShapeSpace/KeyCoordTransformer.h"
#include "ShapeSpace/TSDFIO.h"
#include "CuteGL/Renderer/MultiObjectRenderer.h"
#include <CuteGL/Surface/OffScreenRenderViewer.h>
#include <CuteGL/IO/ImportPLY.h>
#include <CuteGL/Core/PoseUtils.h>
#include <CuteGL/Geometry/ComputeNormals.h>
#include <CuteGL/Geometry/ComputeAlignedBox.h>

#include <Eigen/CXX11/Tensor>
#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>
#include <QGuiApplication>
#include <memory>
#include <iostream>
#include <random>

Eigen::AlignedStdVector<Eigen::Vector3d> getPointsOnSphere(unsigned int num_of_samples = 0, const double radius = 1.5, const bool use_neg_z = false) {
	Eigen::AlignedStdVector<Eigen::Vector3d> points;

	// Include axis aligned points
	points.emplace_back(0.0, -radius, 0.0);
	points.emplace_back(-radius, 0.0, 0.0);
	points.emplace_back(0.0, radius, 0.0);
	points.emplace_back(radius, 0.0, 0.0);
	points.emplace_back(0.0, 0.0, radius);
	if(use_neg_z)
	  points.emplace_back(0.0, 0.0, -radius);

	if(num_of_samples) {
    std::mt19937 gen;  //Standard mersenne_twister_engine seeded with rd()
    std::uniform_real_distribution<> lattitude_dis(0, 2 * M_PI);
    std::uniform_real_distribution<> height_dis(use_neg_z ? -1.0: 0.0, 1.0);

    for (unsigned int n = 0; n < num_of_samples; ++n) {
      double theta = lattitude_dis(gen);
      double z = height_dis(gen);
      double x = std::sqrt(1.0 - z * z) * std::cos(theta);
      double y = std::sqrt(1.0 - z * z) * std::sin(theta);

      Eigen::Vector3d point = radius * Eigen::Vector3d(x,y,z);
      points.push_back(point);
    }

	}

	return points;
}

int main(int argc, char **argv) {
  using namespace CuteGL;
  namespace fs = boost::filesystem;
  namespace po = boost::program_options;

  po::options_description generic_options("Generic Options");
    generic_options.add_options()("help,h", "Help screen");

  po::options_description config_options("Config");
    config_options.add_options()
        ("mesh_files",  po::value<std::vector<std::string>>(), "Path to mesh files")
        ("inverse_resolution,n",  po::value<int>()->default_value(256), "1/resolution (defaults to 256)")
        ("out_directory,o",  po::value<std::string>()->default_value("./"), "Output directory to save tsdf files")
        ("pad_size,p",  po::value<int>()->default_value(2), "Number of voxels to pad")
        ("use_neg_z_views,z",  po::value<bool>()->default_value(false), "Use negative z (height) views")
        ("num_of_views,v",  po::value<unsigned int>()->default_value(0), "Additional num of views apart from axis aligned views")
        ("neg_gap_multiplier,g",  po::value<int>()->default_value(10), "Number of voxels behind surface to update")
        ("tsdf_ramp_width_multiplier,w",  po::value<int>()->default_value(8), "Half Width of TSDF ramp in terms of voxels")
        ;

  po::positional_options_description p;
  p.add("mesh_files", -1);

  po::options_description cmdline_options;
  cmdline_options.add(generic_options).add(config_options);

  po::variables_map vm;

  try {
    po::store(po::command_line_parser(argc, argv).options(cmdline_options).positional(p).run(), vm);
    po::notify(vm);

    if (!vm.count("mesh_files"))
      throw po::error("Please provide at-least one mesh file");
  }
  catch (const po::error &ex) {
    std::cerr << ex.what() << '\n';
    std::cout << cmdline_options << '\n';
    return EXIT_FAILURE;
  }

  if (vm.count("help")) {
    std::cout << cmdline_options << '\n';
    return EXIT_SUCCESS;
  }

  const fs::path out_dir(vm.at("out_directory").as<std::string>());
  if(!fs::exists(out_dir)) {
    std::cout << "User output directory " << out_dir << " does not exists\n";
    return EXIT_FAILURE;
  }
  if(!fs::is_directory(out_dir)) {
    std::cout << "User output directory " << out_dir << " is not a directory\n";
    return EXIT_FAILURE;
  }


  const std::vector<std::string> mesh_files = vm["mesh_files"].as<std::vector<std::string>>();
  std::cout << "User provided " << mesh_files.size() << " mesh files\n";

  std::vector<MeshData> meshes(mesh_files.size());
  std::cout << "Loading " << mesh_files.size() << " meshes ........... " << std::flush;
#pragma omp parallel for
  for (std::size_t i = 0; i< mesh_files.size(); ++i) {
    meshes[i] = loadMeshFromPLY(mesh_files[i]);
    computeNormals(meshes[i]);
  }
  std::cout << "Done" << std::endl;

  Eigen::AlignedBox3f meshes_bbx;
  for (const auto& mesh : meshes) {
    meshes_bbx.extend(computeAlignedBox(mesh));
  }
  const Eigen::IOFormat vecfmt(Eigen::StreamPrecision, Eigen::DontAlignCols, ", ", ", ", "", "", "[", "]");
  std::cout << "Meshes BBX Extends: " << meshes_bbx.min().format(vecfmt) << " ---- " << meshes_bbx.max().format(vecfmt) << "\n";
  std::cout << "Meshes BBX Sizes: " << meshes_bbx.sizes().format(vecfmt) << "\n";
  std::cout << "Meshes BBX Diagonal Length: " << meshes_bbx.diagonal().norm() << "\n";

  if (!meshes_bbx.center().isZero()) {
    std::cout << "Mesh bbx not centered at origin. Meshes probably not aligned\n";
    return EXIT_FAILURE;
  }
  if (meshes_bbx.sizes().maxCoeff() > 1.0f) {
    std::cout << "Expects meshes to within unit cube. BBX Size = " << meshes_bbx.sizes().format(vecfmt) << "\n";
    return EXIT_FAILURE;
  }

  const int N = vm.at("inverse_resolution").as<int>();
  if (N <= 2) {
    std::cout << "inverse_resolution is too low (requires > 2)\n";
    return EXIT_FAILURE;
  }

  const int neg_gap_multiplier = vm.at("neg_gap_multiplier").as<int>();
  const int tsdf_ramp_width_multiplier = vm.at("tsdf_ramp_width_multiplier").as<int>();

  const bool use_neg_z_views = vm.at("use_neg_z_views").as<bool>();
  const unsigned int num_of_views = vm.at("num_of_views").as<unsigned int>();

  const int pad = vm.at("pad_size").as<int>();

  // Set resolution
  const double resolution = 1.0 / N;

  Eigen::Vector3d tsdf_bbx_size_coord = meshes_bbx.sizes().cast<double>().array() + 2 * pad * resolution;

  // Round to nearest even integer
  const Eigen::Vector3i tsdf_dim = 2 * (tsdf_bbx_size_coord / (2 * resolution)).array().round().cast<int>();
  tsdf_bbx_size_coord = tsdf_dim.cast<double>() * resolution;

  const Eigen::AlignedBox3d tsdf_bbx(-tsdf_bbx_size_coord/2.0, tsdf_bbx_size_coord/2.0);
  std::cout << "TSDF BBX: " << tsdf_bbx.min().format(vecfmt) << " ---- " << tsdf_bbx.max().format(vecfmt) << "\n";
  std::cout << "TSDF Size: " << tsdf_dim.format(vecfmt) << "\n";

  // Read command lines arguments.
  int fake_argc = 0;
  char **fake_argv = nullptr;
  QGuiApplication application(fake_argc, fake_argv);

  // loop over all mesh files
  for (size_t i = 0; i < mesh_files.size(); ++i) {


    fs::path mesh_fp(mesh_files[i]);

    std::cout << "Working on mesh " << mesh_fp.stem() << std::endl;

    using Image32FC1 = Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>;
    Eigen::AlignedStdVector<Eigen::Isometry3d> extrinsics;
    std::vector<Image32FC1> zbuffer_images;

    const int W = 4 * tsdf_dim.maxCoeff();
    const int H = 4 * tsdf_dim.maxCoeff();
    const Eigen::Vector2d middle_point(W / 2.0f, H / 2.0f);

    const double camera_radius = 1.0;
    const double scene_radius = 0.6;

    const double range_z = 2 * scene_radius;
    const double near_z = camera_radius - scene_radius;
    const double far_z = near_z + range_z;

    const double half_width = tsdf_bbx.sizes().maxCoeff() / 2.0;
    const double half_height = tsdf_bbx.sizes().maxCoeff() / 2.0;

    const Eigen::Matrix4d ortho = getGLOrthographicProjection(half_width,
                                                              half_height,
                                                              near_z, far_z);

    // Create camera extrinsics and corresponding Depthmaps
    {

      // Create the Renderer
      std::unique_ptr<MultiObjectRenderer> renderer = std::make_unique<MultiObjectRenderer>();
      renderer->setDisplayGrid(false);
      renderer->setDisplayAxis(false);

      // Set the viewer up
      OffScreenRenderViewer viewer(renderer.get());

      viewer.resize(W, H);

      viewer.camera().intrinsics() = ortho.cast<float>();

      viewer.create();

      viewer.makeCurrent();

      renderer->modelDrawers().addItem(Eigen::Affine3f::Identity(), meshes[i]);

      const Eigen::AlignedStdVector<Eigen::Vector3d> points_on_sphere = getPointsOnSphere(num_of_views, camera_radius, use_neg_z_views);

      for (const Eigen::Vector3d& point_on_sphere : points_on_sphere) {
        Eigen::Vector3d up_dir = Eigen::Vector3d::UnitZ();
        if(point_on_sphere.x() == 0.0 && point_on_sphere.y() == 0.0) {
          up_dir = Eigen::Vector3d::UnitY();
        }
        const Eigen::Isometry3d extrinsic = getExtrinsicsFromLookAt(point_on_sphere, Eigen::Vector3d::Zero(), up_dir);
        viewer.camera().extrinsics() = extrinsic.cast<float>();
        extrinsics.push_back(extrinsic);

        viewer.render();

        Image32FC1 zbuffer(H, W);
        viewer.readZBuffer(zbuffer.data());
        zbuffer.colwise().reverseInPlace();

        zbuffer = (2.0f * zbuffer.array()) - 1.0f;
        zbuffer = ( zbuffer.array() * (far_z - near_z)  + (far_z + near_z))/2.0;

        zbuffer_images.push_back(zbuffer);
      }

    } // end of Create camera extrinsics and corresponding Depthmaps

    const std::size_t num_of_cameras = extrinsics.size();
    assert(zbuffer_images.size() == num_of_cameras);

    using Tensor3f = Eigen::Tensor<float, 3>;

    Tensor3f tsdf_distance(tsdf_dim.x(), tsdf_dim.y(), tsdf_dim.z());
    Tensor3f tsdf_weights(tsdf_dim.x(), tsdf_dim.y(), tsdf_dim.z());

    // initialize distances to -1 (inside)
    tsdf_distance.setConstant(-1.0);

    // initialize weights to zero
    tsdf_weights.setZero();

    const ShapeSpace::KeyToCoord<3, double> key_to_coord(tsdf_bbx.min(), resolution);

    std::cout << "Doing Fusion ... " << std::flush;
#pragma omp parallel for
    for (Eigen::Index z = 0; z < tsdf_dim.z(); ++z)
      for (Eigen::Index y = 0; y < tsdf_dim.y(); ++y)
        for (Eigen::Index x = 0; x < tsdf_dim.x(); ++x) {
          const Eigen::Vector3d voxel_center = key_to_coord(x, y, z);

          for (std::size_t i = 0; i < num_of_cameras; ++i) {
            const Eigen::Vector3d cam_frame = extrinsics[i] * voxel_center;
            const Eigen::Vector3d ndc = (ortho * cam_frame.homogeneous()).hnormalized();
            const Eigen::Vector2d screen = (ndc.head<2>().array() + 1.0) * middle_point.array();
            const Eigen::Vector2d img_proj(screen.x(), H - screen.y());

            if(img_proj.x() < 0 || img_proj.x() >= W || img_proj.y() < 0 || img_proj.y() >= H)
              continue;

            const float depth = zbuffer_images[i](img_proj.y(), img_proj.x());
//            const float depth = near_z + clipped_z * range_z;
            const float signed_distance = depth - cam_frame.z();

            const float neg_gap_size = -(neg_gap_multiplier * resolution);
            if (signed_distance < neg_gap_size)
              continue;

            const float distance_ramp_half_width = (tsdf_ramp_width_multiplier * resolution);
            float tsdf = signed_distance / distance_ramp_half_width;

            // Do the truncation
            if (tsdf < -1.0f)
              tsdf = -1.0f;
            else if (tsdf > 1.0f)
              tsdf = 1.0f;

            float& tsdf_dist = tsdf_distance(x, y, z);
            float& tsdf_wt = tsdf_weights(x, y, z);

            tsdf_dist = tsdf_dist * tsdf_wt + tsdf;
            tsdf_wt += 1.0f;
            tsdf_dist /= tsdf_wt;

          }  //end  loop over all cameras
        }  // end loop over all voxels
    std::cout << "Done" << std::endl;

    std::cout << "TSDF Size = " << tsdf_distance.dimension(0) << " "
              << tsdf_distance.dimension(1) << " " << tsdf_distance.dimension(2)
              << '\n';

    {
      fs::path vol_filename = out_dir / (mesh_fp.parent_path().stem().string() + ".nrrd");
      std::cout << "Saving TSDF (32F) to " << vol_filename << std::flush;
      ShapeSpace::saveTSDFasNRRD(tsdf_distance, vol_filename.string(), resolution);
      std::cout << " Done" << std::endl;
    }

  } // end loop over all mesh files

  return EXIT_SUCCESS;
}


