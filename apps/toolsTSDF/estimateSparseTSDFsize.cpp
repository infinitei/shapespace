/**
 * @file estimateSparseTSDFsize.cpp
 * @brief estimateSparseTSDFsize
 *
 * @author Abhijit Kundu
 */

#include "ShapeSpace/TSDFIO.h"

#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>
#include <iostream>

int main(int argc, char **argv) {
  std::cout.imbue(std::locale(""));
  std::cout.precision(17);
  using VolSize = Eigen::Matrix<Eigen::Index, Eigen::Dynamic, 1>;

  namespace po = boost::program_options;
  namespace fs = boost::filesystem;

  po::options_description generic_options("Generic Options");
  generic_options.add_options()("help,h", "Help screen");

  po::options_description config_options("Config");
    config_options.add_options()
      ("input_tsdf_files", po::value< std::vector<std::string> >(), "Input Raw TSDF files")
      ;

  po::positional_options_description p;
  p.add("input_tsdf_files", -1);

  po::options_description cmdline_options;
  cmdline_options.add(generic_options).add(config_options);

  po::variables_map vm;

  try {
    po::store(po::command_line_parser(argc, argv).options(cmdline_options).positional(p).run(), vm);
    po::notify(vm);
  } catch (const po::error &ex) {
    std::cerr << ex.what() << '\n';
    std::cout << cmdline_options << '\n';
    return EXIT_FAILURE;
  }

  if (vm.count("help")) {
    std::cout << cmdline_options << '\n';
    return EXIT_SUCCESS;
  }

  if (!vm.count("input_tsdf_files")) {
    std::cout << "Please provide at-least one raw tsdf files" << '\n';
    std::cout << cmdline_options << '\n';
    return EXIT_FAILURE;
  }

  const std::vector<std::string> files = vm["input_tsdf_files"].as<std::vector<std::string>>();
  std::cout << "User provided " << files.size() << " tsdf files\n";

  // Load 1st file and get properties
  const ShapeSpace::NRRD expected_nrrd = ShapeSpace::parseNRRDheader(files[0]);

  const VolSize tsdf_size = expected_nrrd.sizes;
  const std::string tsdf_size_str = std::to_string(tsdf_size[0]) + "x"
      + std::to_string(tsdf_size[1]) + "x" + std::to_string(tsdf_size[2]);

  const Eigen::Index original_dimension = tsdf_size.prod();

  std::cout << "original_dimension =  " << original_dimension << " (" << tsdf_size_str << ")" << std::endl;

  Eigen::MatrixXf tsdf_data(original_dimension, files.size());
  std::cout << "Loading " << files.size() << " files .. " << std::flush;

#pragma omp parallel for
  for (Eigen::Index i = 0; i < tsdf_data.cols(); ++i) {
    ShapeSpace::loadTSDFfromNRRD(files[i], tsdf_data.col(i).data(), expected_nrrd);
  }
  std::cout << " Done." << std::endl;

  const float thresh = 10 * std::numeric_limits<float>::epsilon();
//  const float thresh = 0.5f;

  const float ub = 1.0f - thresh;
  const float lb = -1.0f + thresh;

  Eigen::VectorXi valid_dims = Eigen::VectorXi::Zero(original_dimension);
  for (Eigen::Index i = 0; i < tsdf_data.rows(); ++i) {
    for (Eigen::Index j = 0; j < tsdf_data.cols(); ++j) {
      if (tsdf_data(i, j) < ub && tsdf_data(i, j) > lb) {
        valid_dims[i] = 1;
        break;
      }
    }
  }

  const int sparse_dims = valid_dims.sum();
  std::cout << "sparse_dims =  " << sparse_dims << std::endl;
  std::cout << "reduction =  " << double(sparse_dims) / original_dimension << std::endl;


  return EXIT_SUCCESS;
}



