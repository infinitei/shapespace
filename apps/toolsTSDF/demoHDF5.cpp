/**
 * @file demoHDF5.cpp
 * @brief demoHDF5
 *
 * @author Abhijit Kundu
 */

#include "ShapeSpace/H5EigenDense.h"
#include "ShapeSpace/H5EigenTensor.h"
#include "ShapeSpace/H5EigenSparse.h"
#include "ShapeSpace/H5StdContainers.h"
#include <iostream>

template <class Derived>
void print_shape(const Eigen::DenseBase<Derived>& mat) {
  std::cout << mat.rows() << " x " << mat.cols() << "\n";
}

template<typename Scalar, int NumIndices, int Options, typename IndexType>
void print_shape(const Eigen::Tensor<Scalar, NumIndices, Options, IndexType>& tensor) {
  for (int i = 0; i < NumIndices; ++i) {
    std::cout << tensor.dimension(i) << " x ";
  }
  std::cout << "\b\b\b   \n";
}

int main(int argc, char **argv) {

  {
    H5::H5File file("smpl_female_lbs_10_207_0.h5", H5F_ACC_RDONLY);

    Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> template_vertices;
    H5Eigen::load(file, "template_vertices", template_vertices);
    print_shape(template_vertices);

    Eigen::Matrix<unsigned int, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> faces;
    H5Eigen::load(file, "faces", faces);
    print_shape(faces);

    Eigen::MatrixXd blend_weights;
    H5Eigen::load(file, "blend_weights", blend_weights);
    print_shape(blend_weights);

    Eigen::Tensor<float, 2, Eigen::RowMajor> template_vertices_tensor;
    H5Eigen::load(file, "template_vertices", template_vertices_tensor);
    print_shape(template_vertices_tensor);

    Eigen::Tensor<float, 3> shape_displacements;
    H5Eigen::load(file, "shape_displacements", shape_displacements);
    print_shape(shape_displacements);

    Eigen::Tensor<float, 1> betas(10);
    betas.setZero();
    print_shape(betas);

    typedef Eigen::Tensor<float, 1>::DimensionPair DimPair;
    Eigen::array<DimPair, 1> product_dims = {{DimPair(2, 0)}};
    Eigen::Tensor<float, 2> AB = shape_displacements.contract(betas, product_dims);
    print_shape(AB);
  }

  {
    Eigen::Tensor<float, 2> t2(8, 6);
    t2.setRandom();

    std::cout << " t2 = \n" << t2 << "\n";

    {

      H5::H5File save_file("out1.h5", H5F_ACC_TRUNC);
      H5Eigen::save(save_file, "t2", t2);
    }

    {
      H5::H5File file("out1.h5", H5F_ACC_RDONLY);

      Eigen::Tensor<float, 2> t2_in;
      H5Eigen::load(file, "t2", t2_in);

      std::cout << " t2_in = \n" << t2_in << "\n";
    }

    {

      H5::H5File save_file("out2.h5", H5F_ACC_TRUNC);
      Eigen::TensorMap<Eigen::Tensor<float, 2> > t2map(t2.data(), 8, 6);
      H5Eigen::save(save_file, "t2", t2map);
    }

    {
      H5::H5File file("out2.h5", H5F_ACC_RDONLY);

      Eigen::Tensor<float, 2> t2_in;
      H5Eigen::load(file, "t2", t2_in);
      std::cout << " t2map_in = \n" << t2_in << "\n";
    }
  }

  {
    std::vector<std::string> strings { "haha", "hehe", "bla_bla", "path/to/some/thing", "path/to/some/else" };

    {
      H5::H5File file("str_list.h5", H5F_ACC_TRUNC);
      H5StdContainers::save(file, "names", strings);
    }
    {
      H5::H5File file("str_list.h5", H5F_ACC_RDONLY);
      std::vector<std::string> strings_in;
      H5StdContainers::load(file, "names", strings_in);
      for (const auto& str: strings_in)
        std::cout << str << "\n";
    }

  }

  return EXIT_SUCCESS;
}



