/**
 * @file computePCAShapeSpace.cpp
 * @brief computePCAShapeSpace
 *
 * @note This is for illustrative purpose only. Use computeDualPCAShapeSpace
 *
 * @author Abhijit Kundu
 */

#include "ShapeSpace/EigenTypedefs.h"
#include "ShapeSpace/H5EigenTensor.h"
#include "ShapeSpace/H5StdContainers.h"
#include "ShapeSpace/TSDFIO.h"

#include <Eigen/Dense>
#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>
#include <array>
#include <iostream>

int main(int argc, char **argv) {
  using VolSize = Eigen::Matrix<Eigen::Index, Eigen::Dynamic, 1>;

  namespace po = boost::program_options;
  namespace fs = boost::filesystem;

  po::options_description generic_options("Generic Options");
  generic_options.add_options()("help,h", "Help screen");

  po::options_description config_options("Config");
  config_options.add_options()
    ("input_tsdf_files", po::value< std::vector<std::string> >(), "Input Raw TSDF files")
    ("reduced_dimension,r", po::value<Eigen::Index>()->default_value(10), "Reduced (output) dimension")
    ("out_file,o", po::value<std::string>()->default_value("pca_shape.h5"), "Path to save shape-space hdf5 file")
    ;

  po::positional_options_description p;
  p.add("input_tsdf_files", -1);

  po::options_description cmdline_options;
  cmdline_options.add(generic_options).add(config_options);

  po::variables_map vm;

  try {
    po::store(po::command_line_parser(argc, argv).options(cmdline_options).positional(p).run(), vm);
    po::notify(vm);
  } catch (const po::error &ex) {
    std::cerr << ex.what() << '\n';
    std::cout << cmdline_options << '\n';
    return EXIT_FAILURE;
  }

  if (vm.count("help")) {
    std::cout << cmdline_options << '\n';
    return EXIT_SUCCESS;
  }

  if (!vm.count("input_tsdf_files")) {
    std::cout << "Please provide at-least one raw tsdf files" << '\n';
    std::cout << cmdline_options << '\n';
    return EXIT_FAILURE;
  }

  const std::vector<std::string> files = vm["input_tsdf_files"].as<std::vector<std::string>>();
  std::cout << "User provided " << files.size() << " tsdf files\n";

  // Load 1st file and get properties
  const ShapeSpace::NRRD expected_nrrd = ShapeSpace::parseNRRDheader(files[0]);

  const VolSize tsdf_size  = expected_nrrd.sizes;
  const std::string tsdf_size_str = std::to_string(tsdf_size[0]) + "x" + std::to_string(tsdf_size[1]) + "x" + std::to_string(tsdf_size[2]);

  const Eigen::Index original_dimension =  tsdf_size.prod();
  const Eigen::Index reduced_dimension = vm["reduced_dimension"].as<Eigen::Index>();

  std::cout << "original_dimension =  " << original_dimension << " (" << tsdf_size_str << ")"
            << " reduced_dimension =  " << reduced_dimension << "\n";
  if(original_dimension < reduced_dimension) {
    std::cout << "Error: original_dimension < reduced_dimension\n";
    return EXIT_FAILURE;
  }

  Eigen::MatrixXf tsdf_data(original_dimension, files.size());
  std::cout << "Loading " << files.size() << " files .. " << std::flush;
#pragma omp parallel for
  for (Eigen::Index i = 0; i < tsdf_data.cols(); ++i) {
    ShapeSpace::loadTSDFfromNRRD(files[i], tsdf_data.col(i).data(), expected_nrrd);
  }
  std::cout << " Done." << std::endl;

  Eigen::VectorXf mean_tsdf;
  Eigen::MatrixXf basis;
  Eigen::MatrixXf encoded_training_data;
  {
    std::cout << "Learning PCA shape-space .." << std::flush;
    mean_tsdf = tsdf_data.rowwise().mean();
    Eigen::MatrixXf centered = tsdf_data.colwise() - mean_tsdf;

    Eigen::JacobiSVD<Eigen::MatrixXf> svd(centered, Eigen::ComputeThinU);
    basis = svd.matrixU().leftCols(reduced_dimension);
    encoded_training_data = basis.transpose() * centered;
    std::cout << " Done" << std::endl;
  }

  {
    std::string filename = vm["out_file"].as<std::string>();
    std::cout << "Saving shape basis to " << filename << " ... " << std::flush;
    H5::H5File file(filename, H5F_ACC_TRUNC);

    Eigen::TensorMap<Eigen::Tensor<float, 3> > tsdf_mean_tensor(mean_tsdf.data(), tsdf_size[0], tsdf_size[1], tsdf_size[2]);

    Eigen::TensorMap<Eigen::Tensor<float, 4> > tsdf_basis_tensor(basis.data(), tsdf_size[0], tsdf_size[1], tsdf_size[2], reduced_dimension);

    H5Eigen::save(file, "tsdf_mean", tsdf_mean_tensor);
    H5Eigen::save(file, "tsdf_basis", tsdf_basis_tensor);
    H5Eigen::save(file, "encoded_training_data", encoded_training_data);

    std::vector<std::string> model_names(files.size());
#pragma omp parallel for
    for (std::size_t i = 0; i < files.size(); ++i) {
      model_names[i] = fs::path(files[i]).stem().string();
    }
    H5StdContainers::save(file, "model_names", model_names);

    H5Eigen::save(file, "thicknesses", expected_nrrd.thicknesses);

    std::cout << " Done" << std::endl;
  }

  return EXIT_SUCCESS;
}




