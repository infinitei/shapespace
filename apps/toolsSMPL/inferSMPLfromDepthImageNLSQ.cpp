/**
 * @file inferSMPLfromDepthImage.cpp
 * @brief inferSMPLfromDepthImage
 *
 * @author Abhijit Kundu
 */

#include <CuteGL/Renderer/SMPLRenderer.h>
#include <CuteGL/Surface/OffScreenRenderViewer.h>
#include <CuteGL/Core/PoseUtils.h>
#include <CuteGL/IO/ImportPLY.h>

#include "ShapeSpace/ImageUtils.h"

#include <QGuiApplication>
#include <memory>
#include <iostream>

#include <ceres/ceres.h>
#include <glog/logging.h>

class Renderer {
 public:
  using Image32FC1 = Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>;
  using SMPLScalar = float;

  Renderer(const int W, const int H)
      : renderer_(new CuteGL::SMPLRenderer),
        viewer_(renderer_.get()),
        counts_(0) {

    renderer_->setDisplayGrid(false);
    renderer_->setDisplayAxis(false);

    viewer_.setBackgroundColor(55, 55, 55);
    viewer_.resize(W, H);

    // Set camera pose via lookAt
    viewer_.setCameraToLookAt(Eigen::Vector3f(0.0f, 0.85f, 2.6f),
                              Eigen::Vector3f(0.0f, 0.85f, 0.0f),
                              Eigen::Vector3f::UnitY());

    float fovY = 45.0f * M_PI / 180.0f;
    float aspect_ratio = float(W) / H;
    viewer_.camera().intrinsics() = CuteGL::getGLPerspectiveProjection(fovY, aspect_ratio, 1.0f, 4.0f);

    viewer_.create();
    viewer_.makeCurrent();
    renderer_->setSMPLData("smpl_male_lbs_10_207_0.h5");
  }

  template <class DerivedPose, class DerivedShape>
  Image32FC1 operator()(const Eigen::DenseBase<DerivedPose>& poses, const Eigen::DenseBase<DerivedShape>& betas) {
    renderer_->smplDrawer().shape() = betas;
    renderer_->smplDrawer().pose() = poses;
    renderer_->smplDrawer().updateShapeAndPose();

    viewer_.render();

    Image32FC1 zbuffer(viewer_.height(), viewer_.width());
    viewer_.readZBuffer(zbuffer.data());

    ++counts_;

    return zbuffer;
  }

  std::unique_ptr<CuteGL::SMPLRenderer> renderer_;
  CuteGL::OffScreenRenderViewer viewer_;

  std::size_t counts_;
};

struct RenderAndCompareCostFunctor {
  using SMPLScalar = Renderer::SMPLScalar;
  using ParamType = Eigen::Matrix<SMPLScalar, Eigen::Dynamic, 1>;

  RenderAndCompareCostFunctor(Renderer& renderer, const Renderer::Image32FC1& target_image)
    : renderer_(renderer),
      target_image_(target_image) {
  }

  template<typename T>
  bool operator()(const T* const shape, T* residual) const {
    Eigen::Map<const Eigen::Matrix<T, 10, 1> > betas(shape);
    Renderer::Image32FC1 rendered = renderer_(ParamType::Zero(72), betas.template cast<SMPLScalar>());

    Eigen::Map<Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> > residual_map(residual, target_image_.rows(), target_image_.cols());
    residual_map = (rendered - target_image_).cast<T>();

    return true;
  }

  Renderer& renderer_;
  const Renderer::Image32FC1& target_image_;
};

int main(int argc, char **argv) {
  google::InitGoogleLogging(argv[0]);
  using namespace ShapeSpace;
  using namespace CuteGL;
  using ceres::NumericDiffCostFunction;
  using ceres::NumericDiffOptions;
  using ceres::CostFunction;
  using ceres::Problem;
  using ceres::Solver;
  using ceres::Solve;

  using ParamScalar = Renderer::SMPLScalar;

  QGuiApplication app(argc, argv);

  Eigen::VectorXd target_pose = 0.00 * Eigen::VectorXd::Random(72);
  Eigen::VectorXd target_shape = Eigen::VectorXd::Zero(10);
  target_shape[0] = -2;
  target_shape[1] = -5;
  target_shape[2] = 3;
  target_shape[8] = 10;


  Eigen::VectorXd pred_shape = Eigen::VectorXd::Zero(10);

  const int W = 512;
  const int H = 512;
  const int pyramid_levels = 2;

  {
    Renderer renderer(W, H);
    Renderer::Image32FC1 target_image = renderer(target_pose.cast<ParamScalar>(),  target_shape.cast<ParamScalar>());
    saveImageAs8UC1(target_image, "target_image.png");

    Renderer::Image32FC1 initial_image = renderer(target_pose.cast<ParamScalar>(),  pred_shape.cast<ParamScalar>());
    saveImageAs8UC1(initial_image, "initial_image.png");
  }

  for (int i = pyramid_levels; i --> 0; ) {
    std::cout << "----------------------------------------------------\n";
    const int cw = W / (1 << i);
    const int ch = H / (1 << i);

    std::cout << "Working on Pyramid level " << i << " (" << cw << "x" << ch << ")" << std::endl;

    Renderer renderer(cw, ch);
    Renderer::Image32FC1 target_image = renderer(target_pose.cast<ParamScalar>(),  target_shape.cast<ParamScalar>());
    RenderAndCompareCostFunctor rac(renderer, target_image);

    // Build the problem.
    Problem problem;

    NumericDiffOptions num_diff_opts;
//    num_diff_opts.relative_step_size = 1e-6;
    num_diff_opts.ridders_relative_initial_step_size = 1e-1;
//    num_diff_opts.max_num_ridders_extrapolations = 10;
//    num_diff_opts.ridders_epsilon = 1e-12;
//    num_diff_opts.ridders_step_shrink_factor = 2.0;

    // Set up the only cost function (also known as residual).
    CostFunction* cost_function = new NumericDiffCostFunction<RenderAndCompareCostFunctor,
        ceres::RIDDERS, ceres::DYNAMIC, 10>(new RenderAndCompareCostFunctor(renderer, target_image),
                                            ceres::TAKE_OWNERSHIP,
                                            cw * ch,
                                            num_diff_opts);
    problem.AddResidualBlock(cost_function, NULL, pred_shape.data());

    // Run the solver!
    Solver::Options options;
    options.minimizer_progress_to_stdout = true;
    options.max_num_iterations = 500;
    options.num_threads = 12;

    Solver::Summary summary;
    Solve(options, &problem, &summary);
    std::cout << summary.FullReport() << "\n";
    std::cout << "shape_params : " << pred_shape.transpose() << "\n";
    std::cout << "renderer.counts_ = " << renderer.counts_ << std::endl;
  }

  {
    Renderer renderer(W, H);
    Renderer::Image32FC1 predicted_image = renderer(target_pose.cast<ParamScalar>(),  pred_shape.cast<ParamScalar>());
    saveImageAs8UC1(predicted_image, "predicted_image.png");
  }

  return EXIT_SUCCESS;
}
