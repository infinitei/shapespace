/**
 * @file renderSingleImageSMPLInfoFiles.cpp
 * @brief renderSingleImageSMPLInfoFiles
 *
 * @author Abhijit Kundu
 */

#include "ShapeSpace/SMPLMeshRenderer.h" // Should be included 1st
#include "ShapeSpace/SMPLmodel.h"
#include "ShapeSpace/H5EigenTensor.h"
#include "ShapeSpace/H5StdContainers.h"
#include "ShapeSpace/ImageUtils.h"

#include <CuteGL/Surface/OffScreenRenderViewer.h>
#include <CuteGL/Geometry/ComputeNormals.h>
#include <CuteGL/Core/PoseUtils.h>

#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include <boost/format.hpp>

#include <QGuiApplication>

struct PosePCADecoder {

  PosePCADecoder(const std::string pose_pca_file) {
    H5::H5File file(pose_pca_file, H5F_ACC_RDONLY);
    H5Eigen::load(file, "pose_mean", pose_mean);
    H5Eigen::load(file, "pose_basis", pose_basis);
    assert(pose_basis.rows() == 69);
    assert(pose_mean.size() == 69);
  }

  template<class Derived>
  Eigen::VectorXf operator()(const Eigen::MatrixBase<Derived>& encoded_pose) const {
    Eigen::Matrix<float, 72, 1> pose_param;
    pose_param.tail<69>() = pose_mean + pose_basis * encoded_pose;
    pose_param.head<3>().setZero();
    return pose_param;
  }

  Eigen::Matrix<float, 69, 1> pose_mean;
  Eigen::Matrix<float, 69, Eigen::Dynamic> pose_basis;
};

int main(int argc, char **argv) {
  using namespace ShapeSpace;
  namespace po = boost::program_options;
  namespace fs = boost::filesystem;
  using namespace CuteGL;
  using namespace ShapeSpace;

  po::options_description generic_options("Generic Options");
  generic_options.add_options()("help,h", "Help screen");

  po::options_description config_options("Config");
  config_options.add_options()
      ("surreal_info_file,i", po::value<fs::path>(),"SURREAL info file in HDF5 format")
      ("gender,g", po::value<std::string>()->default_value("neutral"), "smpl model gender")
      ("pose_pca_file,p", po::value<std::string>()->default_value("pose_pca10_cmu_h36m.h5"), "Path pca pose file")
      ;

  po::positional_options_description p;
  p.add("surreal_info_file", 1);

  po::options_description cmdline_options;
  cmdline_options.add(generic_options).add(config_options);

  po::variables_map vm;

  try {
    po::store(po::command_line_parser(argc, argv).options(cmdline_options).positional(p).run(), vm);
    po::notify(vm);
  } catch (const po::error &ex) {
    std::cerr << ex.what() << '\n';
    std::cout << cmdline_options << '\n';
    return EXIT_FAILURE;
  }

  if (vm.count("help")) {
    std::cout << cmdline_options << '\n';
    return EXIT_SUCCESS;
  }

  if (!vm.count("surreal_info_file")) {
    std::cout << "Error: Please provide surreal_info_file" << '\n';
    std::cout << cmdline_options << '\n';
    return EXIT_FAILURE;
  }

  const fs::path surreal_info_file = vm["surreal_info_file"].as<fs::path>();

  const boost::format smpl_file_fmt("smpl_%s_lbs_10_207_0.h5");
  const std::string gender = vm["gender"].as<std::string>();

  const fs::path pose_pca_file = vm["pose_pca_file"].as<std::string>();

  if (pose_pca_file.extension() != ".h5") {
    std::cout << "Not a HDF5 file: " << pose_pca_file << '\n';
    return EXIT_FAILURE;
  }

  PosePCADecoder pca_decoder(pose_pca_file.string());

  using MeshType = CuteGL::SMPLMeshRenderer::MeshType;
  using SMPL = SMPLmodel<MeshType::PositionScalar>;
  using Image32FC1 = Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>;

  SMPL smpl((boost::format(smpl_file_fmt) % gender).str());

  QGuiApplication app(argc, argv);

  // Create the Renderer
  std::unique_ptr<SMPLMeshRenderer> renderer(new SMPLMeshRenderer);
  renderer->setDisplayGrid(false);
  renderer->setDisplayAxis(false);

  const Eigen::Vector2i image_size(320, 240);
  const Eigen::Matrix3f K(
      (Eigen::Matrix3f() << 600.0f, 0.0f, 160.0f, 0.0f, 600.0f, 120.0f, 0.0f, 0.0f, 1.0f).finished());

  // Set the viewer up
  OffScreenRenderViewer viewer(renderer.get());

  viewer.setBackgroundColor(0, 0, 0);
  viewer.resize(image_size.x(), image_size.y());

  viewer.camera().intrinsics() = getGLPerspectiveProjection(K, image_size.x(), image_size.y(), 0.01f, 100.0f);

  MeshType mesh;
  mesh.positions = smpl.template_vertices;
  mesh.colors = MeshType::ColorType(180, 180, 180, 255).replicate(mesh.positions.rows(), 1);
  mesh.faces = smpl.faces;
  CuteGL::computeNormals(mesh);

  viewer.create();
  viewer.makeCurrent();

  renderer->initMeshDrawer(mesh);
  renderer->initLineDrawer();

  Image32FC1 image(image_size.y(), image_size.x());

  Eigen::Vector3f camera_location;
  Eigen::Matrix3f camera_orientation;
  Eigen::Matrix<float, 69, 1> body_pose;
  Eigen::Matrix<float, 10, 1> body_shape;
  Eigen::Vector3f pelvis_location;
  Eigen::Matrix3f pelvis_orientation;
  {
    H5::H5File file(surreal_info_file.string(), H5F_ACC_RDONLY);

    std::string gender_;
    H5StdContainers::load(file, "gender", gender_);
    if (gender_ != gender) {
      std::cout << "info file " << surreal_info_file << " is of different gender (" << gender_
                << ") than expected (" << gender << ")\n";
//        return EXIT_FAILURE;
    }

    Eigen::Vector2i image_size_;
    H5Eigen::load(file, "image_size", image_size_);
    if (image_size_ != image_size) {
      std::cout << "info file " << surreal_info_file << " is of different image size\n";
      return EXIT_FAILURE;
    }

    Eigen::Matrix3f K_;
    H5Eigen::load(file, "camera_intrinsic", K_);
    if (K_ != K) {
      std::cout << "info file " << surreal_info_file << " is of different intrinsic\n";
      return EXIT_FAILURE;
    }

    H5Eigen::load(file, "camera_location", camera_location);
    H5Eigen::load(file, "camera_orientation", camera_orientation);

    H5Eigen::load(file, "body_pose", body_pose);
    H5Eigen::load(file, "body_shape", body_shape);

    H5Eigen::load(file, "pelvis_location", pelvis_location);
    H5Eigen::load(file, "pelvis_orientation", pelvis_orientation);
  }

  {
    Eigen::Isometry3f camera_world = Eigen::Isometry3f::Identity();
    camera_world.translation() = camera_location;
    camera_world.linear() = camera_orientation;
    viewer.camera().extrinsics() = camera_world.inverse();

    Eigen::Matrix<float, 72, 1> full_pose;
    full_pose.setZero();
    full_pose.tail<69>() = body_pose;

    SMPL::MatrixX3 rest_joint_positions;
    smpl.computeVertices(full_pose, body_shape, mesh.positions, rest_joint_positions);

    const Eigen::Vector3f pelvis_location_rest = rest_joint_positions.row(0);

    Eigen::Affine3f model_mat = Eigen::Translation3f(pelvis_location) * Eigen::AngleAxisf(pelvis_orientation)
        * Eigen::Translation3f(-pelvis_location_rest);

    renderer->modelMat() = model_mat;

    renderer->meshDrawer().updateVBO(mesh.positions);

    viewer.render();

    {
      QImage image = viewer.readColorBuffer();
      std::string image_name = surreal_info_file.stem().string() + ".png";
      image.save(QString::fromStdString(image_name));
    }
  }

  {
    Eigen::VectorXf encoded_pose = pca_decoder.pose_basis.transpose() * (body_pose - pca_decoder.pose_mean);
    Eigen::Matrix<float, 72, 1> decoded_pose = pca_decoder(encoded_pose);
    std::cout << body_pose.transpose() << "\n\n";
    std::cout << decoded_pose.tail<69>().transpose() << "\n\n";
    std::cout << (body_pose - decoded_pose.tail<69>()).norm() << "\n";

    Eigen::Isometry3f camera_world = Eigen::Isometry3f::Identity();
    camera_world.translation() = camera_location;
    camera_world.linear() = camera_orientation;
    viewer.camera().extrinsics() = camera_world.inverse();

    SMPL::MatrixX3 rest_joint_positions;
    smpl.computeVertices(decoded_pose, body_shape, mesh.positions, rest_joint_positions);

    const Eigen::Vector3f pelvis_location_rest = rest_joint_positions.row(0);

    Eigen::Affine3f model_mat = Eigen::Translation3f(pelvis_location) * Eigen::AngleAxisf(pelvis_orientation)
        * Eigen::Translation3f(-pelvis_location_rest);

    renderer->modelMat() = model_mat;

    renderer->meshDrawer().updateVBO(mesh.positions);

    viewer.render();

    {
      QImage image = viewer.readColorBuffer();
      std::string image_name = surreal_info_file.stem().string() + "_pose_pca.png";
      image.save(QString::fromStdString(image_name));
    }

  }

  return EXIT_SUCCESS;
}

