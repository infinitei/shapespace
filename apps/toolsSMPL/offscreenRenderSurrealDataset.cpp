/**
 * @file offscreenRenderSurrealDataset.cpp
 * @brief offscreenRenderSurrealDataset
 *
 * @author Abhijit Kundu
 */

#include "ShapeSpace/SMPLMeshRenderer.h" // Should be included 1st
#include "ShapeSpace/SMPLmodel.h"
#include "ShapeSpace/H5EigenDense.h"
#include "ShapeSpace/H5EigenTensor.h"

#include <CuteGL/Surface/OffScreenRenderViewer.h>
#include <CuteGL/Geometry/ComputeNormals.h>
#include <CuteGL/Core/PoseUtils.h>

#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include <QApplication>

template<class DerivedJoints, class DerivedParents>
CuteGL::LineDrawer::LinesPositionContainer computeLinesFromJoints(
    const Eigen::DenseBase<DerivedJoints>& joint_positions, const Eigen::DenseBase<DerivedParents>& parents) {
  using Scalartype = CuteGL::LineDrawer::PositionType::Scalar;
  CuteGL::LineDrawer::LinesPositionContainer lines;
  for (Eigen::Index i = 1; i < parents.size(); ++i) {
    lines.emplace_back(joint_positions.row(parents[i]).template cast<Scalartype>(),
                       joint_positions.row(i).template cast<Scalartype>());
  }
  return lines;
}

int main(int argc, char **argv) {
  namespace po = boost::program_options;
  namespace fs = boost::filesystem;
  using namespace CuteGL;
  using namespace ShapeSpace;


  po::options_description generic_options("Generic Options");
  generic_options.add_options()("help,h", "Help screen");

  po::options_description config_options("Config");
  config_options.add_options()
    ("surreal_info_file", po::value<std::string>(), "SURREAL info file (HDF5) format")
    ("smpl_model_file,m", po::value<std::string>()->default_value("smpl_male_lbs_10_207_0.h5"), "Path to SMPL model file")
    ;

  po::positional_options_description p;
  p.add("surreal_info_file", 1);

  po::options_description cmdline_options;
  cmdline_options.add(generic_options).add(config_options);

  po::variables_map vm;

  try {
    po::store(po::command_line_parser(argc, argv).options(cmdline_options).positional(p).run(), vm);
    po::notify(vm);
  } catch (const po::error &ex) {
    std::cerr << ex.what() << '\n';
    std::cout << cmdline_options << '\n';
    return EXIT_FAILURE;
  }

  if (vm.count("help")) {
    std::cout << cmdline_options << '\n';
    return EXIT_SUCCESS;
  }

  if (!vm.count("surreal_info_file")) {
    std::cout << "Please provide surreal_info_file" << '\n';
    std::cout << cmdline_options << '\n';
    return EXIT_FAILURE;
  }

  const fs::path surreal_info_file = vm["surreal_info_file"].as<std::string>();
  const fs::path smpl_model_file = vm["smpl_model_file"].as<std::string>();

  if (surreal_info_file.extension() != ".h5") {
    std::cout << "Not a HDF5 file: " << surreal_info_file << '\n';
    return EXIT_FAILURE;
  }

  const Eigen::IOFormat fmt(Eigen::StreamPrecision, Eigen::DontAlignCols, ", ", ", ", "", "", "[", "]");


  Eigen::Vector2i image_size;
  Eigen::Matrix3f K;
  Eigen::Vector3f camera_location;
  Eigen::Matrix3f camera_orientation;

  Eigen::MatrixXf body_poses;
  Eigen::Matrix<float, 10, 1> body_shape;

  Eigen::Matrix<float, 3, Eigen::Dynamic> pelvis_locations;
  Eigen::Tensor<float, 3> pelvis_orientations;
  {
    H5::H5File file(surreal_info_file.string(), H5F_ACC_RDONLY);
    H5Eigen::load(file, "image_size", image_size);
    H5Eigen::load(file, "camera_intrinsic", K);
    H5Eigen::load(file, "camera_location", camera_location);
    H5Eigen::load(file, "camera_orientation", camera_orientation);

    H5Eigen::load(file, "body_poses", body_poses);
    H5Eigen::load(file, "body_shape", body_shape);

    H5Eigen::load(file, "pelvis_locations", pelvis_locations);
    H5Eigen::load(file, "pelvis_orientations", pelvis_orientations);
  }


  std::cout << image_size.format(fmt) << "\n";
  std::cout << K << "\n";
  std::cout << camera_location.format(fmt) << "\n";
  std::cout << body_shape.format(fmt) << "\n";
  std::cout << body_poses.size() << "\n";

  using MeshType = CuteGL::SMPLMeshRenderer::MeshType;
  MeshType mesh;
  using SMPL = SMPLmodel<MeshType::PositionScalar>;
  SMPL::MatrixX3 rest_joint_positions;

  Eigen::Matrix<float, 72, 1> pose;
  pose.setZero();

  SMPL smpl(smpl_model_file.string());
  smpl.computeVertices(pose, body_shape, mesh.positions, rest_joint_positions);
  mesh.colors = MeshType::ColorType(180, 180, 180, 255).replicate(mesh.positions.rows(), 1);
  mesh.faces = smpl.faces;
  CuteGL::computeNormals(mesh);

  // Pelvis location for fixed shape do not change under pose
  const Eigen::Vector3f pelvis_location = rest_joint_positions.row(0);

  QApplication app(argc, argv);

  // Create the Renderer
  std::unique_ptr<SMPLMeshRenderer> renderer(new SMPLMeshRenderer);
  renderer->setDisplayGrid(false);
  renderer->setDisplayAxis(false);

  // Set the viewer up
  OffScreenRenderViewer viewer(renderer.get());

  viewer.setBackgroundColor(55, 55, 55);
  viewer.resize(image_size.x(), image_size.y());


  viewer.camera().intrinsics() = CuteGL::getGLPerspectiveProjection(K, image_size.x(), image_size.y(), 0.01f, 100.0f);

  Eigen::Isometry3f camera_world = Eigen::Isometry3f::Identity();
  camera_world.translation() = camera_location;
  camera_world.linear() = camera_orientation;

  std::cout << "camera_world.matrix() = \n" << camera_world.matrix() << "\n";
  viewer.camera().extrinsics() = camera_world.inverse();

  viewer.create();
  viewer.makeCurrent();

  renderer->initMeshDrawer(mesh);
  renderer->initLineDrawer();

  for (Eigen::Index t = 0; t< body_poses.cols(); ++t) {
    std::cout << "------- Working on Frame: " << t << " --------------" << std::endl;

    Eigen::AngleAxisf pelvis_orientation(Eigen::Map<Eigen::Matrix3f>(pelvis_orientations.data() + 9 * t));
    pose.head<3>() = pelvis_orientation.angle() * pelvis_orientation.axis();

    pose.tail<69>() = body_poses.col(t);

    smpl.computeVertices(pose, body_shape, mesh.positions, rest_joint_positions);
    CuteGL::computeNormals(mesh);

    Eigen::Vector3f current_frame_pelvis_joint = pelvis_locations.col(t);
    std::cout << "current_frame_pelvis_joint = " << current_frame_pelvis_joint.format(fmt) << "\n";

    Eigen::Affine3f model_mat = Eigen::Affine3f::Identity();
    model_mat.translation() = current_frame_pelvis_joint - pelvis_location;
//    std::cout << model_mat.matrix() << "\n";
    renderer->modelMat() = model_mat;

    renderer->meshDrawer().updateVBO(mesh.positions);
    renderer->meshDrawer().updateVBO(mesh.normals, mesh.sizeOfPositions());

    viewer.render();
    QImage image = viewer.readNormalBuffer();
    QString out_image = QString("out_%1.png").arg(t, 4, 10, QChar('0'));
    image.save(out_image);
  }



  return EXIT_SUCCESS;
}


