/**
 * @file animateSMPLPosePCA.cpp
 * @brief animateSMPLPosePCA
 *
 * @author Abhijit Kundu
 */

#include <CuteGL/Drawers/SMPLDrawer.h> // Should be included 1st
#include "ShapeSpace/SMPLmodel.h"
#include "ShapeSpace/H5EigenDense.h"

#include <CuteGL/Geometry/ComputeNormals.h>
#include <CuteGL/Surface/WindowRenderViewer.h>
#include <CuteGL/Renderer/BasicLightRenderer.h>
#include <CuteGL/Utils/QtUtils.h>
#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include <QGuiApplication>
#include <memory>
#include <iostream>

struct PosePCADecoder {

  PosePCADecoder(const std::string pose_pca_file) {
    H5::H5File file(pose_pca_file, H5F_ACC_RDONLY);
    H5Eigen::load(file, "pose_mean", pose_mean);
    H5Eigen::load(file, "pose_basis", pose_basis);
    H5Eigen::load(file, "encoded_training_data", encoded_training_data);
    assert(pose_basis.rows() == 69);
    assert(pose_basis.cols() == 10);
    assert(pose_mean.size() == 69);
  }


  template <class Derived>
  Eigen::VectorXf operator()(const Eigen::MatrixBase<Derived>& encoded_pose) const {
    Eigen::Matrix<float, 72, 1> pose_param;
    pose_param.tail<69>() = pose_mean + pose_basis * encoded_pose;
    pose_param.head<3>().setZero();
    return pose_param;
  }

  Eigen::Matrix<float, 69, 1> pose_mean;
  Eigen::Matrix<float, 69, 10> pose_basis;
  Eigen::Matrix<float, 10, Eigen::Dynamic> encoded_training_data;
};

namespace CuteGL {

class MeshRenderer : public BasicLightRenderer {
 public:
  using SMPLDataType = SMPLData<float>;
  using PoseParam = Eigen::Matrix<float, 10, 1>;
  using AnimationData = Eigen::AlignedStdVector<PoseParam>;


  MeshRenderer(const PosePCADecoder& pca_decoder, const AnimationData& anim_data)
   :smpl_drawer_(),
    model_mat_(Eigen::Affine3f::Identity()),
    pca_decoder_(pca_decoder),
    animation_data_(anim_data),
    time_(0) {
  }

  void init(PhongShader& shader);

  const Eigen::Affine3f& modelMat() const {return model_mat_;}
  Eigen::Affine3f& modelMat() {return model_mat_;}

 protected:
  virtual void draw(PhongShader& shader);
 private:
  SMPLDrawer<SMPLDataType> smpl_drawer_;
  Eigen::Affine3f model_mat_;
  const PosePCADecoder& pca_decoder_;
  const AnimationData& animation_data_;
  std::size_t time_;


 public:
   EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

void MeshRenderer::init(PhongShader& shader) {
  smpl_drawer_.init(shader.program, "smpl_male_lbs_10_207_0.h5");
  shader.program.bind();
  glfuncs_->glUniform3f(shader.program.uniformLocation("light_position_world"), 1.0f, 2.1f, 5.0f);
  shader.program.release();
}

void MeshRenderer::draw(PhongShader& shader) {
  shader.setModelPose(model_mat_);
  smpl_drawer_.draw();

  if (time_ < animation_data_.size()) {
    smpl_drawer_.pose() = pca_decoder_(animation_data_[time_]);
    smpl_drawer_.updateShapeAndPose();
    ++time_;
  }
}

}  // namespace CuteGL


int main(int argc, char **argv) {
  namespace po = boost::program_options;
  namespace fs = boost::filesystem;
  using namespace CuteGL;
  using namespace ShapeSpace;

  po::options_description generic_options("Generic Options");
  generic_options.add_options()("help,h", "Help screen");

  po::options_description config_options("Config");
  config_options.add_options()
    ("pose_pca_file", po::value<std::string>(), "Path pca pose file")
    ("use_vsync,v", po::value<bool>()->default_value(true), "Enable disable vsync")
    ("num_of_iterpolations,n", po::value<Eigen::Index>()->default_value(10), "Enable disable vsync")
    ;

  po::positional_options_description p;
  p.add("pose_pca_file", 1);

  po::options_description cmdline_options;
  cmdline_options.add(generic_options).add(config_options);

  po::variables_map vm;

  try {
    po::store(po::command_line_parser(argc, argv).options(cmdline_options).positional(p).run(), vm);
    po::notify(vm);
  } catch (const po::error &ex) {
    std::cerr << ex.what() << '\n';
    std::cout << cmdline_options << '\n';
    return EXIT_FAILURE;
  }

  if (vm.count("help")) {
    std::cout << cmdline_options << '\n';
    return EXIT_SUCCESS;
  }

  if (!vm.count("pose_pca_file")) {
    std::cout << "Please provide surreal_info_file" << '\n';
    std::cout << cmdline_options << '\n';
    return EXIT_FAILURE;
  }

  const fs::path pose_pca_file = vm["pose_pca_file"].as<std::string>();

  if (pose_pca_file.extension() != ".h5") {
    std::cout << "Not a HDF5 file: " << pose_pca_file << '\n';
    return EXIT_FAILURE;
  }


  PosePCADecoder pca_decoder(pose_pca_file.string());

  std::cout << "Generating animating data ..." << std::flush;
  MeshRenderer::AnimationData anim_data;
  {
    for (Eigen::Index i = 1; i < pca_decoder.encoded_training_data.cols(); ++i) {
      const Eigen::Index num_of_iterpolations =  vm["num_of_iterpolations"].as<Eigen::Index>();
      const MeshRenderer::PoseParam base = pca_decoder.encoded_training_data.col(i-1);
      MeshRenderer::PoseParam incr = (1.0 / num_of_iterpolations) * (pca_decoder.encoded_training_data.col(i) - base);

      for (Eigen::Index j = 0; j < num_of_iterpolations; ++j) {
        anim_data.push_back(base + j * incr);
      }
    }
  }
  std::cout << "Done. With " << anim_data.size() << " frames." << std::endl;

  QGuiApplication app(argc, argv);

  // Create the Renderer
  std::unique_ptr <MeshRenderer> renderer(new MeshRenderer(pca_decoder, anim_data));
  renderer->setDisplayGrid(true);

  // Set the viewer up
  WindowRenderViewer viewer(renderer.get());
  viewer.setBackgroundColor(55, 55, 55);
  viewer.resize(1024, 768);
  viewer.setDisplayFPS(true);

  if (!vm["use_vsync"].as<bool>())
    setSwapIntervalToZero(viewer);


  // Set camera pose via lookAt
  viewer.setCameraToLookAt(Eigen::Vector3f(0.0f, -0.35f, 2.6f),
                           Eigen::Vector3f(0.0f, -0.35f, 0.0f),
                           Eigen::Vector3f::UnitY());

  viewer.showAndWaitTillExposed();

  return app.exec();
}
