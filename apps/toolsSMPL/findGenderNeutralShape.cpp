/**
 * @file findGenderNeutralShape.cpp
 * @brief findGenderNeutralShape
 *
 * @author Abhijit Kundu
 */

#include "ShapeSpace/SMPLmodel.h"
#include "ShapeSpace/H5EigenTensor.h"
#include "ShapeSpace/H5StdContainers.h"

#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include <boost/format.hpp>
#include <iostream>

#include <ceres/ceres.h>
#include <glog/logging.h>


namespace ShapeSpace {

struct ShapeFunctor {
  using SMPL = SMPLmodel<double>;
  using MatrixX3 = SMPL::MatrixX3;


  ShapeFunctor(const MatrixX3& target_vertices, const SMPL& neutral_smpl) {
    const Eigen::Index N = target_vertices.rows();
    template_diff_ = neutral_smpl.template_vertices - target_vertices;

    blend_shapes_.resize(10);
    for (Eigen::Index b = 0; b < 10; ++b) {
      MatrixX3& shape_basis = blend_shapes_[b];
      shape_basis.resize(N, 3);
      for (Eigen::Index i = 0; i < N; ++i)
        for (Eigen::Index j = 0; j < 3; ++j) {
          shape_basis(i, j) = neutral_smpl.shape_displacements(i , j, b);
        }
    }
  }

  template <typename T>
  bool operator()(const T* const shape, T* residuals_) const {
    using MatrixX3T = Eigen::Matrix<T, Eigen::Dynamic, 3, Eigen::RowMajor>;

    const Eigen::Index N = template_diff_.rows();

    MatrixX3T SD(N, 3);
    SD.setZero();
    for (Eigen::Index b = 0; b < 10; ++b) {
      SD += shape[b] * blend_shapes_[b];
    }

    Eigen::Map<MatrixX3T> residuals(residuals_, N, 3);
    residuals = template_diff_ + SD;
    return true;
  }

 private:
  MatrixX3 template_diff_;
  std::vector<MatrixX3> blend_shapes_;
};

}  // namespace ShapeSpace




int main(int argc, char **argv) {
  google::InitGoogleLogging(argv[0]);
  using namespace ShapeSpace;
  namespace po = boost::program_options;
  namespace fs = boost::filesystem;

  po::options_description generic_options("Generic Options");
  generic_options.add_options()("help,h", "Help screen");

  po::options_description config_options("Config");
  config_options.add_options()
    ("surreal_info_file,i", po::value<std::string>(), "SURREAL info file (HDF5) format")
    ("out_dir,o", po::value<std::string>()->default_value("./"), "SURREAL info file (HDF5) format")
    ("force_overwrite,f", po::value<bool>()->default_value(false), "Force overwrite existing files")
    ;

  po::positional_options_description p;
  p.add("surreal_info_file", 1);

  po::options_description cmdline_options;
  cmdline_options.add(generic_options).add(config_options);

  po::variables_map vm;

  try {
    po::store(po::command_line_parser(argc, argv).options(cmdline_options).positional(p).run(), vm);
    po::notify(vm);
  } catch (const po::error &ex) {
    std::cerr << ex.what() << '\n';
    std::cout << cmdline_options << '\n';
    return EXIT_FAILURE;
  }

  if (vm.count("help")) {
    std::cout << cmdline_options << '\n';
    return EXIT_SUCCESS;
  }

  if (!vm.count("surreal_info_file")) {
    std::cout << "Error: Please provide surreal_info_file" << '\n';
    std::cout << cmdline_options << '\n';
    return EXIT_FAILURE;
  }

  const fs::path surreal_info_file = vm["surreal_info_file"].as<std::string>();
  if (surreal_info_file.extension() != ".h5") {
    std::cout << "Error: Not a HDF5 file: " << surreal_info_file << '\n';
    return EXIT_FAILURE;
  }

  const fs::path out_dir = vm["out_dir"].as<std::string>();
  if (! fs::is_directory(out_dir)) {
    std::cout << "Error: " << out_dir << " is not a directory\n";
    return EXIT_FAILURE;
  }

  using SMPL = SMPLmodel<double>;

  SMPL::Tensor1 body_shape;
  std::string gender;
  {
    H5::H5File file(surreal_info_file.string(), H5F_ACC_RDONLY);
    H5Eigen::load(file, "body_shape", body_shape);

    H5StdContainers::load(file, "gender", gender);
  }

  const boost::format smpl_file_fmt("smpl_%s_lbs_10_207_0.h5");
  const SMPL neutral_smpl((boost::format(smpl_file_fmt) % "neutral").str());
  const SMPL gender_sp_smpl((boost::format(smpl_file_fmt) % gender).str());

  const Eigen::Index N =  neutral_smpl.template_vertices.rows();

  if (gender_sp_smpl.template_vertices.rows() != N) {
    std::cout << "Cannot handle models with different number of vertices\n";
    return EXIT_FAILURE;
  }

  SMPL::MatrixX3 target_vertices;
  {
    using DimPair = SMPL::Tensor1::DimensionPair;
    Eigen::array<DimPair, 1> product_dims = { { DimPair(2, 0) } };
    SMPL::Tensor2 SD = gender_sp_smpl.shape_displacements.contract(body_shape, product_dims);
    target_vertices = gender_sp_smpl.template_vertices + Eigen::Map<SMPL::MatrixX3>(SD.data(), N, 3);
  }

//  {
//    ShapeFunctor sf(target_vertices, gender_sp_smpl);
//    SMPL::MatrixX3 residuals (N, 3);
//    const SMPL::Tensor1 shape = body_shape;
//    sf(shape.data(), residuals.data());
//    std::cout << residuals.cwiseAbs().sum() << "\n";
//    assert(residuals.cwiseAbs().sum() < 1e-6);
//  }


  SMPL::Tensor1 neutral_shape = body_shape;

  {
    using ceres::AutoDiffCostFunction;
    using ceres::CostFunction;
    using ceres::Problem;
    using ceres::Solver;
    using ceres::Solve;

    // Build the problem.
    Problem problem;

    CostFunction* cost_function =
          new AutoDiffCostFunction<ShapeFunctor, ceres::DYNAMIC, 10>(
              new ShapeFunctor(target_vertices, neutral_smpl),
              target_vertices.size());

    problem.AddResidualBlock(cost_function, NULL, neutral_shape.data());

    // Run the solver!
    Solver::Options options;
    options.function_tolerance = 1e-10;
    options.parameter_tolerance = 1e-12;
    options.minimizer_progress_to_stdout = false;
    Solver::Summary summary;
    Solve(options, &problem, &summary);

    if (!summary.IsSolutionUsable()) {
      std::cout << "Error: Failed to converge\n";
      std::cout << summary.FullReport() << "\n";
      return EXIT_FAILURE;
    }
  }

  {
    fs::path neutral_info_file = out_dir / surreal_info_file.filename();

    fs::copy_option copy_option = vm["force_overwrite"].as<bool>() ? fs::copy_option::overwrite_if_exists : fs::copy_option::fail_if_exists;
    fs::copy_file(surreal_info_file, neutral_info_file, copy_option);

    H5::H5File file(neutral_info_file.string(), H5F_ACC_RDWR);

    Eigen::Tensor<float, 1, Eigen::RowMajor> updated_shape = neutral_shape.cast<float>();
    H5Eigen::replace(file, "body_shape", updated_shape);

    H5StdContainers::save(file, "gender", "neutral");

    std::cout << "Saved neutral body info at " << neutral_info_file << std::endl;
  }

  return EXIT_SUCCESS;
}



