/**
 * @file demoSMPLmodel.cpp
 * @brief demoSMPLmodel
 *
 * @author Abhijit Kundu
 */

#include "ShapeSpace/EigenTypedefs.h"
#include "ShapeSpace/SMPLmodel.h"
#include "ShapeSpace/H5EigenDense.h"
#include <CuteGL/IO/ImportPLY.h>
#include <CuteGL/Geometry/ComputeNormals.h>
#include <CuteGL/Utils/ColorUtils.h>
#include <iostream>
#include <random>

int main(int argc, char **argv) {
  using namespace ShapeSpace;
  using namespace CuteGL;

  using SMPL = SMPLmodel<float>;
  SMPL smpl("smpl_female_lbs_10_207_0.h5");

  assert(
      smpl.template_vertices.isApprox(
          smpl.computeVertices(Eigen::VectorXf::Zero(72), Eigen::VectorXf::Zero(10))));

  Eigen::VectorXf poses = 0.0f * Eigen::VectorXf::Random(72);
  Eigen::VectorXf betas = 0.03f * Eigen::VectorXf::Random(10);

  betas[1] = -2.0f;
  poses[6] = -0.5;
  poses[7] = -0.5;
  poses[8] = -1.3;

  SMPL::MatrixX3 vertex_positions = smpl.computeVertices(poses, betas);

  MeshData mesh;
  mesh.vertices.resize(vertex_positions.rows());

  for (Eigen::Index i = 0; i < vertex_positions.rows(); ++i) {
    MeshData::VertexData& vd = mesh.vertices[i];
    vd.position = vertex_positions.row(i);
    vd.color = MeshData::ColorType(180, 180, 180, 255);
  }

  enum ColorMode {
    BLEND_WEIGHTS,
    PARTS_24,
    PARTS_14
  };

  ColorMode color_mode = ColorMode::PARTS_14;

  // Use joint based color
  if (color_mode == BLEND_WEIGHTS) {
    Eigen::Matrix<float, 3, 24> colors;
    colors.setRandom();
    colors.array() += 1.0;
    colors *= 255.0;
    for (Eigen::Index i = 0; i < vertex_positions.rows(); ++i) {
      CuteGL::MeshData::VertexData& vd = mesh.vertices[i];
      Eigen::Vector3f rgb = colors * smpl.blend_weights.row(i).transpose();
      vd.color << rgb.cast<unsigned char>(), 255;
    }
  } else if (color_mode == PARTS_24) {

    Eigen::VectorXi vertex_segmm;
    {
      H5::H5File file("vertex_segm.h5", H5F_ACC_RDONLY);
      H5Eigen::load(file, "vertex_segmm_24", vertex_segmm);
    }
    assert(vertex_segmm.size() == vertex_positions.rows());

    Eigen::AlignedStdVector<MeshData::ColorType> colors(vertex_segmm.maxCoeff() + 1);
    {

      std::random_device rd;  //Will be used to obtain a seed for the random number engine
      std::mt19937 gen(rd());  //Standard mersenne_twister_engine seeded with rd()
      std::uniform_real_distribution<float> hue_dist(0.0f, 1.0f);
      std::uniform_real_distribution<float> sat_dist(0.95f, 1.0f);
      std::uniform_real_distribution<float> val_dist(0.95f, 1.0f);

      for (std::size_t i = 0; i < colors.size(); ++i) {
        const float hue = 360.0f * hue_dist(gen);
        colors[i] = makeRGBAfromHSV(hue, sat_dist(gen), val_dist(gen));
      }
    }

    for (Eigen::Index i = 0; i < vertex_positions.rows(); ++i) {
      MeshData::VertexData& vd = mesh.vertices[i];
      vd.color = colors[vertex_segmm[i]];
    }
  } else if (color_mode == PARTS_14) {

    Eigen::VectorXi vertex_segmm;
    {
      H5::H5File file("vertex_segm.h5", H5F_ACC_RDONLY);
      H5Eigen::load(file, "vertex_segmm_14", vertex_segmm);
    }
    assert(vertex_segmm.size() == vertex_positions.rows());

    Eigen::AlignedStdVector<MeshData::ColorType> colors(14);
    {
      colors[0] = MeshData::ColorType(217, 83, 25, 255);
      colors[1] = MeshData::ColorType(126, 47, 91, 255);
      colors[2] = MeshData::ColorType(234, 191, 213, 255);
      colors[3] = MeshData::ColorType(0, 62, 255, 255);
      colors[4] = MeshData::ColorType(94, 79, 162, 255);
      colors[5] = MeshData::ColorType(226, 157, 137, 255);
      colors[6] = MeshData::ColorType(0, 199, 255, 255);
      colors[7] = MeshData::ColorType(235, 167, 91, 255);
      colors[8] = MeshData::ColorType(237, 220, 103, 255);
      colors[9] = MeshData::ColorType(244, 35, 232, 255);
      colors[10] = MeshData::ColorType(46, 229, 0, 255);
      colors[11] = MeshData::ColorType(35, 255, 203, 255);
      colors[12] = MeshData::ColorType(242, 22, 41, 255);
      colors[13] = MeshData::ColorType(102, 23, 23, 255);
    }

    for (Eigen::Index i = 0; i < vertex_positions.rows(); ++i) {
      MeshData::VertexData& vd = mesh.vertices[i];
      vd.color = colors[vertex_segmm[i]];
    }
  }

  // Set faces
  mesh.faces = smpl.faces;

  // Set normals
  CuteGL::computeNormals(mesh);

  // Save  as PLY
  const std::string mesh_filename("posed_mesh.ply");
  CuteGL::saveMeshAsBinPly(mesh, mesh_filename);
  std::cout << "Mesh saved as " << mesh_filename << "\n";

  return EXIT_SUCCESS;
}

