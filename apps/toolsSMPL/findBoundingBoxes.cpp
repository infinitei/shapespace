/**
 * @file findBoundingBoxes.cpp
 * @brief findBoundingBoxes
 *
 * @author Abhijit Kundu
 */

#include "ShapeSpace/SMPLMeshRenderer.h" // Should be included 1st
#include "ShapeSpace/SMPLmodel.h"
#include "ShapeSpace/H5EigenTensor.h"
#include "ShapeSpace/H5StdContainers.h"

#include <CuteGL/Surface/OffScreenRenderViewer.h>
#include <CuteGL/Geometry/ComputeNormals.h>
#include <CuteGL/Core/PoseUtils.h>

#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include <boost/format.hpp>

#include <QApplication>

int main(int argc, char **argv) {
  using namespace ShapeSpace;
  namespace po = boost::program_options;
  namespace fs = boost::filesystem;
  using namespace CuteGL;
  using namespace ShapeSpace;

  po::options_description generic_options("Generic Options");
  generic_options.add_options()("help,h", "Help screen");

  po::options_description config_options("Config");
  config_options.add_options()
    ("surreal_info_file,i", po::value<std::string>(), "SURREAL info file (HDF5) format")
    ("out_dir,o", po::value<std::string>()->default_value("./"), "Out directory for updated SURREAL info file")
    ("force_overwrite,f", po::value<bool>()->default_value(false), "Force overwrite existing files")
    ;

  po::positional_options_description p;
  p.add("surreal_info_file", 1);

  po::options_description cmdline_options;
  cmdline_options.add(generic_options).add(config_options);

  po::variables_map vm;

  try {
    po::store(po::command_line_parser(argc, argv).options(cmdline_options).positional(p).run(), vm);
    po::notify(vm);
  } catch (const po::error &ex) {
    std::cerr << ex.what() << '\n';
    std::cout << cmdline_options << '\n';
    return EXIT_FAILURE;
  }

  if (vm.count("help")) {
    std::cout << cmdline_options << '\n';
    return EXIT_SUCCESS;
  }

  if (!vm.count("surreal_info_file")) {
    std::cout << "Error: Please provide surreal_info_file" << '\n';
    std::cout << cmdline_options << '\n';
    return EXIT_FAILURE;
  }

  const fs::path surreal_info_file = vm["surreal_info_file"].as<std::string>();
  if (surreal_info_file.extension() != ".h5") {
    std::cout << "Error: Not a HDF5 file: " << surreal_info_file << '\n';
    return EXIT_FAILURE;
  }

  std::string gender;
  Eigen::Vector2i image_size;
  Eigen::Matrix3f K;
  Eigen::Vector3f camera_location;
  Eigen::Matrix3f camera_orientation;

  Eigen::Matrix<float, 69, 1> body_pose;
  Eigen::Matrix<float, 10, 1> body_shape;

  Eigen::Vector3f pelvis_location;
  Eigen::Matrix3f pelvis_orientation;
  {
    H5::H5File file(surreal_info_file.string(), H5F_ACC_RDONLY);

    H5StdContainers::load(file, "gender", gender);

    H5Eigen::load(file, "image_size", image_size);
    H5Eigen::load(file, "camera_intrinsic", K);
    H5Eigen::load(file, "camera_location", camera_location);
    H5Eigen::load(file, "camera_orientation", camera_orientation);

    H5Eigen::load(file, "body_pose", body_pose);
    H5Eigen::load(file, "body_shape", body_shape);

    H5Eigen::load(file, "pelvis_location", pelvis_location);
    H5Eigen::load(file, "pelvis_orientation", pelvis_orientation);
  }

  using MeshType = CuteGL::SMPLMeshRenderer::MeshType;
  using SMPL = SMPLmodel<MeshType::PositionScalar>;

  SMPL smpl((boost::format("smpl_%s_lbs_10_207_0.h5") % gender).str());


  using Image32FC1 = Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>;
  Image32FC1 image(image_size.y(), image_size.x());

  // 0 based minx,miny,maxx,maxy
  Eigen::Vector4f visible_bbx;
  {
    QApplication app(argc, argv);

    // Create the Renderer
    std::unique_ptr<SMPLMeshRenderer> renderer(new SMPLMeshRenderer);
    renderer->setDisplayGrid(false);
    renderer->setDisplayAxis(false);

    // Set the viewer up
    OffScreenRenderViewer viewer(renderer.get());

    viewer.setBackgroundColor(0, 0, 0);
    viewer.resize(image_size.x(), image_size.y());

    viewer.camera().intrinsics() = CuteGL::getGLPerspectiveProjection(K, image_size.x(), image_size.y(), 0.01f, 100.0f);

    Eigen::Isometry3f camera_world = Eigen::Isometry3f::Identity();
    camera_world.translation() = camera_location;
    camera_world.linear() = camera_orientation;
    viewer.camera().extrinsics() = camera_world.inverse();

    Eigen::Matrix<float, 72, 1> full_pose;
    full_pose.setZero();

    Eigen::AngleAxisf pelvis_orientation_aa(pelvis_orientation);
    full_pose.head<3>() = pelvis_orientation_aa.angle() * pelvis_orientation_aa.axis();
    full_pose.tail<69>() = body_pose;

    SMPL::MatrixX3 rest_joint_positions;
    MeshType mesh;
    smpl.computeVertices(full_pose, body_shape, mesh.positions, rest_joint_positions);
    mesh.colors = MeshType::ColorType(180, 180, 180, 255).replicate(mesh.positions.rows(), 1);
    mesh.faces = smpl.faces;
    CuteGL::computeNormals(mesh);

    const Eigen::Vector3f pelvis_location_rest = rest_joint_positions.row(0);
    Eigen::Affine3f model_mat = Eigen::Affine3f::Identity();
    model_mat.translation() = pelvis_location - pelvis_location_rest;
    renderer->modelMat() = model_mat;

    viewer.create();
    viewer.makeCurrent();

    renderer->initMeshDrawer(mesh);
    renderer->initLineDrawer();

    viewer.render();
    viewer.readDepthBuffer(image.data());

    Eigen::Vector2i min(image_size.x(), image_size.y());
    Eigen::Vector2i max(0, 0);
    {
      for (Eigen::Index i = 0; i < image.rows(); ++i) {
        for (Eigen::Index j = 0; j < image.cols(); ++j) {
          if (image(i, j) > 0) {
            min.x() = std::min(min.x(), j);
            min.y() = std::min(min.y(), i);
            max.x() = std::max(max.x(), j);
            max.y() = std::max(max.y(), i);
          }
        }
      }
    }

    // 0 based minx,miny,maxx,maxy
    visible_bbx[0] = min.x();
    visible_bbx[1] = image_size.y() - max.y() - 1;
    visible_bbx[2] = max.x() + 1;
    visible_bbx[3] = image_size.y() - min.y();
  }

  {
    const fs::path out_dir = vm["out_dir"].as<std::string>();
    if (!fs::exists(out_dir)) {
      fs::create_directory(out_dir);
    }
    fs::path updated_info_file = out_dir / surreal_info_file.filename();

    fs::copy_option copy_option = vm["force_overwrite"].as<bool>() ? fs::copy_option::overwrite_if_exists : fs::copy_option::fail_if_exists;
    fs::copy_file(surreal_info_file, updated_info_file, copy_option);

    H5::H5File file(updated_info_file.string(), H5F_ACC_RDWR);
    H5Eigen::save(file, "visible_bbx", visible_bbx);

    std::cout << "Saved updated info at " << updated_info_file << std::endl;
  }

  return EXIT_SUCCESS;
}

