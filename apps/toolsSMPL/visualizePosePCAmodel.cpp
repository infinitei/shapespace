/**
 * @file visualizePosePCAmodel.cpp
 * @brief visualizePosePCAmodel
 *
 * @author Abhijit Kundu
 */

#include <CuteGL/Renderer/SMPLRenderer.h> // Should be included 1st
#include "ShapeSpace/SMPLmodel.h"
#include "ShapeSpace/H5EigenDense.h"

#include <CuteGL/Geometry/ComputeNormals.h>
#include <CuteGL/Surface/WindowRenderViewer.h>
#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include <QGuiApplication>
#include <memory>
#include <iostream>

namespace CuteGL {

class SMPLViewer : public WindowRenderViewer {
 public:
  using MatType = Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>;

  enum Action {
    NEXT_PARAM = 100,
    PREVIOUS_PARAM,
  };

  explicit SMPLViewer(SMPLRenderer* renderer);

  const MatType& poseParams() const {return pose_params_;}
  MatType& poseParams() {return pose_params_;}

 protected:
  virtual void handleKeyboardAction(int action);

 private:
  SMPLRenderer* renderer_;
  Eigen::Index param_index_;
  MatType pose_params_;
};

SMPLViewer::SMPLViewer(SMPLRenderer* renderer)
    : WindowRenderViewer(renderer),
      renderer_(renderer),
      param_index_(-1) {
  keyboard_handler_.registerKey(NEXT_PARAM, Qt::Key_Right, "Move to next param");
  keyboard_handler_.registerKey(PREVIOUS_PARAM, Qt::Key_Left, "Move to previous param");
}

void SMPLViewer::handleKeyboardAction(int action) {
  switch (action) {
    case NEXT_PARAM: {
      const int max_param_index_ = pose_params_.rows() - 1;
      if (param_index_ < max_param_index_) {
        ++param_index_;
        renderer_->smplDrawer().pose() = pose_params_.row(param_index_);
        renderer_->smplDrawer().updateShapeAndPose();
      }
      break;
    }
    case PREVIOUS_PARAM: {
      if (param_index_ > 0) {
        --param_index_;
        renderer_->smplDrawer().pose() = pose_params_.row(param_index_);
        renderer_->smplDrawer().updateShapeAndPose();
      }
      break;
    }
    default:
      WindowRenderViewer::handleKeyboardAction(action);
      break;
  }
}

}  // namespace CuteGL


int main(int argc, char **argv) {
  using namespace CuteGL;
  using namespace ShapeSpace;
  namespace po = boost::program_options;
  namespace fs = boost::filesystem;

  po::options_description generic_options("Generic Options");
  generic_options.add_options()("help,h", "Help screen");

  po::options_description config_options("Config");
  config_options.add_options()
    ("pose_pca_file", po::value<std::string>(), "Pose PCA file")
    ("smpl_model_file,m", po::value<std::string>()->default_value("smpl_male_lbs_10_207_0.h5"), "Path to SMPL model file")
    ;

  po::positional_options_description p;
  p.add("pose_pca_file", 1);

  po::options_description cmdline_options;
  cmdline_options.add(generic_options).add(config_options);

  po::variables_map vm;

  try {
    po::store(po::command_line_parser(argc, argv).options(cmdline_options).positional(p).run(), vm);
    po::notify(vm);
  } catch (const po::error &ex) {
    std::cerr << ex.what() << '\n';
    std::cout << cmdline_options << '\n';
    return EXIT_FAILURE;
  }

  if (vm.count("help")) {
    std::cout << cmdline_options << '\n';
    return EXIT_SUCCESS;
  }

  if (!vm.count("pose_pca_file")) {
    std::cout << "Please provide pose_pca_file" << '\n';
    std::cout << cmdline_options << '\n';
    return EXIT_FAILURE;
  }

  const std::string pose_pca_file = vm["pose_pca_file"].as<std::string>();
  const std::string smpl_model_file = vm["smpl_model_file"].as<std::string>();

  QGuiApplication app(argc, argv);

  // Create the Renderer
  std::unique_ptr <SMPLRenderer> renderer(new SMPLRenderer);
  renderer->setDisplayGrid(true);
  renderer->setDisplayAxis(true);

  // Set the viewer up
  SMPLViewer viewer(renderer.get());
  viewer.setBackgroundColor(55, 55, 55);
  viewer.resize(1024, 768);


  // Set camera pose via lookAt
  viewer.setCameraToLookAt(Eigen::Vector3f(0.0f, -0.35f, 2.6f),
                           Eigen::Vector3f(0.0f, -0.35f, 0.0f),
                           Eigen::Vector3f::UnitY());

  viewer.showAndWaitTillExposed();

  renderer->setSMPLData(loadSMPLDataFromHDF5<SMPLRenderer::SMPLDataType>(smpl_model_file));

  {
    Eigen::VectorXf pose_mean;
    Eigen::MatrixXf pose_basis;
    Eigen::MatrixXf encoded_training_data;

    H5::H5File file(pose_pca_file, H5F_ACC_RDONLY);
    H5Eigen::load(file, "pose_mean", pose_mean);
    H5Eigen::load(file, "pose_basis", pose_basis);
    H5Eigen::load(file, "encoded_training_data", encoded_training_data);
    assert(pose_basis.rows() == 69);
    assert(pose_basis.cols() == 10);
    assert(pose_mean.size() == 69);

    viewer.poseParams().setZero(encoded_training_data.cols(), 72);
    for (Eigen::Index i = 0; i < viewer.poseParams().rows(); ++i) {
      viewer.poseParams().row(i).tail(69) = pose_mean + pose_basis * encoded_training_data.col(i);
    }
  }

  return app.exec();
}


