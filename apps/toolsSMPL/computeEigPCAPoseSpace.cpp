/**
 * @file computeEigPCAPoseSpace.cpp
 * @brief computeEigPCAPoseSpace
 *
 * @author Abhijit Kundu
 */

#include "ShapeSpace/EigenTypedefs.h"
#include "ShapeSpace/H5EigenDense.h"

#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>
#include <iostream>

int main(int argc, char **argv) {
  namespace po = boost::program_options;
  namespace fs = boost::filesystem;

  po::options_description generic_options("Generic Options");
  generic_options.add_options()("help,h", "Help screen");

  po::options_description config_options("Config");
  config_options.add_options()
    ("input_smpl_pose_files", po::value< std::vector<std::string> >(), "Input SMPL HDF5 Pose files")
    ("reduced_dimension,r", po::value<Eigen::Index>()->default_value(10), "Reduced (output) dimension")
    ("out_file,o", po::value<std::string>()->default_value("pca_pose.h5"), "Path to save pose-space hdf5 file")
    ;

  po::positional_options_description p;
  p.add("input_smpl_pose_files", -1);

  po::options_description cmdline_options;
  cmdline_options.add(generic_options).add(config_options);

  po::variables_map vm;

  try {
    po::store(po::command_line_parser(argc, argv).options(cmdline_options).positional(p).run(), vm);
    po::notify(vm);
  } catch (const po::error &ex) {
    std::cerr << ex.what() << '\n';
    std::cout << cmdline_options << '\n';
    return EXIT_FAILURE;
  }

  if (vm.count("help")) {
    std::cout << cmdline_options << '\n';
    return EXIT_SUCCESS;
  }

  if (!vm.count("input_smpl_pose_files")) {
    std::cout << "Please provide at-least one pose file" << '\n';
    std::cout << cmdline_options << '\n';
    return EXIT_FAILURE;
  }

  const std::vector<std::string> files = vm["input_smpl_pose_files"].as<std::vector<std::string>>();
  const Eigen::Index reduced_dimension = vm["reduced_dimension"].as<Eigen::Index>();

  std::cout << "User provided " << files.size() << " pose files\n";

  Eigen::MatrixXd pose_data;
  {
    using RMMatrixXd = Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>;
    std::vector<RMMatrixXd> all_pose_data(files.size());
  #pragma omp parallel for
    for (std::size_t i = 0; i < files.size(); ++i) {
      H5::H5File file(files[i], H5F_ACC_RDONLY);
      H5Eigen::load(file, "poses", all_pose_data[i]);
      assert(all_pose_data[i].cols() == 72);
    }

    Eigen::Index num_of_data_points = 0;
    for (const RMMatrixXd& poses : all_pose_data) {
      num_of_data_points += poses.rows();
    }

    pose_data.resize(69, num_of_data_points);
    Eigen::Index i = 0;
    for (const RMMatrixXd& poses : all_pose_data) {
      for (Eigen::Index k= 0; k < poses.rows(); ++k) {
        pose_data.col(i++) = poses.row(k).tail(69);
      }
    }
  }

//  {
//    H5::H5File file("pose_data.h5", H5F_ACC_TRUNC);
//    H5Eigen::save(file, "pose_data", pose_data);
//  }

  std::cout << "We have " << pose_data.cols() << " data points\n";

  std::cout << "original_dimension =  " << pose_data.rows() << " reduced_dimension =  " << reduced_dimension << "\n";
  if(pose_data.rows() < reduced_dimension) {
    std::cout << "Error: original_dimension < reduced_dimension\n";
    return EXIT_FAILURE;
  }

  Eigen::VectorXd pose_mean;
  Eigen::MatrixXd pose_basis;
  Eigen::MatrixXd encoded_training_data;
  {
    std::cout << "Learning PCA pose-space via SelfAdjointEigenSolver .." << std::flush;
    pose_mean = pose_data.rowwise().mean();
    Eigen::MatrixXd centered = pose_data.colwise() - pose_mean;

    Eigen::MatrixXd cov = centered * centered.adjoint();

    Eigen::SelfAdjointEigenSolver<Eigen::MatrixXd> eig(cov);
    pose_basis = eig.eigenvectors().rightCols(reduced_dimension);
    encoded_training_data = pose_basis.transpose() * centered;

//    std::cout << svd.singularValues() << std::endl;

    std::cout << " Done" << std::endl;
  }

  {
    Eigen::MatrixXd reconstructed_training_data = pose_basis * encoded_training_data;
    reconstructed_training_data.colwise() += pose_mean;

    std::cout << "Mean L1 error =  " << (reconstructed_training_data - pose_data).colwise().lpNorm<1>().mean() << "\n";
    std::cout << "Mean L2 error =  " << (reconstructed_training_data - pose_data).colwise().norm().mean() << "\n";
  }

  {
    std::string filename = vm["out_file"].as<std::string>();
    std::cout << "Saving pose basis to " << filename << " ... " << std::flush;
    H5::H5File file(filename, H5F_ACC_TRUNC);

    H5Eigen::save(file, "pose_mean", pose_mean);
    H5Eigen::save(file, "pose_basis", pose_basis);
    H5Eigen::save(file, "encoded_training_data", encoded_training_data);

    std::cout << " Done" << std::endl;
  }


  return EXIT_SUCCESS;
}



