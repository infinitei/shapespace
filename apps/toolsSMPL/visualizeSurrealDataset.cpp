/**
 * @file visualizeSurrealDataset.cpp
 * @brief visualizeSurrealDataset
 *
 * @author Abhijit Kundu
 */

#include "ShapeSpace/SMPLMeshRenderer.h" // Should be included 1st
#include "ShapeSpace/SMPLmodel.h"
#include "ShapeSpace/H5EigenDense.h"
#include "ShapeSpace/H5EigenTensor.h"

#include <CuteGL/Geometry/ComputeNormals.h>
#include <CuteGL/Surface/WindowRenderViewer.h>
#include <CuteGL/Core/PoseUtils.h>

#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include <QGuiApplication>

template<class DerivedJoints, class DerivedParents>
CuteGL::LineDrawer::LinesPositionContainer computeLinesFromJoints(
    const Eigen::DenseBase<DerivedJoints>& joint_positions, const Eigen::DenseBase<DerivedParents>& parents) {
  using Scalartype = CuteGL::LineDrawer::PositionType::Scalar;
  CuteGL::LineDrawer::LinesPositionContainer lines;
  for (Eigen::Index i = 1; i < parents.size(); ++i) {
    lines.emplace_back(joint_positions.row(parents[i]).template cast<Scalartype>(),
                       joint_positions.row(i).template cast<Scalartype>());
  }
  return lines;
}

int main(int argc, char **argv) {
  namespace po = boost::program_options;
  namespace fs = boost::filesystem;
  using namespace CuteGL;
  using namespace ShapeSpace;


  po::options_description generic_options("Generic Options");
  generic_options.add_options()("help,h", "Help screen");

  po::options_description config_options("Config");
  config_options.add_options()
    ("surreal_info_file", po::value<std::string>(), "SURREAL info file (HDF5) format")
    ("smpl_model_file,m", po::value<std::string>()->default_value("smpl_male_lbs_10_207_0.h5"), "Path to SMPL model file")
    ;

  po::positional_options_description p;
  p.add("surreal_info_file", 1);

  po::options_description cmdline_options;
  cmdline_options.add(generic_options).add(config_options);

  po::variables_map vm;

  try {
    po::store(po::command_line_parser(argc, argv).options(cmdline_options).positional(p).run(), vm);
    po::notify(vm);
  } catch (const po::error &ex) {
    std::cerr << ex.what() << '\n';
    std::cout << cmdline_options << '\n';
    return EXIT_FAILURE;
  }

  if (vm.count("help")) {
    std::cout << cmdline_options << '\n';
    return EXIT_SUCCESS;
  }

  if (!vm.count("surreal_info_file")) {
    std::cout << "Please provide surreal_info_file" << '\n';
    std::cout << cmdline_options << '\n';
    return EXIT_FAILURE;
  }

  const fs::path surreal_info_file = vm["surreal_info_file"].as<std::string>();
  const fs::path smpl_model_file = vm["smpl_model_file"].as<std::string>();

  if (surreal_info_file.extension() != ".h5") {
    std::cout << "Not a HDF5 file: " << surreal_info_file << '\n';
    return EXIT_FAILURE;
  }

  const Eigen::IOFormat fmt(Eigen::StreamPrecision, Eigen::DontAlignCols, ", ", ", ", "", "", "[", "]");

  Eigen::Vector2i image_size;
  Eigen::Matrix3f K;
  Eigen::Vector3f camera_location;
  Eigen::Matrix3f camera_orientation;

  Eigen::MatrixXf body_poses;
  Eigen::Matrix<float, 10, 1> body_shape;

  Eigen::Matrix<float, 3, Eigen::Dynamic> pelvis_locations;
  Eigen::Tensor<float, 3> pelvis_orientations;
  {
    H5::H5File file(surreal_info_file.string(), H5F_ACC_RDONLY);
    H5Eigen::load(file, "image_size", image_size);
    H5Eigen::load(file, "camera_intrinsic", K);
    H5Eigen::load(file, "camera_location", camera_location);
    H5Eigen::load(file, "camera_orientation", camera_orientation);

    H5Eigen::load(file, "body_poses", body_poses);
    H5Eigen::load(file, "body_shape", body_shape);

    H5Eigen::load(file, "pelvis_locations", pelvis_locations);
    H5Eigen::load(file, "pelvis_orientations", pelvis_orientations);
  }

  std::cout << image_size.format(fmt) << "\n";
  std::cout << K << "\n";
  std::cout << camera_location.format(fmt) << "\n";
  std::cout << body_shape.format(fmt) << "\n";
  std::cout << body_poses.size() << "\n";

  using MeshType = CuteGL::SMPLMeshRenderer::MeshType;
  MeshType mesh;
  using SMPL = SMPLmodel<MeshType::PositionScalar>;
  SMPL::MatrixX3 rest_joint_positions;

  Eigen::Matrix<float, 72, 1> pose;
  pose.setZero();
  pose.tail<69>() = body_poses.col(0);

  SMPL smpl(smpl_model_file.string());
  smpl.computeVertices(pose, body_shape, mesh.positions, rest_joint_positions);
  mesh.colors = MeshType::ColorType(180, 180, 180, 255).replicate(mesh.positions.rows(), 1);
  mesh.faces = smpl.faces;
  CuteGL::computeNormals(mesh);

  SMPL::MatrixX3 posed_joint_positions = smpl.computePosedJoints(pose, body_shape);
  std::cout << "pelvic_loc_rest  = " << rest_joint_positions.row(0).format(fmt) << "\n";
  std::cout << "pelvic_loc_posed = " << posed_joint_positions.row(0).format(fmt) << "\n";

  // Pelvis location for fixed shape do not change under pose
  const Eigen::Vector3f pelvis_location_rest = rest_joint_positions.row(0);

  QGuiApplication app(argc, argv);

  // Create the Renderer
  std::unique_ptr<SMPLMeshRenderer> renderer(new SMPLMeshRenderer);
  renderer->setDisplayGrid(true);
  renderer->setDisplayAxis(true);

  // Set the viewer up
  WindowRenderViewer viewer(renderer.get());

  viewer.setBackgroundColor(55, 55, 55);
  viewer.resize(image_size.x(), image_size.y());
  viewer.setSceneRadius(10.0f);


  viewer.camera().intrinsics() = CuteGL::getGLPerspectiveProjection(K, image_size.x(), image_size.y(), 0.01f, 100.0f);

//  // Set camera pose via lookAt
//  viewer.setCameraToLookAt(Eigen::Vector3f(8.0f, 0.0f, 0.0f),
//                           Eigen::Vector3f(0.0f, 0.0f, 0.0f),
//                           -Eigen::Vector3f::UnitY());
//  std::cout << viewer.camera().extrinsics().inverse().matrix() << "\n";

  Eigen::Isometry3f camera_world = Eigen::Isometry3f::Identity();
  camera_world.translation() = camera_location;
  camera_world.linear() = camera_orientation;

  std::cout << "camera_world.matrix() = \n" << camera_world.matrix() << "\n";
  viewer.camera().extrinsics() = camera_world.inverse();


  Eigen::Vector3f current_frame_pelvis_joint = pelvis_locations.col(0);
  std::cout << "current_frame_pelvis_joint = " << current_frame_pelvis_joint.format(fmt) << "\n";


  Eigen::Affine3f model_mat = Eigen::Translation3f(current_frame_pelvis_joint)
                            * Eigen::AngleAxisf(Eigen::Map<Eigen::Matrix3f>(pelvis_orientations.data() + 9 * 0))
                            * Eigen::Translation3f(-pelvis_location_rest);

  std::cout << model_mat.matrix() << "\n";
  renderer->modelMat() = model_mat;

  viewer.showAndWaitTillExposed();
  viewer.setWireframeMode(true);

  renderer->initMeshDrawer(mesh);
  renderer->initLineDrawer();

  renderer->lineDrawer().updateLineVertices(computeLinesFromJoints(posed_joint_positions, smpl.parents),
                                            LineDrawer::ColorType(255, 0, 0, 255),
                                            LineDrawer::ColorType(0, 0, 255, 255));

  return app.exec();

  return EXIT_SUCCESS;
}



