/**
 * @file inferSMPLfromDepthImage.cpp
 * @brief inferSMPLfromDepthImage
 *
 * @author Abhijit Kundu
 */

#include <CuteGL/Renderer/SMPLRenderer.h>
#include <CuteGL/Surface/OffScreenRenderViewer.h>
#include <CuteGL/Core/PoseUtils.h>
#include <CuteGL/IO/ImportPLY.h>

#include "ShapeSpace/ImageUtils.h"
#include "ShapeSpace/NumericDiff.h"

#include <QGuiApplication>
#include <memory>
#include <iostream>

#include <ceres/ceres.h>
#include <glog/logging.h>

class Renderer {
 public:
  using Image32FC1 = Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>;
  using SMPLScalar = float;

  Renderer(const int W, const int H)
      : renderer_(new CuteGL::SMPLRenderer),
        viewer_(renderer_.get()),
        counts_(0) {

    renderer_->setDisplayGrid(false);
    renderer_->setDisplayAxis(false);

    viewer_.setBackgroundColor(55, 55, 55);
    viewer_.resize(W, H);

    // Set camera pose via lookAt
    viewer_.setCameraToLookAt(Eigen::Vector3f(0.0f, 0.85f, 2.6f),
                              Eigen::Vector3f(0.0f, 0.85f, 0.0f),
                              Eigen::Vector3f::UnitY());

    float fovY = 45.0f * M_PI / 180.0f;
    float aspect_ratio = float(W) / H;
    viewer_.camera().intrinsics() = CuteGL::getGLPerspectiveProjection(fovY, aspect_ratio, 1.0f, 4.0f);

    viewer_.create();
    viewer_.makeCurrent();
    renderer_->setSMPLData("smpl_male_lbs_10_207_0.h5");
  }

  template <class DerivedPose, class DerivedShape>
  Image32FC1 operator()(const Eigen::MatrixBase<DerivedPose>& poses, const Eigen::MatrixBase<DerivedShape>& betas) {
    EIGEN_STATIC_ASSERT_VECTOR_SPECIFIC_SIZE(DerivedShape, 10);
    EIGEN_STATIC_ASSERT_VECTOR_SPECIFIC_SIZE(DerivedPose, 72);

    renderer_->smplDrawer().shape() = betas;
    renderer_->smplDrawer().pose() = poses;
    renderer_->smplDrawer().updateShapeAndPose();

    viewer_.render();

    Image32FC1 zbuffer(viewer_.height(), viewer_.width());
    viewer_.readZBuffer(zbuffer.data());

    ++counts_;

    return zbuffer;
  }

  std::unique_ptr<CuteGL::SMPLRenderer> renderer_;
  CuteGL::OffScreenRenderViewer viewer_;

  std::size_t counts_;
};

struct RenderAndCompareCostFunctor {
  using SMPLScalar = Renderer::SMPLScalar;
  using PoseParamType = Eigen::Matrix<SMPLScalar, 72, 1>;

  RenderAndCompareCostFunctor(Renderer& renderer, const Renderer::Image32FC1& target_image)
      : renderer_(renderer),
        target_image_(target_image) {
  }

  template <class Derived>
  typename Derived::Scalar operator()(const Eigen::MatrixBase<Derived>& shape_params) const {
    using Scalar = typename Derived::Scalar;
    //    Renderer::Image32FC1 rendered = renderer_(PoseParamType::Zero(72), shape_params.cast<SMPLScalar>());
    //    Renderer::Image32FC1 diff = renderer_(PoseParamType::Zero(72), shape_params.cast<SMPLScalar>()) - target_image_;
    //    double cost = 0.5 * * diff.squaredNorm();
    return Scalar(0.5) * (renderer_(PoseParamType::Zero(), shape_params.template cast<SMPLScalar>()) - target_image_).squaredNorm();
  }

  Renderer& renderer_;
  const Renderer::Image32FC1& target_image_;
};




class RenderAndCompareFunction : public ceres::FirstOrderFunction {
 public:
  using SMPLScalar = Renderer::SMPLScalar;
  using PoseParamType = Eigen::Matrix<SMPLScalar, Eigen::Dynamic, 1>;

  RenderAndCompareFunction(Renderer& renderer, const Renderer::Image32FC1& target_image)
      : cost_functor_(renderer, target_image) {
  }

  virtual bool Evaluate(const double* parameters_,
                        double* cost_,
                        double* gradient_) const {
    using ShapeParam = Eigen::Matrix<double, 10, 1>;
    Eigen::Map<const ShapeParam> parameters(parameters_);
    cost_[0] = cost_functor_(parameters);
    if (gradient_ != NULL) {
      Eigen::Map<ShapeParam> gradient(gradient_);
//      gradient = ShapeSpace::numericDiffForward(cost_functor_, parameters, cost_[0]);
//      gradient = ShapeSpace::numericDiffCentral(cost_functor_, parameters);
      gradient = ShapeSpace::numericDiffRidders(cost_functor_, parameters);
    }
    return true;
  }

  virtual int NumParameters() const { return 10; }

  RenderAndCompareCostFunctor cost_functor_;
};

int main(int argc, char **argv) {
  google::InitGoogleLogging(argv[0]);
  using namespace ShapeSpace;
  using namespace CuteGL;

  using ParamScalar = Renderer::SMPLScalar;

  QGuiApplication app(argc, argv);

  using PoseParam = Eigen::Matrix<double, 72, 1>;
  using ShapeParam = Eigen::Matrix<double, 10, 1>;

  PoseParam target_pose = 0.00 * PoseParam::Random();
  ShapeParam target_shape = ShapeParam::Zero();
  target_shape[0] = -2;
  target_shape[1] = -5;
  target_shape[2] = 3;
  target_shape[8] = 10;


  ShapeParam pred_shape = ShapeParam::Zero();

  const int W = 512;
  const int H = 512;
  const int pyramid_levels = 2;

  {
    Renderer renderer(W, H);
    Renderer::Image32FC1 target_image = renderer(target_pose.cast<ParamScalar>(),  target_shape.cast<ParamScalar>());
    saveImageAs8UC1(target_image, "target_image.png");

    Renderer::Image32FC1 initial_image = renderer(target_pose.cast<ParamScalar>(),  pred_shape.cast<ParamScalar>());
    saveImageAs8UC1(initial_image, "initial_image.png");
  }

  for (int i = pyramid_levels; i --> 0; ) {
    std::cout << "----------------------------------------------------\n";
    const int cw = W / (1 << i);
    const int ch = H / (1 << i);

    std::cout << "Working on Pyramid level " << i << " (" << cw << "x" << ch << ")" << std::endl;

    Renderer renderer(cw, ch);
    Renderer::Image32FC1 target_image = renderer(target_pose.cast<ParamScalar>(),  target_shape.cast<ParamScalar>());
    RenderAndCompareCostFunctor rac(renderer, target_image);

    {
      ceres::GradientProblem problem(new RenderAndCompareFunction(renderer, target_image));

      ceres::GradientProblemSolver::Options options;
      options.minimizer_progress_to_stdout = true;
      ceres::GradientProblemSolver::Summary summary;
      ceres::Solve(options, problem, pred_shape.data(), &summary);
      std::cout << summary.FullReport() << "\n";
    }

    std::cout << "shape_params : " << pred_shape.transpose() << "\n";
    std::cout << "renderer.counts_ = " << renderer.counts_ << std::endl;
  }

  {
    Renderer renderer(W, H);
    Renderer::Image32FC1 predicted_image = renderer(target_pose.cast<ParamScalar>(),  pred_shape.cast<ParamScalar>());
    saveImageAs8UC1(predicted_image, "predicted_image.png");
  }

  return EXIT_SUCCESS;
}
