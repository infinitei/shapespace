/**
 * @file visualizeSMPLparams.cpp
 * @brief visualizeSMPLparams
 *
 * @author Abhijit Kundu
 */

#include <CuteGL/Renderer/SMPLRenderer.h> // Should be included 1st
#include "ShapeSpace/SMPLmodel.h"
#include "ShapeSpace/H5EigenDense.h"
#include "ShapeSpace/H5StdContainers.h"

#include <CuteGL/Geometry/ComputeNormals.h>
#include <CuteGL/Surface/WindowRenderViewer.h>
#include <CuteGL/Utils/QtUtils.h>

#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include <QGuiApplication>
#include <QPainter>
#include <memory>
#include <iostream>

namespace CuteGL {

class SMPLViewer : public WindowRenderViewer {
 public:
  using MatType = Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>;

  enum Action {
    NEXT_PARAM = 100,
    PREVIOUS_PARAM,
    TOGGLE_PAUSE,
  };

  explicit SMPLViewer(SMPLRenderer* renderer);

  const MatType& poseParams() const {return pose_params_;}
  MatType& poseParams() {return pose_params_;}

  const MatType& shapeParams() const {return shape_params_;}
  MatType& shapeParams() {return shape_params_;}

  const std::vector<std::string>& frameNames() const {return frame_names_;}
  std::vector<std::string>& frameNames() {return frame_names_;}

 protected:
  virtual void handleKeyboardAction(int action);
  virtual void postDraw();

  void updateRenderer();

 private:
  SMPLRenderer* renderer_;
  Eigen::Index param_index_;
  MatType pose_params_;
  MatType shape_params_;
  std::vector<std::string> frame_names_;
  bool paused_;
};

SMPLViewer::SMPLViewer(SMPLRenderer* renderer)
    : WindowRenderViewer(renderer),
      renderer_(renderer),
      param_index_(-1),
      paused_(true) {
  keyboard_handler_.registerKey(NEXT_PARAM, Qt::Key_Right, "Move to next param");
  keyboard_handler_.registerKey(PREVIOUS_PARAM, Qt::Key_Left, "Move to previous param");
  keyboard_handler_.registerKey(TOGGLE_PAUSE, Qt::Key_P, "Pause/Unpause");
}

void SMPLViewer::handleKeyboardAction(int action) {
  switch (action) {
    case NEXT_PARAM: {
      const int max_param_index_ = std::min(pose_params_.rows(), shape_params_.rows()) - 1;
      if (param_index_ < max_param_index_) {
        ++param_index_;
        updateRenderer();
      }
      break;
    }
    case PREVIOUS_PARAM: {
      if (param_index_ > 0) {
        --param_index_;
        updateRenderer();
      }
      break;
    }
    case TOGGLE_PAUSE: {
      paused_ =  !paused_;
      break;
    }
    default:
      WindowRenderViewer::handleKeyboardAction(action);
      break;
  }
}

void SMPLViewer::updateRenderer() {
  renderer_->smplDrawer().pose() = pose_params_.row(param_index_);
  renderer_->smplDrawer().shape() = shape_params_.row(param_index_);
  renderer_->smplDrawer().updateShapeAndPose();
}

void SMPLViewer::postDraw() {
  WindowRenderViewer::postDraw();

  if (param_index_ >= 0) {
    QPainter p(this);
    glfuncs_->glDisable(GL_DEPTH_TEST);
    p.setPen(foregroundColor());

    const QString str(frame_names_[param_index_].c_str());
    p.drawText(200, 20, str);

    p.end();
    glfuncs_->glEnable(GL_DEPTH_TEST);

  }

  if (!paused_) {
    const int max_param_index_ = std::min(pose_params_.rows(), shape_params_.rows()) - 1;
    if (param_index_ < max_param_index_) {
      ++param_index_;
      updateRenderer();
    }
  }
}

}  // namespace CuteGL


int main(int argc, char **argv) {
  using namespace CuteGL;
  using namespace ShapeSpace;
  namespace po = boost::program_options;
  namespace fs = boost::filesystem;

  po::options_description generic_options("Generic Options");
  generic_options.add_options()("help,h", "Help screen");

  po::options_description config_options("Config");
  config_options.add_options()
    ("smplify_result_file", po::value<std::string>(), "SMPL GMM file")
    ("smpl_model_file,m", po::value<std::string>()->default_value("smpl_male_lbs_10_207_0.h5"), "Path to SMPL model file")
    ("use_vsync,v", po::value<bool>()->default_value(true), "Enable disable vsync")
    ;

  po::positional_options_description p;
  p.add("smplify_result_file", 1);

  po::options_description cmdline_options;
  cmdline_options.add(generic_options).add(config_options);

  po::variables_map vm;

  try {
    po::store(po::command_line_parser(argc, argv).options(cmdline_options).positional(p).run(), vm);
    po::notify(vm);
  } catch (const po::error &ex) {
    std::cerr << ex.what() << '\n';
    std::cout << cmdline_options << '\n';
    return EXIT_FAILURE;
  }

  if (vm.count("help")) {
    std::cout << cmdline_options << '\n';
    return EXIT_SUCCESS;
  }

  if (!vm.count("smplify_result_file")) {
    std::cout << "Please provide at-least one smplify_result files" << '\n';
    std::cout << cmdline_options << '\n';
    return EXIT_FAILURE;
  }

  const std::string smplify_result_file = vm["smplify_result_file"].as<std::string>();
  const std::string smpl_model_file = vm["smpl_model_file"].as<std::string>();

  QGuiApplication app(argc, argv);

  // Create the Renderer
  std::unique_ptr <SMPLRenderer> renderer(new SMPLRenderer);
  renderer->setDisplayGrid(true);
  renderer->setDisplayAxis(true);

  // Set the viewer up
  SMPLViewer viewer(renderer.get());
  viewer.setBackgroundColor(55, 55, 55);
  viewer.resize(1024, 768);

  if (!vm["use_vsync"].as<bool>())
      setSwapIntervalToZero(viewer);


  // Set camera pose via lookAt
  viewer.setCameraToLookAt(Eigen::Vector3f(0.0f, -0.35f, 2.6f),
                           Eigen::Vector3f(0.0f, -0.35f, 0.0f),
                           Eigen::Vector3f::UnitY());

  viewer.showAndWaitTillExposed();

  renderer->setSMPLData(loadSMPLDataFromHDF5<SMPLRenderer::SMPLDataType>(smpl_model_file));

  {

    H5::H5File file(smplify_result_file, H5F_ACC_RDONLY);
    try {

      H5Eigen::load(file, "poses", viewer.poseParams());
      H5Eigen::load(file, "betas", viewer.shapeParams());
      H5StdContainers::load(file, "frame_names", viewer.frameNames());

    } catch (const H5::FileIException& e) {
      std::cout << "Exception while reading param files" << std::endl;
      if (viewer.poseParams().size() == 0) {
        return EXIT_FAILURE;
      }

      if (viewer.shapeParams().size() == 0) {
        viewer.shapeParams().setZero(viewer.poseParams().rows(), 10);
      }

      if (viewer.frameNames().size() == 0) {
        for (Eigen::Index i =0; i< viewer.poseParams().rows(); ++i) {
          viewer.frameNames().push_back(std::to_string(i));
        }
      }
    }

    assert(viewer.poseParams().cols() == 72);
    assert(viewer.shapeParams().cols() == 10);
    assert(viewer.poseParams().rows() == viewer.shapeParams().rows());


    //  for (Eigen::Index i= 0; i < poses.rows(); ++i) {
    //    Eigen::Vector3f global_pose_param = poses.row(i).head(3);
    //    Eigen::AngleAxisf global_pose(global_pose_param.norm(), global_pose_param.stableNormalized());
    //    const Eigen::AngleAxisf rot(M_PI, Eigen::Vector3f::UnitZ());
    //
    //    Eigen::AngleAxisf global_pose_corrected(global_pose * rot);
    //    poses.row(i).head(3) = global_pose_corrected.angle() * global_pose_corrected.axis();
    //  }

    for (Eigen::Index i = 0; i < viewer.poseParams().rows(); ++i) {
      viewer.poseParams().row(i).head(3) = Eigen::Vector3f::Zero();
    }
  }

  return app.exec();
}


