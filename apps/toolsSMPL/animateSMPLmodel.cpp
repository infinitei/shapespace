/**
 * @file animateSMPLmodel.cpp
 * @brief animateSMPLmodel
 *
 * @author Abhijit Kundu
 */

#include "ShapeSpace/SMPLmodel.h"

#include <CuteGL/Geometry/ComputeNormals.h>
#include <CuteGL/Surface/WindowRenderViewer.h>
#include <CuteGL/Renderer/BasicLightRenderer.h>
#include <CuteGL/Drawers/MeshDrawer.h>
#include <CuteGL/Utils/QtUtils.h>

#include <QGuiApplication>
#include <memory>
#include <iostream>

namespace CuteGL {

using MeshType = Mesh<double, double, unsigned char>;

class MeshRenderer : public BasicLightRenderer {
 public:
  using Scalar = MeshType::PositionScalar;
  using SMPL = ShapeSpace::SMPLmodel<Scalar>;

  using ShapeParam = Eigen::Matrix<Scalar, 10, 1>;
  using PoseParam = Eigen::Matrix<Scalar, 72, 1>;
  using AnimationData = Eigen::AlignedStdVector<std::pair<ShapeParam, PoseParam>>;


  MeshRenderer(const AnimationData& anim_data)
   :mesh_drawer_(),
    model_mat_(Eigen::Affine3f::Identity()),
    smpl_("smpl_male_lbs_10_207_0.h5"),
    animation_data_(anim_data),
    time_(0) {

    mesh_.positions = smpl_.template_vertices;
    mesh_.colors = CuteGL::MeshType::ColorType(180, 180, 180, 255).replicate(mesh_.positions.rows(), 1);
    mesh_.faces = smpl_.faces;
    computeNormals (mesh_);
  }

  void init(PhongShader& shader);

  const Eigen::Affine3f& modelMat() const {return model_mat_;}
  Eigen::Affine3f& modelMat() {return model_mat_;}

  MeshDrawer& meshDrawer() {return mesh_drawer_;}
  const MeshDrawer& meshDrawer() const {return mesh_drawer_;}

 protected:
  virtual void draw(PhongShader& shader);
 private:
  MeshDrawer mesh_drawer_;
  Eigen::Affine3f model_mat_;
  SMPL smpl_;
  const AnimationData& animation_data_;
  std::size_t time_;
  CuteGL::MeshType mesh_;


 public:
   EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

void MeshRenderer::init(PhongShader& shader) {
  mesh_drawer_.init(shader.program, mesh_);
  shader.program.bind();
  glfuncs_->glUniform3f(shader.program.uniformLocation("light_position_world"),
      1.0f, 2.1f, 5.0f);
  shader.program.release();
}

void MeshRenderer::draw(PhongShader& shader) {
  shader.setModelPose(model_mat_);
  mesh_drawer_.draw();

  if (time_ < animation_data_.size()) {
    const auto& shape_and_pose = animation_data_[time_];
    mesh_.positions = smpl_.computeVertices(shape_and_pose.second, shape_and_pose.first);
    computeNormals(mesh_);
    mesh_drawer_.updateVBO(mesh_.positions);
    mesh_drawer_.updateVBO(mesh_.normals, mesh_.sizeOfPositions());
    ++time_;
  }
}

}  // namespace CuteGL


int main(int argc, char **argv) {
  using namespace CuteGL;
  using namespace ShapeSpace;

  QGuiApplication app(argc, argv);

  std::cout << "Generating animating data ..." << std::flush;
  MeshRenderer::AnimationData anim_data;
  {
    for (float b0 = -2.0f; b0 < 2.0f; b0+=0.5f)
      for (float b1 = -4.0f; b1 < 2.0f; b1+=0.1f)
        for (float b2 = -2.0f; b2 < 2.0f; b2+=1.1f) {
          MeshRenderer::PoseParam poses = 0.00 * MeshRenderer::PoseParam::Random();
          MeshRenderer::ShapeParam betas = MeshRenderer::ShapeParam::Zero();

          betas[0] = b0;
          betas[1] = b1;
  //        betas[2] = b2;

          poses[6] = 0.4;
          poses[7] = 0.0;
          poses[8] = -0.0;

          anim_data.emplace_back(betas, poses);
        }
  }
  std::cout << "Done. With " << anim_data.size() << " frames." << std::endl;

  // Create the Renderer
  std::unique_ptr <MeshRenderer> renderer(new MeshRenderer(anim_data));
  renderer->setDisplayGrid(true);

  // Set the viewer up
  WindowRenderViewer viewer(renderer.get());
  viewer.setBackgroundColor(55, 55, 55);
  viewer.resize(1024, 768);
  viewer.setDisplayFPS(true);
  setSwapIntervalToZero(viewer);


  // Set camera pose via lookAt
  viewer.setCameraToLookAt(Eigen::Vector3f(0.0f, -0.35f, 2.6f),
                           Eigen::Vector3f(0.0f, -0.35f, 0.0f),
                           Eigen::Vector3f::UnitY());

  viewer.showAndWaitTillExposed();

  return app.exec();
}



