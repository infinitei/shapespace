/**
 * @file visualizeSMPLPosePrior.cpp
 * @brief visualizeSMPLPosePrior
 *
 * @author Abhijit Kundu
 */

#include <CuteGL/Renderer/SMPLRenderer.h> // Should be included 1st
#include "ShapeSpace/SMPLmodel.h"
#include "ShapeSpace/MultivariateNormal.h"
#include "ShapeSpace/H5EigenTensor.h"

#include <CuteGL/Geometry/ComputeNormals.h>
#include <CuteGL/Surface/WindowRenderViewer.h>
#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include <QGuiApplication>
#include <QPainter>
#include <memory>
#include <iostream>

namespace CuteGL {

class SMPLViewer : public WindowRenderViewer {
 public:
  using MatType = Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>;

  enum Action {
    NEXT_PARAM = 100,
    PREVIOUS_PARAM,
    TOGGLE_ACCEPT_MODE,
    ACCEPT_CURRENT,
    REJECT_CURRENT,
    SAVE_ACCEPTED_PARAMS,
  };

  enum AcceptanceMode {
    REJECT,
    NO_CHANGE,
    ACCEPT,
  };

  explicit SMPLViewer(SMPLRenderer* renderer);
  void setPoseParams(const MatType& pose_params);
  void saveAcceptedParams();

 protected:
  virtual void handleKeyboardAction(int action);
  virtual void postDraw();

 private:
  SMPLRenderer* renderer_;
  Eigen::Index param_index_;
  MatType pose_params_;
  Eigen::VectorXi param_acceptance_;
  AcceptanceMode acceptance_mode_;
};

SMPLViewer::SMPLViewer(SMPLRenderer* renderer)
    : WindowRenderViewer(renderer),
      renderer_(renderer),
      param_index_(-1),
      acceptance_mode_(NO_CHANGE) {
  keyboard_handler_.registerKey(NEXT_PARAM, Qt::Key_Right, "Move to next param");
  keyboard_handler_.registerKey(PREVIOUS_PARAM, Qt::Key_Left, "Move to previous param");
  keyboard_handler_.registerKey(TOGGLE_ACCEPT_MODE, Qt::Key_T, "Toggle acceptance mode");
  keyboard_handler_.registerKey(ACCEPT_CURRENT, Qt::Key_Y, "Accept current param");
  keyboard_handler_.registerKey(REJECT_CURRENT, Qt::Key_N, "Reject current param");
  keyboard_handler_.registerKey(SAVE_ACCEPTED_PARAMS, Qt::Key_S, "Save accepted params");
}

void SMPLViewer::setPoseParams(const MatType& pose_params) {
  pose_params_ = pose_params;
  param_acceptance_.setOnes(pose_params.rows());
}

void SMPLViewer::saveAcceptedParams() {
  MatType good_poses(param_acceptance_.sum(), pose_params_.cols());
  for (Eigen::Index i = 0, g = 0; i < pose_params_.rows(); ++i) {
    if (param_acceptance_[i])
      good_poses.row(g++) = pose_params_.row(i);
  }
  H5::H5File save_file("selected_poses.h5", H5F_ACC_TRUNC);
  H5Eigen::save(save_file, "poses", good_poses);
  std::cout << "Saved params as selected_poses.h5\n";
}

void SMPLViewer::handleKeyboardAction(int action) {
  switch (action) {
    case NEXT_PARAM: {
      if (param_index_ < (pose_params_.rows() - 1)) {
        ++param_index_;
        renderer_->smplDrawer().pose() = pose_params_.row(param_index_);
        renderer_->smplDrawer().updateShapeAndPose();
      }
      break;
    }
    case PREVIOUS_PARAM: {
      if (param_index_ > 0) {
        --param_index_;
        renderer_->smplDrawer().pose() = pose_params_.row(param_index_);
        renderer_->smplDrawer().updateShapeAndPose();
      }
      break;
    }
    case TOGGLE_ACCEPT_MODE: {
      if (acceptance_mode_ == ACCEPT) {
        acceptance_mode_ = REJECT;
      } else if (acceptance_mode_ == REJECT) {
        acceptance_mode_ = NO_CHANGE;
      } else if (acceptance_mode_ == NO_CHANGE) {
        acceptance_mode_ = ACCEPT;
      }
      break;
    }
    case ACCEPT_CURRENT: {
      if (param_index_ >= 0) {
        param_acceptance_[param_index_] = 1;
      }
      break;
    }
    case REJECT_CURRENT: {
      if (param_index_ >= 0) {
        param_acceptance_[param_index_] = 0;
      }
      break;
    }
    case SAVE_ACCEPTED_PARAMS: {
      saveAcceptedParams();
      break;
    }
    default:
      WindowRenderViewer::handleKeyboardAction(action);
      break;
  }

  if (param_index_ >= 0) {
    if (acceptance_mode_ == ACCEPT) {
      param_acceptance_[param_index_] = 1;
    }
    else if(acceptance_mode_ == REJECT) {
      param_acceptance_[param_index_] = 0;
    }
  }
}

void SMPLViewer::postDraw() {
  WindowRenderViewer::postDraw();

  QPainter p(this);
  glfuncs_->glDisable(GL_DEPTH_TEST);
  p.setPen(foregroundColor());

  if (param_index_ >= 0) {
    const QString str = param_acceptance_[param_index_] ? QString("Accept: Yes") : QString("Accept: No ");
    p.drawText(200, 20, str);
  }

  QString acceptance_mode_str;
  switch (acceptance_mode_) {
    case REJECT:
      acceptance_mode_str = "Mode: REJECT";
      break;
    case NO_CHANGE:
      acceptance_mode_str = "Mode: NO_CHANGE";
      break;
    case ACCEPT:
      acceptance_mode_str = "Mode: ACCEPT";
      break;
    default:
      acceptance_mode_str = "Mode: Unknown";
      break;
  }

  p.drawText(320, 20, acceptance_mode_str);

  p.end();
  glfuncs_->glEnable(GL_DEPTH_TEST);
}

}  // namespace CuteGL


int main(int argc, char **argv) {
  using namespace CuteGL;
  using namespace ShapeSpace;
  namespace po = boost::program_options;
  namespace fs = boost::filesystem;

  po::options_description generic_options("Generic Options");
  generic_options.add_options()("help,h", "Help screen");

  po::options_description config_options("Config");
  config_options.add_options()
    ("smplify_gmm_file", po::value<std::string>()->default_value("pose_gmm_08.h5"), "SMPL GMM file")
    ("smpl_model_file,m", po::value<std::string>()->default_value("smpl_male_lbs_10_207_0.h5"), "Path to SMPL model file")
    ("gmm_mixture_id,g", po::value<int>()->default_value(1), "GMM Mixture ID")
    ("num_of_samples,n", po::value<int>()->default_value(100), "Number of samples")
    ;

  po::positional_options_description p;
  p.add("smplify_gmm_file", 1);

  po::options_description cmdline_options;
  cmdline_options.add(generic_options).add(config_options);

  po::variables_map vm;

  try {
    po::store(po::command_line_parser(argc, argv).options(cmdline_options).positional(p).run(), vm);
    po::notify(vm);
  } catch (const po::error &ex) {
    std::cerr << ex.what() << '\n';
    std::cout << cmdline_options << '\n';
    return EXIT_FAILURE;
  }

  if (vm.count("help")) {
    std::cout << cmdline_options << '\n';
    return EXIT_SUCCESS;
  }

  const std::string smplify_gmm_file = vm["smplify_gmm_file"].as<std::string>();
  const std::string smpl_model_file = vm["smpl_model_file"].as<std::string>();
  const int gmm_mixture_id = vm["gmm_mixture_id"].as<int>();
  const int num_of_samples = vm["num_of_samples"].as<int>();

  QGuiApplication app(argc, argv);

  // Create the Renderer
  std::unique_ptr <SMPLRenderer> renderer(new SMPLRenderer);
  renderer->setDisplayGrid(true);
  renderer->setDisplayAxis(true);

  // Set the viewer up
  SMPLViewer viewer(renderer.get());
  viewer.setBackgroundColor(55, 55, 55);
  viewer.resize(1024, 768);


  // Set camera pose via lookAt
  viewer.setCameraToLookAt(Eigen::Vector3f(0.0f, -0.35f, 2.6f),
                           Eigen::Vector3f(0.0f, -0.35f, 0.0f),
                           Eigen::Vector3f::UnitY());

  viewer.showAndWaitTillExposed();

  renderer->setSMPLData(loadSMPLDataFromHDF5<SMPLRenderer::SMPLDataType>(smpl_model_file));

  {
    using Tensor3 = Eigen::Tensor<float, 3, Eigen::RowMajor>;
    Tensor3 covars;
    SMPLViewer::MatType weights;
    SMPLViewer::MatType means;
    H5::H5File file(smplify_gmm_file, H5F_ACC_RDONLY);
    H5Eigen::load(file, "covars", covars);
    H5Eigen::load(file, "weights", weights);
    H5Eigen::load(file, "means", means);


//    const Eigen::Index num_of_mixtures = means.rows();
//
//    poses.setZero(num_of_mixtures, 72);
//
//    for (Eigen::Index m= 0; m < num_of_mixtures; ++m) {
//      poses.row(m).tail(69) = means.row(m);
//    }



      Eigen::VectorXf mean = means.row(gmm_mixture_id);
      Eigen::MatrixXf covar(mean.size(), mean.size());
      for (Eigen::Index i = 0; i < covar.rows(); ++i)
        for (Eigen::Index j = 0; j < covar.cols(); ++j) {
          covar(i, j) = covars(gmm_mixture_id, i, j);
      }

      Eigen::MultivariateNormal<float> mvn(mean, covar, true);

      SMPLViewer::MatType pose_params;
      pose_params.setZero(num_of_samples, 72);
      for (Eigen::Index k= 0; k < num_of_samples; ++k) {
        pose_params.row(k).tail(69) = mvn.sample();
      }
      viewer.setPoseParams(pose_params);
  }

  return app.exec();
}
