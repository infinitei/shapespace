/**
 * @file offscreenRenderSMPL.cpp
 * @brief offscreenRenderSMPL
 *
 * @author Abhijit Kundu
 */

#include <CuteGL/Renderer/SMPLRenderer.h> // Should be included 1st
#include <CuteGL/Surface/OffScreenRenderViewer.h>

#include "ShapeSpace/ImageUtils.h"
#include "ShapeSpace/SMPLmodel.h"
#include "ShapeSpace/H5EigenDense.h"

#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include <QGuiApplication>
#include <memory>
#include <iostream>

int main(int argc, char **argv) {
  using namespace CuteGL;
  using namespace ShapeSpace;
  namespace po = boost::program_options;
  namespace fs = boost::filesystem;

  po::options_description generic_options("Generic Options");
  generic_options.add_options()("help,h", "Help screen");

  po::options_description config_options("Config");
  config_options.add_options()
    ("smplify_result_file", po::value<std::string>(), "Shape and pose file")
    ("smpl_model_file,m", po::value<std::string>()->default_value("smpl_male_lbs_10_207_0.h5"), "Path to SMPL model file")
    ("smpl_segmm_file,m", po::value<std::string>()->default_value("vertex_segm24_col24_14.h5"), "Path to SMPL segmentation file")
    ;

  po::positional_options_description p;
  p.add("smplify_result_file", 1);

  po::options_description cmdline_options;
  cmdline_options.add(generic_options).add(config_options);

  po::variables_map vm;

  try {
    po::store(po::command_line_parser(argc, argv).options(cmdline_options).positional(p).run(), vm);
    po::notify(vm);
  } catch (const po::error &ex) {
    std::cerr << ex.what() << '\n';
    std::cout << cmdline_options << '\n';
    return EXIT_FAILURE;
  }

  if (vm.count("help")) {
    std::cout << cmdline_options << '\n';
    return EXIT_SUCCESS;
  }

  if (!vm.count("smplify_result_file")) {
    std::cout << "Please provide at-least one smplify_result files" << '\n';
    std::cout << cmdline_options << '\n';
    return EXIT_FAILURE;
  }

  const std::string smpl_model_file = vm["smpl_model_file"].as<std::string>();
  const std::string smpl_segmm_file = vm["smpl_segmm_file"].as<std::string>();
  const std::string smplify_result_file = vm["smplify_result_file"].as<std::string>();

  using MatType = Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>;
  MatType poses;
  MatType shapes;
  {
    H5::H5File file(smplify_result_file, H5F_ACC_RDONLY);
    H5Eigen::load(file, "poses", poses);
    H5Eigen::load(file, "betas", shapes);
    assert(poses.cols() == 72);
    assert(shapes.cols() == 10);
    assert(poses.rows() == shapes.rows());
  }

  Eigen::Index num_of_items = std::min(poses.rows(), shapes.rows());

//  for (Eigen::Index i= 0; i < poses.rows(); ++i) {
//    Eigen::Vector3f global_pose_param = poses.row(i).head(3);
//    Eigen::AngleAxisf global_pose(global_pose_param.norm(), global_pose_param.stableNormalized());
//    const Eigen::AngleAxisf rot(M_PI, Eigen::Vector3f::UnitZ());
//
//    Eigen::AngleAxisf global_pose_corrected(global_pose * rot);
//    poses.row(i).head(3) = global_pose_corrected.angle() * global_pose_corrected.axis();
//  }

  for (Eigen::Index i= 0; i < poses.rows(); ++i) {
   poses.row(i).head(3) = Eigen::Vector3f::Zero();
  }

  QGuiApplication app(argc, argv);

  // Create the Renderer
  std::unique_ptr <SMPLRenderer> renderer(new SMPLRenderer);
  renderer->setDisplayGrid(false);
  renderer->setDisplayAxis(false);

  // Set the viewer up
  OffScreenRenderViewer viewer(renderer.get());
  viewer.setBackgroundColor(0, 0, 0);

  const int W = 640;
  const int H = 480;

  viewer.resize(W, H);


  // Set camera pose via lookAt
  viewer.setCameraToLookAt(Eigen::Vector3f(0.0f, 0.85f, 2.6f),
                           Eigen::Vector3f(0.0f, 0.85f, 0.0f),
                           Eigen::Vector3f::UnitY());

  viewer.create();
  viewer.makeCurrent();

  renderer->setSMPLData(smpl_model_file, smpl_segmm_file);

  std::cout << "Rendering " << num_of_items << " instances " << std::endl;
  for (Eigen::Index i= 0; i < num_of_items; ++i) {
    renderer->smplDrawer().shape() = shapes.row(i);
    renderer->smplDrawer().pose() = poses.row(i);
    renderer->smplDrawer().updateShapeAndPose();

    viewer.render();

//    {
//      QImage image = viewer.grabNormalBuffer();
//      QString out_file = QString("normal_") + QString("%1").arg(i, 6, 'g', -1, '0') + QString(".png");
//      image.save(out_file);
//    }


    using Image32FC1 = Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>;

//    {
//      Image32FC1 buffer(H, W);
//      viewer.grabDepthBuffer(buffer.data());
//      QString out_file = QString("depth_") + QString("%1").arg(i, 6, 'g', -1, '0') + QString(".png");
//      saveImage(buffer, out_file.toStdString(), 255.0);
//    }

    {
      Image32FC1 buffer(H, W);
      viewer.readLabelBuffer(buffer.data());
//      std::cout << buffer.minCoeff() << "  " << buffer.maxCoeff() << "\n";
      QString out_file = QString("label_") + QString("%1").arg(i, 6, 'g', -1, '0') + QString(".exr");
      saveImageAs32FC1(buffer, out_file.toStdString(), 1.0);
    }



  }
  std::cout << "Finished Rendering." << std::endl;

  return EXIT_SUCCESS;
}


