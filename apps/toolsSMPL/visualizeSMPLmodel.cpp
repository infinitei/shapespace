/**
 * @file visualizeSMPLmodel.cpp
 * @brief visualizeSMPLmodel
 *
 * @author Abhijit Kundu
 */

#include "ShapeSpace/SMPLMeshRenderer.h" // Should be included 1st
#include "ShapeSpace/SMPLmodel.h"
#include "ShapeSpace/ShapeSliders.h"

#include <CuteGL/Geometry/ComputeNormals.h>
#include <CuteGL/Surface/WindowRenderViewer.h>

#include <QApplication>
#include <memory>
#include <iostream>

template<class DerivedJoints, class DerivedParents>
CuteGL::LineDrawer::LinesPositionContainer computeLinesFromJoints(
    const Eigen::DenseBase<DerivedJoints>& joint_positions, const Eigen::DenseBase<DerivedParents>& parents) {
  using Scalartype = CuteGL::LineDrawer::PositionType::Scalar;
  CuteGL::LineDrawer::LinesPositionContainer lines;
  for (Eigen::Index i = 1; i < parents.size(); ++i) {
    lines.emplace_back(joint_positions.row(parents[i]).template cast<Scalartype>(),
                       joint_positions.row(i).template cast<Scalartype>());
  }
  return lines;
}


int main(int argc, char **argv) {
  using namespace CuteGL;
  using namespace ShapeSpace;

  std::string smpl_model_file = "smpl_male_lbs_10_207_0.h5";
  if (argc > 1) {
    smpl_model_file = argv[1];
  }
  std::cout << "Using model " << smpl_model_file << std::endl;


  QApplication app(argc, argv);

  // Create the Renderer
  std::unique_ptr<SMPLMeshRenderer> renderer(new SMPLMeshRenderer);
  renderer->setDisplayGrid(false);
  renderer->setDisplayAxis(false);

  // Set the viewer up
  WindowRenderViewer viewer(renderer.get());
  viewer.setBackgroundColor(55, 55, 55);
  viewer.resize(1024, 768);


  // Set camera pose via lookAt
  viewer.setCameraToLookAt(Eigen::Vector3f(0.0f, 0.85f, 2.6f),
                           Eigen::Vector3f(0.0f, 0.85f, 0.0f),
                           Eigen::Vector3f::UnitY());

  viewer.showAndWaitTillExposed();
  viewer.setWireframeMode(true);

  using MeshType = CuteGL::SMPLMeshRenderer::MeshType;
  MeshType mesh;
  using SMPL = SMPLmodel<float>;
  SMPL::MatrixX3 joint_positions;

  Eigen::VectorXf pose = Eigen::VectorXf::Zero(72);
  Eigen::VectorXf betas = Eigen::VectorXf::Zero(10);

//  pose.head(3) = Eigen::Vector3f(1.15665581f, 1.09132814f, 1.21260116f);

  SMPL smpl(smpl_model_file);
  smpl.computeVertices(pose, betas, mesh.positions, joint_positions);

  std::cout << "joint_positions.row(0) = \n" << joint_positions.row(0) << "\n";
  std::cout << "vertices.row(0) = \n" << smpl.template_vertices.row(0) << "\n";

  Eigen::AlignedBox3f bbx;
  for (Eigen::Index i = 0; i < smpl.template_vertices.rows(); ++i) {
    bbx.extend(smpl.template_vertices.row(i).transpose());
  }
  std::cout << "bbx.min() = " << bbx.min() << "\n";
  std::cout << "bbx.center() = " << bbx.center().transpose() << "\n";
  std::cout << "vertices.colwise().mean() = " << smpl.template_vertices.colwise().mean() << "\n";



  mesh.colors = MeshType::ColorType(180, 180, 180, 255).replicate(mesh.positions.rows(), 1);
  mesh.faces = smpl.faces;
  CuteGL::computeNormals(mesh);

  renderer->initMeshDrawer(mesh);
  renderer->initLineDrawer();

  renderer->lineDrawer().updateLineVertices(computeLinesFromJoints(joint_positions, smpl.parents),
                                            LineDrawer::ColorType(255, 0, 0, 255),
                                            LineDrawer::ColorType(0, 0, 255, 255));



  ShapeSliders ss(10, "ShapeSliders");
  ss.show();

  QObject::connect(&ss, &ShapeSliders::valueChanged, [&]() {
    pose = Eigen::VectorXf::Zero(72);
    betas = Eigen::VectorXf::Constant(10, -10.0f);
    betas += ss.value().cast<float>() * 0.2f;

    smpl.computeVertices(pose, betas, mesh.positions, joint_positions);
    CuteGL::computeNormals(mesh);

    renderer->lineDrawer().updateLineVertices(computeLinesFromJoints(joint_positions, smpl.parents),
                                              LineDrawer::ColorType(255, 0, 0, 255),
                                              LineDrawer::ColorType(0, 0, 255, 255));

    renderer->meshDrawer().updateVBO(mesh.positions);
    renderer->meshDrawer().updateVBO(mesh.normals, mesh.sizeOfPositions());
  });

  return app.exec();
}



