# ShapeSpace #

Parametric low dimensional representation of object shapes.

Currently supports SMPL, examplar (TODO)

### Dependencies ###

 - [CMake] - a cross-platform, open-source build system
 - [Eigen 3] - a C++ template library for linear algebra
 - [Boost] - C++ Libraries
 - [CuteGL] - Simple OpenGL visualization library based on Qt and Eigen
 - [Spectra] - C++ Library For Large Scale Eigenvalue Problems


### Installation ###

You can then compile and build using the cmake build system. For an Unix system, that might look as the following steps:

1. Clone the repo
2. `cd /path/to/repo`
4. `mkdir build`
5. `cd build`
6. `cmake ..`
7. `make -j6`


*Author: [Abhijit Kundu]*

  [CuteGL]: https://bitbucket.org/infinitei/CuteGL
  [Boost]: http://www.boost.org/
  [Eigen 3]: http://eigen.tuxfamily.org/
  [Spectra]: https://spectralib.org/
  [CMake]: http://www.cmake.org/
  [Doxygen]: http://doxygen.org
  [Abhijit Kundu]: http://abhijitkundu.info/