#!/usr/bin/env python
import os.path as osp
import numpy as np
import h5py
# from pprint import pprint

smpl_data_file = '/media/Data2/HumanPoseDatsets/surreal/datageneration/smpl_data/SURREAL/smpl_data/smpl_data.npz'
assert osp.exists(smpl_data_file)

smpl_data = np.load(smpl_data_file)
# pprint(sorted(smpl_data.files))
# print type(smpl_data.files)
# print smpl_data['pose_01_01'].shape
# print smpl_data['trans_01_01'].shape
# print smpl_data['femaleshapes'].shape
# print smpl_data['maleshapes'].shape
# print smpl_data['joint_regressor'].shape

for file in smpl_data.files:
    if file.startswith('pose'):
        output_filename = file.replace('pose', 'smpl', 1) + '.h5'
        print 'Saving {}'.format(output_filename)
        with h5py.File(output_filename, 'w') as hf:
            hf.create_dataset('poses', data=smpl_data[file])
