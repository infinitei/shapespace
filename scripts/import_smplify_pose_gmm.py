#!/usr/bin/env python
import os.path as osp
from glob import glob
import cPickle as pickle
# import numpy as np
import h5py

pkl_file = '/media/Data2/HumanPoseDatsets/SMPLify/smplify_public/code/models/gmm_08.pkl'


print('opening %s' % pkl_file)
with open(pkl_file, 'r') as f:
    gmm = pickle.load(f)
    print gmm.keys()
    covars = gmm['covars']
    weights = gmm['weights']
    means = gmm['means']

    output_filename = 'pose_gmm_08.h5'
    print 'Exporting as {}'.format(output_filename)
    with h5py.File(output_filename, 'w') as hf:
        hf.create_dataset('covars', data=covars)
        hf.create_dataset('weights', data=weights)
        hf.create_dataset('means', data=means)
