'''
This script creates visualization data for http://projector.tensorflow.org/

'''

import numpy as np
import h5py


shape_space_file = 'standard_cars_pca.h5'
out_tensor_file = 'standard_cars_data_10d.tsv'

with h5py.File(shape_space_file, 'r') as hf:
	tensor = hf['encoded_training_data'][:]
	print 'Tensor Data shepe: ', tensor.shape
	np.savetxt(out_tensor_file, tensor.T, delimiter="\t")
	print 'Model exported to: ', out_tensor_file


