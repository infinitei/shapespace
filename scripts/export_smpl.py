#!/usr/bin/env python
'''
This script exports SMPL model in Pickle (*.pkl) format to HDF5.
The models in Pickele format are available from http://smpl.is.tue.mpg.de/

This code depends on the python code of SMPL (see http://smpl.is.tue.mpg.de/)

'''

from smpl_webuser.serialization import load_model
# import numpy as np
import h5py


def export_to_hdf5(input_model_fp, export_model_path):
    m = load_model(input_model_fp)

    # Fix to make it compatible with fbx (SMPL maya files)
    # by making sure the lowest point foot is zero
    template_vertices = m.v_template.r.copy()
    min_y = template_vertices[:, 1].min()
    template_vertices[:, 1] -= min_y

    with h5py.File(export_model_path, 'w') as hf:
        # Save template vertices
        hf.create_dataset('template_vertices', data=template_vertices)

        # Save template faces
        hf.create_dataset('faces', data=m.f)

        # Save blend weights
        hf.create_dataset('blend_weights', data=m.weights)

        # Save principal components of shape_displacements (N x 3 x 10)
        hf.create_dataset('shape_displacements', data=m.shapedirs)

        # Save components of pose_displacements (N x 3 x 207)
        hf.create_dataset('pose_displacements', data=m.posedirs)

        # Save joint regressor matrix (24 x N) (transforms rest vertices to rest joints)
        hf.create_dataset('joint_regressor', data=m.J_regressor.toarray())

        # Save the index to the parent joint (in kinemetric tree)
        parents = m.kintree_table[0, :]
        parents[0] = 0
        hf.create_dataset('parents', data=parents)

        print '..Model exported to: ', export_model_path


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()

    parser.add_argument("input_file", help="Path to input pkl file")
    parser.add_argument("-o", "--output_file", required=True, help="Outfile to save to")

    args = parser.parse_args()
    export_to_hdf5(args.input_file, args.output_file)
