#!/usr/bin/env python
import os.path as osp
import scipy.io as sio
import numpy as np
import h5py
from tqdm import tqdm


def get_pose_data(mat_file_path):
    # Load mat file
    mat_contents = sio.loadmat(mat_file_path)

    assert mat_contents['pose'].ndim == 2
    T = mat_contents['pose'].shape[1]
    assert mat_contents['pose'].shape == (72, T)

    return mat_contents['pose']


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description='Collect Pose data from a set of info files')
    parser.add_argument('info_file_list', help='File containing list of (mat) info files')
    parser.add_argument("-d", "--data_dir", required=True, help="Dataset dir root path")
    args = parser.parse_args()

    assert osp.exists(args.info_file_list), 'Input file {} do not exist'.format(args.info_file_list)
    assert osp.exists(args.data_dir), 'data_dir do not exist'.format(args.data_dir)
    # data_dir = '/media/Data2/HumanPoseDatsets/SURREAL_Dataset/SURREAL/data'

    with open(args.info_file_list) as f:
        info_files = f.readlines()
    info_files = [x.strip() for x in info_files]

    print 'Found {} info files'.format(len(info_files))

    all_poses = []
    frame_names = []
    print 'Collecting poses data'
    for info_file in tqdm(info_files):
        info_file_full_path = osp.join(args.data_dir, info_file)
        assert osp.exists(info_file_full_path), 'Info file {} do not exist'.format(info_file_full_path)
        poses = get_pose_data(info_file_full_path)
        clip_name = osp.basename(info_file).replace('_info.mat', '')

        for t in xrange(poses.shape[1]):
            all_poses.append(np.squeeze(poses[:, t]))
            frame_name = '{}_f{:04}'.format(clip_name, t)
            frame_names.append(frame_name)

    print 'Collected {} poses'.format(len(all_poses))
    np_all_poses = np.array(all_poses)
    assert np_all_poses.shape == (len(all_poses), 72)

    out_file = 'selected_surreal_poses.h5'
    print 'saving data to {}'.format(out_file)
    with h5py.File(out_file, 'w') as hf:
        hf.create_dataset('poses', data=np_all_poses)
        hf.create_dataset('frame_names', data=frame_names)
