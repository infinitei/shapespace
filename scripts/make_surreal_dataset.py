#!/usr/bin/env python
import os.path as osp
from os import makedirs
from os import walk
from tqdm import tqdm
import scipy.io as sio
import numpy as np
import fnmatch
import cv2
import h5py
import math


def angle_axis_to_rotation(angle_axis):
    kOne = 1.0
    theta2 = np.dot(angle_axis, angle_axis)
    if theta2 > np.finfo(float).eps:
        # We want to be careful to only evaluate the square root if the
        # norm of the angle_axis vector is greater than zero. Otherwise
        # we get a division by zero.
        theta = math.sqrt(theta2)
        wx = angle_axis[0] / theta
        wy = angle_axis[1] / theta
        wz = angle_axis[2] / theta

        costheta = math.cos(theta)
        sintheta = math.sin(theta)

        R = np.identity(3)

        R[0, 0] = costheta + wx * wx * (kOne - costheta)
        R[1, 0] = wz * sintheta + wx * wy * (kOne - costheta)
        R[2, 0] = -wy * sintheta + wx * wz * (kOne - costheta)
        R[0, 1] = wx * wy * (kOne - costheta) - wz * sintheta
        R[1, 1] = costheta + wy * wy * (kOne - costheta)
        R[2, 1] = wx * sintheta + wy * wz * (kOne - costheta)
        R[0, 2] = wy * sintheta + wx * wz * (kOne - costheta)
        R[1, 2] = -wx * sintheta + wy * wz * (kOne - costheta)
        R[2, 2] = costheta + wz * wz * (kOne - costheta)
    else:
        # Near zero, we switch to using the first order Taylor expansion.
        R[0, 0] = kOne
        R[1, 0] = angle_axis[2]
        R[2, 0] = -angle_axis[1]
        R[0, 1] = -angle_axis[2]
        R[1, 1] = kOne
        R[2, 1] = angle_axis[0]
        R[0, 2] = angle_axis[1]
        R[1, 2] = -angle_axis[0]
        R[2, 2] = kOne
    return R


def read_clip_info_mat_file(mat_file_path):
    # Load mat file
    mat_contents = sio.loadmat(mat_file_path)
    # print mat_contents.keys()

    T = mat_contents['pose'].shape[1]

    assert ((mat_contents['gender'] - mat_contents['gender'][0, 0]) == 0).all(), 'Gender is not consistent across video'
    assert ((mat_contents['shape'] - mat_contents['shape'][:, [0]]) == 0).all(), 'Shapes is not consistent across video'
    assert ((mat_contents['zrot'] - mat_contents['zrot'][0, 0]) == 0).all(), 'zrot is not consistent across video'
    assert mat_contents['pose'].shape == (72, T)
    assert mat_contents['shape'].shape == (10, T)
    assert mat_contents['zrot'].shape == (T, 1)
    if mat_contents['joints3D'].ndim == 2:
        mat_contents['joints3D'] = mat_contents['joints3D'][..., np.newaxis]
    assert mat_contents['joints3D'].shape == (3, 24, T)

    # print 'bg image: {}'.format(mat_contents['bg'][0])

    clip_info = {}

    genders_dict = {0: 'female', 1: 'male'}

    clip_info['gender'] = genders_dict[mat_contents['gender'][0, 0]]
    clip_info['body_poses'] = mat_contents['pose'][-69:, :]
    clip_info['body_shape'] = mat_contents['shape'][:, 0]
    clip_info['image_size'] = np.array([320, 240])
    clip_info['camera_intrinsic'] = np.array([[600.0, 0.0, 160.0], [0.0, 600.0, 120.0], [0.0, 0.0, 1.0]])

    clip_info['camera_location'] = mat_contents['camLoc']
    clip_info['camera_orientation'] = np.array([[0.0, 0.0, -1.0], [0.0, 1.0, 0.0], [-1.0, 0.0, 0.0]])

    zrot = mat_contents['zrot'][0, 0]

    pelvis_orientations = np.zeros((3, 3, T))
    pelvis_locations = np.zeros((3, T))

    for t in xrange(T):
        pelvis_locations[:, t] = mat_contents['joints3D'][:, 0, t]

        blender_mistake = np.array([[1.0, 0.0, 0.0], [0.0, 0.0, -1.0], [0.0, 1.0, 0.0]])
        zrot_mat = angle_axis_to_rotation(np.array([0.0, 0.0, zrot]))
        pelvis_mat = angle_axis_to_rotation(mat_contents['pose'][:3, t])

        pelvis_orientations[:, :, t] = blender_mistake.dot(zrot_mat).dot(pelvis_mat)

    clip_info['pelvis_orientations'] = pelvis_orientations
    clip_info['pelvis_locations'] = pelvis_locations

    clip_info['number_of_frames'] = T

    return clip_info


def read_clip_segm_mat_file(clip_segm_mat_file, frame_ids):
    mat_contents = sio.loadmat(clip_segm_mat_file)
    segms = []
    for frame_id in frame_ids:
        segms.append(mat_contents['segm_{}'.format(frame_id + 1)])
    return segms


def make_colored_image(label_image, colormap):
    W = label_image.shape[1]
    H = label_image.shape[0]
    colored_image = np.zeros((H, W, 3), dtype=np.uint8)
    for i in range(H):
        for j in range(W):
            colored_image[i, j, ...] = colormap[label_image[i, j], ...]
    return colored_image


def sample_data_from_clip(video_clip_file, num_of_samples_per_clip, out_root_dir, colormap):
    splits_ = video_clip_file.split(osp.sep)
    clip_name = osp.splitext(splits_[-1])[0]
    run_id = splits_[-3]
    dataset_split = splits_[-4]
    dataset_subid = splits_[-5]

    clip_dir = osp.dirname(video_clip_file)

    image_dir = osp.join(out_root_dir, dataset_subid, dataset_split, run_id, 'image')
    segm_dir = osp.join(out_root_dir, dataset_subid, dataset_split, run_id, 'segm')
    info_dir = osp.join(out_root_dir, dataset_subid, dataset_split, run_id, 'info')

    for dir_name in [image_dir, segm_dir, info_dir]:
        if not osp.exists(dir_name):
            makedirs(dir_name)

    clip_info = read_clip_info_mat_file(osp.join(clip_dir, clip_name + '_info.mat'))

    if num_of_samples_per_clip > 0:
        samples_per_clip = min(clip_info['number_of_frames'], num_of_samples_per_clip)
        frame_ids = np.sort(np.random.choice(clip_info['number_of_frames'], samples_per_clip, replace=False))
    else:
        frame_ids = [int(clip_info['number_of_frames'] / 2)]

    segmms = read_clip_segm_mat_file(osp.join(clip_dir, clip_name + '_segm.mat'), frame_ids)

    cap = cv2.VideoCapture(video_clip_file)
    assert cap.isOpened(), 'Failed to read video from {}'.format(video_clip_file)

    # loop over frame_ids
    for i, t in enumerate(frame_ids):
        frame_name = '{}_f{:04}'.format(clip_name, t)

        # Read clip
        cap.set(cv2.CAP_PROP_POS_FRAMES, t)
        ret, image = cap.read()
        assert ret, 'Failed to read frame#{} from {}'.format(t, video_clip_file)
        assert image.shape == (clip_info['image_size'][1], clip_info['image_size'][0], 3)

        # Save images
        out_image_file = osp.join(image_dir, frame_name + '.png')
        cv2.imwrite(out_image_file, image)

        # Save finfo
        frame_info = {}
        frame_info['gender'] = clip_info['gender']
        frame_info['image_size'] = clip_info['image_size']
        frame_info['camera_intrinsic'] = clip_info['camera_intrinsic']
        frame_info['camera_location'] = clip_info['camera_location']
        frame_info['camera_orientation'] = clip_info['camera_orientation']
        frame_info['body_shape'] = clip_info['body_shape']

        frame_info['body_pose'] = clip_info['body_poses'][:, t]
        frame_info['pelvis_location'] = clip_info['pelvis_locations'][:, t]
        frame_info['pelvis_orientation'] = clip_info['pelvis_orientations'][:, :, t]

        out_info_file = osp.join(info_dir, frame_name + '.h5')
        with h5py.File(out_info_file, 'w') as hf:
            for key, value in frame_info.iteritems():
                hf.create_dataset(key, data=value)

        assert segmms[i].dtype == np.uint8, "Unexpected segm type = {}".format(segmms[i].dtype)

        # Save Segmentation h5
        out_segm_h5 = osp.join(segm_dir, frame_name + '.h5')
        with h5py.File(out_segm_h5, 'w') as hf:
            hf.create_dataset("segmm", data=segmms[i])

        # Save Segmentation image
        out_segm_png = osp.join(segm_dir, frame_name + '.png')
        cv2.imwrite(out_segm_png, segmms[i])

        # Save Segmentation image
        out_segm_color_png = osp.join(segm_dir, frame_name + '_color.png')
        cv2.imwrite(out_segm_color_png, make_colored_image(segmms[i], colormap))


if __name__ == '__main__':
    np.random.seed(42)
    import argparse
    parser = argparse.ArgumentParser(description='Sample Frames from Surreal dataset')
    parser.add_argument("-i", "--input_dir", required=True, help="Path to input dataset dir")
    parser.add_argument("-o", "--out_dir", required=True, help="Path to output dataset dir")
    parser.add_argument("-s", "--samples_per_clip", type=int, default=5, help="Num of samples per clip (Use <=0 for middle clip only)")

    args = parser.parse_args()
    assert osp.isdir(args.input_dir), '{}  is not a directory'.format(args.input_dir)
    assert osp.isdir(args.out_dir), '{}  is not a directory'.format(args.out_dir)

    print 'Searching clips inside {}'.format(args.input_dir)
    video_clips = []
    for root, dirnames, filenames in walk(args.input_dir):
        for filename in fnmatch.filter(filenames, '*.mp4'):
            video_clips.append(osp.join(root, filename))

    colormap = np.array([[55, 55, 55],
                         [47, 148, 84],
                         [231, 114, 177],
                         [89, 70, 0],
                         [187, 43, 143],
                         [7, 70, 70],
                         [251, 92, 0],
                         [63, 252, 211],
                         [53, 144, 229],
                         [248, 150, 144],
                         [52, 82, 125],
                         [189, 73, 1],
                         [42, 210, 52],
                         [253, 192, 40],
                         [231, 233, 157],
                         [109, 131, 203],
                         [190, 195, 243],
                         [97, 70, 171],
                         [32, 137, 233],
                         [68, 43, 29],
                         [142, 35, 220],
                         [243, 169, 53],
                         [119, 8, 153],
                         [217, 181, 152],
                         [32, 91, 213]], dtype=np.uint8)
    assert colormap.shape == (25, 3)

    print 'Working on {} clips with {} samples per clip'.format(len(video_clips), args.samples_per_clip)
    bar = tqdm(video_clips)
    for clip in bar:
        bar.set_description('Clip: {}'.format(osp.basename(clip)))
        sample_data_from_clip(clip, args.samples_per_clip, args.out_dir, colormap)
