#!/usr/bin/env python
import _init_paths
import os.path as osp
import glob
from subprocess import call
from tqdm import tqdm


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input_dir", required=True, help="Path to Input ShapeNet category directory")
    parser.add_argument("-o", "--output_dir", required=True, help="Path to save the PLY files")
    args = parser.parse_args()

    assert osp.exists(args.input_dir)
    assert osp.exists(args.output_dir)

    print 'Looking for model_normalized.obj files in {}'.format(args.input_dir)
    obj_files = glob.glob(osp.join(args.input_dir, '*/models/model_normalized.obj'))
    print 'Found {} files'.format(len(obj_files))

    export_app = osp.join(_init_paths.root_dir(), 'build', 'exportShapeNetObjModelToPly')
    assert osp.exists(export_app)

    for obj_file in tqdm(obj_files):
        model_name = osp.basename(osp.dirname(osp.dirname(obj_file)))
        ply_file = osp.join(args.output_dir, model_name + '.ply')
        call([export_app, obj_file, ply_file])
        tqdm.write('Exported "{}"'.format(ply_file))
