#!/usr/bin/env python
from pickle import load
import numpy as np
import h5py


if __name__ == '__main__':
    pkl_file = '/home/abhijit/Workspace/HumanPoseStuff/surreal/datageneration/pkl/segm_per_v_overlap.pkl'
    with open(pkl_file, 'rb') as f:
        vertex_segm = load(f)

    num_of_vertices = 0
    max_vid = 0
    for part, vertices in vertex_segm.iteritems():
        num_of_vertices += len(vertices)
        max_vid = max(max_vid, max(vertices))

    if num_of_vertices > (max_vid + 1):
        print('Some vertices are shared by parts')

    parts_24 = ['hips', 'leftUpLeg', 'rightUpLeg', 'spine', 'leftLeg', 'rightLeg',
                'spine1', 'leftFoot', 'rightFoot', 'spine2', 'leftToeBase', 'rightToeBase',
                'neck', 'leftShoulder', 'rightShoulder', 'head', 'leftArm', 'rightArm',
                'leftForeArm', 'rightForeArm', 'leftHand', 'rightHand', 'leftHandIndex1', 'rightHandIndex1']

    parts_14 = ['head',
                'torso',
                'leftUpLeg', 'rightUpLeg',
                'leftLowLeg', 'rightLowLeg',
                'leftUpArm', 'rightUpArm',
                'leftLowArm', 'rightLowArm',
                'leftHand', 'rightHand',
                'leftFoot', 'rightFoot']

    parts_map_14_to_24 = {'head': ['head'],
                          'torso': ['hips', 'spine', 'spine1', 'spine2', 'neck', 'leftShoulder', 'rightShoulder'],
                          'leftUpLeg': ['leftUpLeg'],
                          'rightUpLeg': ['rightUpLeg'],
                          'leftLowLeg': ['leftLeg'],
                          'rightLowLeg': ['rightLeg'],
                          'leftUpArm': ['leftArm'],
                          'rightUpArm': ['rightArm'],
                          'leftLowArm': ['leftForeArm'],
                          'rightLowArm': ['rightForeArm'],
                          'leftHand': ['leftHand', 'leftHandIndex1'],
                          'rightHand': ['rightHand', 'rightHandIndex1'],
                          'leftFoot': ['leftFoot', 'leftToeBase'],
                          'rightFoot': ['rightFoot', 'rightToeBase']}

    parts_map_24_to_14 = {}
    for key, values in parts_map_14_to_24.iteritems():
        for value in values:
            parts_map_24_to_14[value] = key
    assert len(parts_map_24_to_14) == 24

    vertex_segmm_24 = np.zeros(max_vid + 1, dtype=np.int)
    vertex_segmm_14 = np.zeros(max_vid + 1, dtype=np.int)

    for idx24, part24 in enumerate(parts_24):
        vertices = vertex_segm[part24]
        for vert in vertices:
            vertex_segmm_24[vert] = idx24
            vertex_segmm_14[vert] = parts_14.index(parts_map_24_to_14[part24])

    assert max(vertex_segmm_24) == 23
    assert max(vertex_segmm_14) == 13

    colors_14 = np.array([[217, 83, 25, 255],
                          [126, 47, 91, 255],
                          [50, 131, 189, 255],
                          [234, 191, 213, 255],
                          [226, 157, 137, 255],
                          [94, 79, 137, 255],
                          [235, 167, 91, 255],
                          [77, 190, 238, 255],
                          [175, 119, 176, 255],
                          [237, 220, 103, 255],
                          [202, 202, 202, 255],
                          [157, 196, 106, 255],
                          [158, 57, 79, 255],
                          [243, 212, 167, 255]], dtype=np.int)
    assert colors_14.shape == (14, 4)

    colors_24_14 = np.zeros((24, 4), dtype=np.int)
    for idx24, part24 in enumerate(parts_24):
        colors_24_14[idx24, :] = colors_14[parts_14.index(parts_map_24_to_14[part24]), :]

    colors_24 = np.random.randint(256, size=(24, 4))
    colors_24[:, 3] = 255
    print colors_24_14

    out_file = 'vertex_segm.h5'
    print 'Saving output at {}'.format(out_file)
    with h5py.File(out_file, 'w') as hf:
        hf.create_dataset('vertex_segmm_24', data=vertex_segmm_24)
        hf.create_dataset('parts_24', data=parts_24)
        hf.create_dataset('vertex_segmm_14', data=vertex_segmm_14)
        hf.create_dataset('parts_14', data=parts_14)
        for part, vertices in vertex_segm.iteritems():
            hf.create_dataset(part, data=vertices)

    out_file = 'vertex_segm24_col24.h5'
    print 'Saving output at {}'.format(out_file)
    with h5py.File(out_file, 'w') as hf:
        hf.create_dataset('vertex_segmm', data=vertex_segmm_24)
        hf.create_dataset('parts', data=parts_24)
        hf.create_dataset('color_map', data=colors_24)

    out_file = 'vertex_segm24_col24_14.h5'
    print 'Saving output at {}'.format(out_file)
    with h5py.File(out_file, 'w') as hf:
        hf.create_dataset('vertex_segmm', data=vertex_segmm_24)
        hf.create_dataset('parts', data=parts_24)
        hf.create_dataset('color_map', data=colors_24_14)

    out_file = 'vertex_segm14_col14.h5'
    print 'Saving output at {}'.format(out_file)
    with h5py.File(out_file, 'w') as hf:
        hf.create_dataset('vertex_segmm', data=vertex_segmm_14)
        hf.create_dataset('parts', data=parts_14)
        hf.create_dataset('color_map', data=colors_14)
