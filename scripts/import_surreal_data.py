#!/usr/bin/env python
import os.path as osp
import scipy.io as sio
import numpy as np
import h5py
import math


def angle_axis_to_rotation(angle_axis):
    kOne = 1.0
    theta2 = np.dot(angle_axis, angle_axis)
    if theta2 > np.finfo(float).eps:
        # We want to be careful to only evaluate the square root if the
        # norm of the angle_axis vector is greater than zero. Otherwise
        # we get a division by zero.
        theta = math.sqrt(theta2)
        wx = angle_axis[0] / theta
        wy = angle_axis[1] / theta
        wz = angle_axis[2] / theta

        costheta = math.cos(theta)
        sintheta = math.sin(theta)

        R = np.identity(3)

        R[0, 0] = costheta + wx * wx * (kOne - costheta)
        R[1, 0] = wz * sintheta + wx * wy * (kOne - costheta)
        R[2, 0] = -wy * sintheta + wx * wz * (kOne - costheta)
        R[0, 1] = wx * wy * (kOne - costheta) - wz * sintheta
        R[1, 1] = costheta + wy * wy * (kOne - costheta)
        R[2, 1] = wx * sintheta + wy * wz * (kOne - costheta)
        R[0, 2] = wy * sintheta + wx * wz * (kOne - costheta)
        R[1, 2] = -wx * sintheta + wy * wz * (kOne - costheta)
        R[2, 2] = costheta + wz * wz * (kOne - costheta)
    else:
        # Near zero, we switch to using the first order Taylor expansion.
        R[0, 0] = kOne
        R[1, 0] = angle_axis[2]
        R[2, 0] = -angle_axis[1]
        R[0, 1] = -angle_axis[2]
        R[1, 1] = kOne
        R[2, 1] = angle_axis[0]
        R[0, 2] = angle_axis[1]
        R[1, 2] = -angle_axis[0]
        R[2, 2] = kOne
    return R


def read_clip_info_mat_file(mat_file_path):
    # Load mat file
    mat_contents = sio.loadmat(mat_file_path)
    # print mat_contents.keys()

    T = mat_contents['pose'].shape[1]

    assert ((mat_contents['gender'] - mat_contents['gender'][0, 0]) == 0).all(), 'Gender is not consistent across video'
    assert ((mat_contents['shape'] - mat_contents['shape'][:, [0]]) == 0).all(), 'Shapes is not consistent across video'
    assert ((mat_contents['zrot'] - mat_contents['zrot'][0, 0]) == 0).all(), 'zrot is not consistent across video'
    assert mat_contents['pose'].shape == (72, T)
    assert mat_contents['shape'].shape == (10, T)
    assert mat_contents['zrot'].shape == (T, 1)
    if mat_contents['joints3D'].ndim == 2:
        mat_contents['joints3D'] = mat_contents['joints3D'][..., np.newaxis]
    assert mat_contents['joints3D'].shape == (3, 24, T)

    # print 'bg image: {}'.format(mat_contents['bg'][0])

    clip_info = {}

    genders_dict = {0: 'female', 1: 'male'}

    clip_info['gender'] = genders_dict[mat_contents['gender'][0, 0]]
    clip_info['body_poses'] = mat_contents['pose'][-69:, :]
    clip_info['body_shape'] = mat_contents['shape'][:, 0]
    clip_info['image_size'] = np.array([320, 240])
    clip_info['camera_intrinsic'] = np.array([[600.0, 0.0, 160.0], [0.0, 600.0, 120.0], [0.0, 0.0, 1.0]])

    clip_info['camera_location'] = mat_contents['camLoc']
    clip_info['camera_orientation'] = np.array([[0.0, 0.0, -1.0], [0.0, 1.0, 0.0], [-1.0, 0.0, 0.0]])

    zrot = mat_contents['zrot'][0, 0]

    pelvis_orientations = np.zeros((3, 3, T))
    pelvis_locations = np.zeros((3, T))

    for t in xrange(T):
        pelvis_locations[:, t] = mat_contents['joints3D'][:, 0, t]

        blender_mistake = np.array([[1.0, 0.0, 0.0], [0.0, 0.0, -1.0], [0.0, 1.0, 0.0]])
        zrot_mat = angle_axis_to_rotation(np.array([0.0, 0.0, zrot]))
        pelvis_mat = angle_axis_to_rotation(mat_contents['pose'][:3, t])

        pelvis_orientations[:, :, t] = blender_mistake.dot(zrot_mat).dot(pelvis_mat)

    clip_info['pelvis_orientations'] = pelvis_orientations
    clip_info['pelvis_locations'] = pelvis_locations

    clip_info['number_of_frames'] = T

    return clip_info


if __name__ == '__main__':
    import argparse
    description = ('Parse a Caffe training log and plot them')
    parser = argparse.ArgumentParser(description='Convert SURREAL dataset mat files to HDf5')
    parser.add_argument('mat_file_path', help='Path to mat file')
    args = parser.parse_args()
    assert osp.exists(args.mat_file_path), 'Input file do not exist'
    assert osp.splitext(args.mat_file_path)[1] == '.mat', 'Input needs to be a mat file'

    clip_info = read_clip_info_mat_file(args.mat_file_path)
    out_file = args.mat_file_path.replace('.mat', '.h5')
    print 'Saving {}'.format(out_file)
    with h5py.File(out_file, 'w') as hf:
        for key, value in clip_info.iteritems():
            hf.create_dataset(key, data=value)
