#!/usr/bin/env python
import os.path as osp
from glob import glob
import cPickle as pickle
# import numpy as np
import h5py


if __name__ == '__main__':
    # import argparse
    # args = parser.parse_args()

    pkl_files_pattern = osp.join(osp.dirname(__file__), '..', 'results', '*', '*', '*.pkl')
    pkl_files = glob(pkl_files_pattern)
    print 'Found {} files'.format(len(pkl_files))

    for pkl_file in pkl_files:
        print('opening %s' % pkl_file)
        with open(pkl_file, 'r') as f:
            res = pickle.load(f)
        poses = res['poses']
        betas = res['betas']

        output_filename = osp.basename(osp.dirname(osp.dirname(pkl_file))) + '_' + osp.basename(osp.dirname(pkl_file)) + '.h5'
        print 'Exporting as {}'.format(output_filename)
        with h5py.File(output_filename, 'w') as hf:
            hf.create_dataset('poses', data=poses)
            hf.create_dataset('betas', data=betas)
