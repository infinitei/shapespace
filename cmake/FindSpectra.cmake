# Try to find Spectra http://ddemidov.github.io/Spectra/
# 
# You can simply use find_package(Spectra) or find_package(Spectra REQUIRED)
#
# Once done the following variables will be defined
#
#  Spectra_FOUND              - True if Spectra was found on your system
#  Spectra_INCLUDE_DIRS       - List of directories of Spectra and it's dependencies

find_path(Spectra_INCLUDE_DIR 
  NAMES SymEigsSolver.h
  PATHS ${CMAKE_INSTALL_PREFIX}/include
  )

set(Spectra_INCLUDE_DIRS ${Spectra_INCLUDE_DIR} )

#include the Standard package handler
include(FindPackageHandleStandardArgs)

# Handle QUIETLY and REQUIRED arguments and 
# set Spectra_FOUND to TRUE if Spectra_INCLUDE_DIR 
find_package_handle_standard_args(Spectra DEFAULT_MSG Spectra_INCLUDE_DIR)

mark_as_advanced(Spectra_INCLUDE_DIR)

if(Spectra_FOUND)
  add_library(Spectra INTERFACE)
  target_include_directories(Spectra 
    INTERFACE
      ${Spectra_INCLUDE_DIRS}
  )
endif()